import { readFileSync, writeFileSync } from 'node:fs'

writeFileSync('./src/version.ts', `export const version = '${JSON.parse(readFileSync('./package.json', 'utf8')).version}'\n`)
