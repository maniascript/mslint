import { Linter, type LinterTextReport } from '../src/lib/linter/lint.js'
import { type Rule } from '../src/lib/linter/rule.js'

const ruleTester = new Linter()

async function testRule (code: string, rule: Rule | string, ruleConfig?: unknown): Promise<LinterTextReport> {
  const ruleId = typeof rule === 'string' ? rule : rule.meta.id
  return await ruleTester.lintCode(code, { rules: { [ruleId]: ['error', ruleConfig] } })
}

export {
  testRule
}
