import { describe, test, expect } from 'vitest'
import { sameTokens, equalEnums, equalLiterals } from '../../../src/lib/linter/ast-utils.js'
import { parse, Kind, EnumLiteral, ExpressionStatement, type Statement, Node, type Expression } from '@maniascript/parser'

describe('lib/linter/ast-utils.ts', () => {
  describe('sameTokens()', () => {
    test('find identical tokens', async () => {
      const result = await parse(`
        main() {
          A;
          A;
          1 + 1;
          1 + 1;
          F(True, [1, 2, 3]);
          F(True, [1, 2, 3]);
        }
      `, { twoStepsParsing: true, buildAst: true })
      const statements = result.ast.program?.main?.body.body
      expect(statements).toBeDefined()
      if (statements !== undefined) {
        expect(sameTokens(statements[0], statements[1], result.tokens)).toBe(true)
        expect(sameTokens(statements[2], statements[3], result.tokens)).toBe(true)
        expect(sameTokens(statements[4], statements[5], result.tokens)).toBe(true)
        expect(sameTokens(statements[0], statements[2], result.tokens)).toBe(false)
        expect(sameTokens(statements[0], statements[4], result.tokens)).toBe(false)
        expect(sameTokens(statements[2], statements[4], result.tokens)).toBe(false)
      }
    })
  })
  describe('equalEnums()', () => {
    test('find equal enums', async () => {
      function getEnum (statement: Statement): EnumLiteral {
        return ((statement as ExpressionStatement).expression as EnumLiteral)
      }

      const result = await parse(`
        main() {
          CSmMode::EMedal::Gold; // 0
          CSmMode::EMedal::Gold; // 1
          ::EMedal::Silver; // 2
          ::EMedal::Silver; // 3
          ::EMedal::Gold; // 4
          CSmMode::EMedal::Silver; // 5
        }
      `, { twoStepsParsing: true, buildAst: true })
      const statements = result.ast.program?.main?.body.body
      expect(statements).toBeDefined()
      if (statements !== undefined) {
        for (const statement of statements) {
          expect(getEnum(statement).kind).toBe(Kind.EnumLiteral)
        }
        expect(equalEnums(getEnum(statements[0]), getEnum(statements[1]))).toBe(true)
        expect(equalEnums(getEnum(statements[2]), getEnum(statements[3]))).toBe(true)
        expect(equalEnums(getEnum(statements[0]), getEnum(statements[2]))).toBe(false)
        expect(equalEnums(getEnum(statements[0]), getEnum(statements[4]))).toBe(false)
        expect(equalEnums(getEnum(statements[0]), getEnum(statements[5]))).toBe(false)
        expect(equalEnums(getEnum(statements[2]), getEnum(statements[4]))).toBe(false)
      }
    })
  })
  describe('equalLiterals()', () => {
    function getLiteral (node: Node): Expression {
      return (node as ExpressionStatement).expression
    }

    test('find equal literals', async () => {
      const result = await parse(`
        main() {
          "A"; // 0
          "A"; // 1
          "B"; // 2
          1; // 3
          1; // 4
          2; // 5
          -1; // 6
          -1; // 7
          1.; // 8
          1.; // 9
          1.0; // 10
          2.; // 11
          -1.; // 12
          -1.; // 13
          True; // 14
          True; // 15
          False; // 16
          Null; // 17
          Null; // 18
          NullId; // 19
          NullId; // 20
          CSmMode::EMedal::Gold; // 21
          CSmMode::EMedal::Gold; // 22
          CSmMode::EMedal::Silver; // 23
          !False; // 24
          !False; // 25
          !1; // 26
          !!!False; // 27
          !!!False; // 28
          !!False; // 29
        }
      `, { twoStepsParsing: true, buildAst: true })
      const statements = result.ast.program?.main?.body.body
      expect(statements).toBeDefined()
      if (statements !== undefined) {
        expect(equalLiterals(getLiteral(statements[0]), getLiteral(statements[1]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[0]), getLiteral(statements[2]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[3]), getLiteral(statements[4]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[3]), getLiteral(statements[5]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[6]), getLiteral(statements[7]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[3]), getLiteral(statements[6]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[8]), getLiteral(statements[9]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[8]), getLiteral(statements[10]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[8]), getLiteral(statements[11]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[12]), getLiteral(statements[13]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[8]), getLiteral(statements[12]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[14]), getLiteral(statements[15]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[14]), getLiteral(statements[16]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[17]), getLiteral(statements[18]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[19]), getLiteral(statements[20]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[21]), getLiteral(statements[22]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[21]), getLiteral(statements[23]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[0]), getLiteral(statements[3]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[6]), getLiteral(statements[14]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[17]), getLiteral(statements[19]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[24]), getLiteral(statements[25]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[24]), getLiteral(statements[26]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[6]), getLiteral(statements[26]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[6]), getLiteral(statements[26]))).toBe(false)
        expect(equalLiterals(getLiteral(statements[27]), getLiteral(statements[28]))).toBe(true)
        expect(equalLiterals(getLiteral(statements[27]), getLiteral(statements[29]))).toBe(false)
      }
    })
  })
})
