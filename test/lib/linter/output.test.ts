import { describe, test, expect } from 'vitest'
import { formatNanoseconds } from '../../../src/lib/linter/output.js'

describe('lib/linter/output.ts', () => {
  test('formatNanoseconds()', () => {
    expect(formatNanoseconds(1n)).toBe('0s000ms000001ns')
    expect(formatNanoseconds(10n)).toBe('0s000ms000010ns')
    expect(formatNanoseconds(100n)).toBe('0s000ms000100ns')
    expect(formatNanoseconds(1000n)).toBe('0s000ms001000ns')
    expect(formatNanoseconds(10000n)).toBe('0s000ms010000ns')
    expect(formatNanoseconds(100000n)).toBe('0s000ms100000ns')
    expect(formatNanoseconds(1000000n)).toBe('0s001ms000000ns')
    expect(formatNanoseconds(10000000n)).toBe('0s010ms000000ns')
    expect(formatNanoseconds(100000000n)).toBe('0s100ms000000ns')
    expect(formatNanoseconds(1000000000n)).toBe('1s000ms000000ns')
    expect(formatNanoseconds(10000000000n)).toBe('10s000ms000000ns')
  })
})
