import { describe, test, expect } from 'vitest'
import { parseArguments } from '../../../src/lib/linter/cli.js'

describe('lib/linter/cli.ts', () => {
  describe('parseArguments()', () => {
    test('parse paths', () => {
      expect(parseArguments([]).patterns).toBeNull()
      expect(parseArguments(['path/first']).patterns).toStrictEqual(['path/first'])
      expect(parseArguments(['path/first', 'path/second']).patterns).toStrictEqual(['path/first', 'path/second'])
      expect(parseArguments(['--version', 'path/first', 'path/second']).patterns).toStrictEqual(['path/first', 'path/second'])
      expect(parseArguments(['--config', 'path/to/config', 'path/first', 'path/second']).patterns).toStrictEqual(['path/first', 'path/second'])
    })

    test('find unknown argument', () => {
      expect(() => parseArguments(['-z'])).toThrow('Unknown argument -z')
    })

    test('no arguments after paths', () => {
      expect(() => parseArguments(['--help', 'path/to/file/1', 'path/to/file/2', '--version'])).toThrow('You can\'t add more arguments after path \'path/to/file/2\'')
    })

    test('missing value after argument', () => {
      expect(() => parseArguments(['--config'])).toThrow('Missing value for last argument --config')
      expect(() => parseArguments(['--config', '--version'])).toThrow('Missing value for argument --config')
    })

    test('parse verbose', () => {
      expect(parseArguments(['a']).verbose).toBeNull()
      expect(parseArguments(['--verbose']).verbose).toBe(true)
      expect(parseArguments(['-l']).verbose).toBe(true)
    })

    test('parse stats', () => {
      expect(parseArguments(['a']).displayStats).toBeNull()
      expect(parseArguments(['--stats']).displayStats).toBe(true)
      expect(parseArguments(['-s']).displayStats).toBe(true)
    })

    test('parse version', () => {
      expect(parseArguments(['a']).displayVersion).toBeNull()
      expect(parseArguments(['--version']).displayVersion).toBe(true)
      expect(parseArguments(['-v']).displayVersion).toBe(true)
    })

    test('parse help', () => {
      expect(parseArguments(['a']).displayHelp).toBeNull()
      expect(parseArguments(['--help']).displayHelp).toBe(true)
      expect(parseArguments(['-h']).displayHelp).toBe(true)
    })

    test('parse config path', () => {
      expect(parseArguments(['a']).configPath).toBeNull()
      expect(parseArguments(['--config', 'a']).configPath).toBe('a')
      expect(parseArguments(['-c', 'b']).configPath).toBe('b')
      expect(() => parseArguments(['--config', 'a', '-c', 'b'])).toThrow('You can use [--config, -c] only once')
    })

    test('parse report unused disable directives', () => {
      expect(parseArguments(['a']).reportUnusedDisableDirective).toBeNull()
      expect(parseArguments(['--report-unused-disable-directives']).reportUnusedDisableDirective).toBe(true)
    })

    test('parse report disable directives without description', () => {
      expect(parseArguments(['a']).reportDisableDirectiveWithoutDescription).toBeNull()
      expect(parseArguments(['--report-disable-directives-without-description']).reportDisableDirectiveWithoutDescription).toBe(true)
    })
  })
})
