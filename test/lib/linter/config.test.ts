import { describe, test, expect, beforeAll, afterAll } from 'vitest'
import { mkdtemp, rm, writeFile } from 'node:fs/promises'
import {
  Severity,
  getRuleSeverity,
  getRuleSettings,
  validateConfig,
  loadConfig,
  type GlobalConfig
} from '../../../src/lib/linter/config.js'

let tmpDir: string
const config = {
  empty: '',
  noConfig: '{}',
  verbose: `{
    "verbose": true
  }`,
  invalid: 'test'
}

describe('lib/linter/config.ts', () => {
  beforeAll(async () => {
    tmpDir = await mkdtemp('./tmp-config-')
    await writeFile(`${tmpDir}/empty.config.json`, config.empty)
    await writeFile(`${tmpDir}/no-config.config.json`, config.noConfig)
    await writeFile(`${tmpDir}/verbose.config.json`, config.verbose)
    await writeFile(`${tmpDir}/invalid.config.json`, config.invalid)
  })

  afterAll(async () => {
    await Promise.all([
      rm(tmpDir, { recursive: true, force: true })
    ])
  })

  describe('getRuleSeverity()', () => {
    test('converts a value to a rule severity', () => {
      expect(getRuleSeverity('off')).toBe(Severity.Off)
      expect(getRuleSeverity('warn')).toBe(Severity.Warn)
      expect(getRuleSeverity('error')).toBe(Severity.Error)
      expect(getRuleSeverity(0)).toBe(Severity.Off)
      expect(getRuleSeverity(1)).toBe(Severity.Warn)
      expect(getRuleSeverity(2)).toBe(Severity.Error)
      expect(getRuleSeverity(['off', {}])).toBe(Severity.Off)
      expect(getRuleSeverity(['warn', {}])).toBe(Severity.Warn)
      expect(getRuleSeverity(['error', {}])).toBe(Severity.Error)
      expect(getRuleSeverity([0, {}])).toBe(Severity.Off)
      expect(getRuleSeverity([1, {}])).toBe(Severity.Warn)
      expect(getRuleSeverity([2, {}])).toBe(Severity.Error)
      expect(getRuleSeverity('wrong')).toBe(undefined)
      expect(getRuleSeverity(3)).toBe(undefined)
      expect(getRuleSeverity(true)).toBe(undefined)
      expect(getRuleSeverity(['wrong', 'off'])).toBe(undefined)
    })
  })
  describe('getRuleSettings()', () => {
    test('gets the rule settings from an array', () => {
      expect(getRuleSettings([])).toStrictEqual({})
      expect(getRuleSettings([true])).toStrictEqual({})
      expect(getRuleSettings([true, {}])).toStrictEqual({})
      expect(getRuleSettings([true, { test: true }])).toStrictEqual({ test: true })
      expect(getRuleSettings([true, true])).toStrictEqual({})
      expect(getRuleSettings([true, true, true])).toStrictEqual({})
    })
  })
  describe('validateConfig()', () => {
    test('adds missing fields', () => {
      const validatedConfig: GlobalConfig = validateConfig({})
      expect(validatedConfig.cwd).toBeDefined()
      expect(validatedConfig.patterns).toBeDefined()
      expect(validatedConfig.verbose).toBeDefined()
      expect(validatedConfig.displayStats).toBeDefined()
      expect(validatedConfig.reportUnusedDisableDirective).toBeDefined()
      expect(validatedConfig.reportDisableDirectiveWithoutDescription).toBeDefined()
      expect(validatedConfig.linter).toBeDefined()
    })
    test('detects incorrect cwd path', () => {
      expect(() => validateConfig({ cwd: '' })).toThrow(/'cwd' must be an absolute path/)
      expect(() => validateConfig({ cwd: './relative/path' })).toThrow(/'cwd' must be an absolute path/)
    })
    test('convert string patterns to array', () => {
      expect(validateConfig({ patterns: 'test' }).patterns).toStrictEqual(['test'])
    })
  })
  describe('loadConfig()', () => {
    test('loads a config file', () => {
      expect(loadConfig(`${tmpDir}/empty.config.json`)).toStrictEqual(validateConfig({}))
      expect(loadConfig(`${tmpDir}/no-config.config.json`)).toStrictEqual(validateConfig({}))
      expect(loadConfig(`${tmpDir}/verbose.config.json`)).toStrictEqual(validateConfig({ verbose: true }))
      expect(() => loadConfig(`${tmpDir}/error.config.json`)).toThrow(/Config file '.+' not found/)
      expect(() => loadConfig(`${tmpDir}/invalid.config.json`)).toThrow(/Failed to parse config file '.+'/)
    })
  })
})
