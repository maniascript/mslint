import { describe, test, expect, beforeAll, afterAll } from 'vitest'
import { mkdir, mkdtemp, rm, writeFile } from 'fs/promises'
import { normalize } from 'path'
import { iteratePaths, readFile } from '../../../src/lib/linter/files.js'

let tmpDirWithFiles: string
let tmpDirWithDirs: string
let tmpDirWithExtensions: string

describe('lib/linter/files.ts', () => {
  beforeAll(async () => {
    [tmpDirWithFiles, tmpDirWithDirs, tmpDirWithExtensions] = await Promise.all([
      mkdtemp('./tmp-test1-'),
      mkdtemp('./tmp-test2-'),
      mkdtemp('./tmp-test3-')
    ])
    await Promise.all([
      mkdir(`${tmpDirWithDirs}/dir1`),
      mkdir(`${tmpDirWithDirs}/dir2`),
      mkdir(`${tmpDirWithDirs}/dir3`)
    ])
    await Promise.all([
      writeFile(`${tmpDirWithFiles}/file1`, ''),
      writeFile(`${tmpDirWithFiles}/file2`, ''),
      writeFile(`${tmpDirWithFiles}/file3`, ''),
      writeFile(`${tmpDirWithFiles}/file4`, 'Hello!'),
      writeFile(`${tmpDirWithDirs}/dir1/file1`, ''),
      writeFile(`${tmpDirWithDirs}/dir2/file2`, ''),
      writeFile(`${tmpDirWithExtensions}/file1`, ''),
      writeFile(`${tmpDirWithExtensions}/file2.Script.txt`, ''),
      writeFile(`${tmpDirWithExtensions}/file3.md`, '')
    ])
  })

  afterAll(async () => {
    await Promise.all([
      rm(tmpDirWithFiles, { recursive: true, force: true }),
      rm(tmpDirWithDirs, { recursive: true, force: true }),
      rm(tmpDirWithExtensions, { recursive: true, force: true })
    ])
  })

  describe('iteratePaths()', () => {
    test('exports iteratePaths generator', () => {
      expect(iteratePaths).toBeDefined()
    })

    test('iterates over files in a directory', () => {
      const files: string[] = [...iteratePaths(tmpDirWithFiles)]
      expect(files).toStrictEqual([
        normalize(`${tmpDirWithFiles}/file1`),
        normalize(`${tmpDirWithFiles}/file2`),
        normalize(`${tmpDirWithFiles}/file3`),
        normalize(`${tmpDirWithFiles}/file4`)
      ])
    })

    test('iterates recursively over directories', () => {
      const files: string[] = [...iteratePaths(tmpDirWithDirs)]
      expect(files).toStrictEqual([
        normalize(`${tmpDirWithDirs}/dir1/file1`),
        normalize(`${tmpDirWithDirs}/dir2/file2`)
      ])
    })

    test('iterates over several directories', () => {
      const files: string[] = [...iteratePaths([tmpDirWithFiles, tmpDirWithDirs])]
      expect(files).toStrictEqual([
        normalize(`${tmpDirWithFiles}/file1`),
        normalize(`${tmpDirWithFiles}/file2`),
        normalize(`${tmpDirWithFiles}/file3`),
        normalize(`${tmpDirWithFiles}/file4`),
        normalize(`${tmpDirWithDirs}/dir1/file1`),
        normalize(`${tmpDirWithDirs}/dir2/file2`)
      ])
    })

    test('lists each file only once even if they are iterated over several times', () => {
      const files: string[] = [...iteratePaths([tmpDirWithDirs, `${tmpDirWithDirs}/dir1`, `${tmpDirWithDirs}/dir2`])]
      expect(files).toStrictEqual([
        normalize(`${tmpDirWithDirs}/dir1/file1`),
        normalize(`${tmpDirWithDirs}/dir2/file2`)
      ])
    })

    test('filter files by extension', () => {
      const files1: string[] = [...iteratePaths(tmpDirWithExtensions, { extensions: '.Script.txt' })]
      expect(files1).toStrictEqual([
        normalize(`${tmpDirWithExtensions}/file2.Script.txt`)
      ])

      const files2: string[] = [...iteratePaths(tmpDirWithExtensions, { extensions: ['Script.txt', 'md'] })]
      expect(files2).toStrictEqual([
        normalize(`${tmpDirWithExtensions}/file2.Script.txt`),
        normalize(`${tmpDirWithExtensions}/file3.md`)
      ])
    })

    test('filter only extensions not end of filename', () => {
      const files: string[] = [...iteratePaths(tmpDirWithExtensions, { extensions: 'e1' })]
      expect(files).toStrictEqual([])
    })

    test('throws an MSLINT error if the path does not exists', () => {
      const path = 'do/not/exists'
      expect(() => iteratePaths(path).next()).toThrow(`MSLint failed to access path '${path}'`)
    })
  })

  describe('readFile()', () => {
    test('exports a readFile function', () => {
      expect(readFile).toBeDefined()
    })

    test('returns the file content', () => {
      expect(readFile(`${tmpDirWithFiles}/file4`)).toBe('Hello!')
    })

    test('throws an MSLINT error if we cannot read the file', () => {
      const path = 'do/not/exists'
      expect(() => readFile(path)).toThrow(`MSLint failed to read file '${path}'`)
    })
  })
})
