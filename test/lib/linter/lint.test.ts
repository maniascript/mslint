import { describe, test, expect } from 'vitest'
import { Linter, DisableDirectiveType } from '../../../src/lib/linter/lint.js'
import { idDenylist } from '../../../src/lib/rules/id-denylist.js'
import { idLength } from '../../../src/lib/rules/id-length.js'
import { noSleep } from '../../../src/lib/rules/no-sleep.js'
import { noWait } from '../../../src/lib/rules/no-wait.js'
import { Severity } from '../../../src/lib/linter/rule.js'
import { createConfig } from '../../../src/lib/linter/config.js'

describe('lib/linter/lint.ts', () => {
  describe('Linter', () => {
    test('export Linter class', () => {
      expect(Linter).toBeDefined()
    })

    test('ignore disabled rules', async () => {
      const code = `
      main() {
        declare Integer NotOk; // @mslint-disable-line
        declare Integer NotOk; // @mslint-disable-line ${idDenylist.meta.id}
        declare Integer NotOkTooLong; // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}
        declare Integer NotOkTooLong; sleep(1); // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}
        declare Integer NotOk; /* @mslint-disable-line */
        declare Integer NotOk; /* @mslint-disable-line ${idDenylist.meta.id} */
        declare Integer NotOkTooLong; /* @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id} */
        declare Integer NotOkTooLong; sleep(1); /* @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} */

        declare Integer NotOk; // @mslint-disable-line -- describe why mslint was disabled
        declare Integer NotOk; // @mslint-disable-line ${idDenylist.meta.id} -- describe why mslint was disabled
        declare Integer NotOkTooLong; // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id} -- describe why mslint was disabled
        declare Integer NotOkTooLong; sleep(1); // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} -- describe why mslint was disabled
        declare Integer NotOk; /* @mslint-disable-line -- describe why mslint was disabled */
        declare Integer NotOk; /* @mslint-disable-line ${idDenylist.meta.id} -- describe why mslint was disabled */
        declare Integer NotOkTooLong; /* @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id} -- describe why mslint was disabled */
        declare Integer NotOkTooLong; sleep(1); /* @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} -- describe why mslint was disabled */

        // @mslint-disable-next-line
        declare Integer NotOk;
        // @mslint-disable-next-line ${idDenylist.meta.id}
        declare Integer NotOk;
        // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}
        declare Integer NotOkTooLong;
        // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}
        declare Integer NotOkTooLong; sleep(1);
        /* @mslint-disable-next-line */
        declare Integer NotOk;
        /* @mslint-disable-next-line ${idDenylist.meta.id} */
        declare Integer NotOk;
        /* @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id} */
        declare Integer NotOkTooLong;
        /* @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} */
        declare Integer NotOkTooLong; sleep(1);

        // @mslint-disable-next-line -- describe why mslint was disabled
        declare Integer NotOk;
        // @mslint-disable-next-line ${idDenylist.meta.id} -- describe why mslint was disabled
        declare Integer NotOk;
        // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id} -- describe why mslint was disabled
        declare Integer NotOkTooLong;
        // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} -- describe why mslint was disabled
        declare Integer NotOkTooLong; sleep(1);
        /* @mslint-disable-next-line -- describe why mslint was disabled */
        declare Integer NotOk;
        /* @mslint-disable-next-line ${idDenylist.meta.id} -- describe why mslint was disabled */
        declare Integer NotOk;
        /* @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id} -- describe why mslint was disabled */
        declare Integer NotOkTooLong;
        /* @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} -- describe why mslint was disabled */
        declare Integer NotOkTooLong; sleep(1);

        declare Integer NotOk; wait(True); // @mslint-disable-line
        declare Integer NotOkTooLong; sleep(1); wait(True); // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}

        // @mslint-disable-next-line
        declare Integer NotOk; wait(True);
        // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}
        declare Integer NotOkTooLong; sleep(1); wait(True);

        /*
         * @mslint-disable-next-line
         */
        declare Integer NotOk;
        /*
         * @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} -- describe why mslint was disabled
         */    // @Debug
        declare Integer NotOkTooLong; sleep(1);
        /*
         * @mslint-disable-next-line
         */

        declare Integer NotOk;
      }
      // @mslint-disable-next-line`

      const linter = new Linter()
      const lintResult = await linter.lintCode(
        code,
        {
          rules: {
            [idDenylist.meta.id]: ['error', { list: ['NotOk'] }],
            [idLength.meta.id]: ['error', { maximum: 5 }],
            [noSleep.meta.id]: ['error'],
            [noWait.meta.id]: ['error']
          }
        }
      )

      expect(lintResult.disabledRuleComments.length).toBe(40)

      expect(lintResult.disabledRuleComments[0]).toMatchObject({ line: 3, ruleIds: [], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[1]).toMatchObject({ line: 4, ruleIds: [idDenylist.meta.id], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[2]).toMatchObject({ line: 5, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[3]).toMatchObject({ line: 6, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[4]).toMatchObject({ line: 7, ruleIds: [], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[5]).toMatchObject({ line: 8, ruleIds: [idDenylist.meta.id], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[6]).toMatchObject({ line: 9, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[7]).toMatchObject({ line: 10, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[8]).toMatchObject({ line: 12, ruleIds: [], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[9]).toMatchObject({ line: 13, ruleIds: [idDenylist.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[10]).toMatchObject({ line: 14, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[11]).toMatchObject({ line: 15, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[12]).toMatchObject({ line: 16, ruleIds: [], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[13]).toMatchObject({ line: 17, ruleIds: [idDenylist.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[14]).toMatchObject({ line: 18, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[15]).toMatchObject({ line: 19, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[16]).toMatchObject({ line: 22, ruleIds: [], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[17]).toMatchObject({ line: 24, ruleIds: [idDenylist.meta.id], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[18]).toMatchObject({ line: 26, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[19]).toMatchObject({ line: 28, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[20]).toMatchObject({ line: 30, ruleIds: [], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[21]).toMatchObject({ line: 32, ruleIds: [idDenylist.meta.id], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[22]).toMatchObject({ line: 34, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[23]).toMatchObject({ line: 36, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[24]).toMatchObject({ line: 39, ruleIds: [], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[25]).toMatchObject({ line: 41, ruleIds: [idDenylist.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[26]).toMatchObject({ line: 43, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[27]).toMatchObject({ line: 45, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[28]).toMatchObject({ line: 47, ruleIds: [], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[29]).toMatchObject({ line: 49, ruleIds: [idDenylist.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[30]).toMatchObject({ line: 51, ruleIds: [idDenylist.meta.id, idLength.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[31]).toMatchObject({ line: 53, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[32]).toMatchObject({ line: 55, ruleIds: [], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[33]).toMatchObject({ line: 56, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: '', directiveType: DisableDirectiveType.CurrentLine })
      expect(lintResult.disabledRuleComments[34]).toMatchObject({ line: 59, ruleIds: [], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[35]).toMatchObject({ line: 61, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[36]).toMatchObject({ line: 66, ruleIds: [], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[37]).toMatchObject({ line: 70, ruleIds: [idDenylist.meta.id, idLength.meta.id, noSleep.meta.id], description: 'describe why mslint was disabled', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[38]).toMatchObject({ line: 74, ruleIds: [], description: '', directiveType: DisableDirectiveType.NextLine })
      expect(lintResult.disabledRuleComments[39]).toMatchObject({ line: 78, ruleIds: [], description: '', directiveType: DisableDirectiveType.NextLine })

      expect(lintResult.success).toBe(false)
      expect(lintResult.messages.length).toBe(54)

      const errorMessages = lintResult.messages.filter(message => message.severity === Severity.Error)
      expect(errorMessages.length).toBe(3)
      expect(errorMessages[0].ruleId).toBe(noWait.meta.id)
      expect(errorMessages[0].source.loc.start.line).toBe(56)
      expect(errorMessages[1].ruleId).toBe(noWait.meta.id)
      expect(errorMessages[1].source.loc.start.line).toBe(61)
      expect(errorMessages[2].ruleId).toBe(idDenylist.meta.id)
      expect(errorMessages[2].source.loc.start.line).toBe(75)
    })

    test('report unusued disable directives', async () => {
      const code = `
      declare Integer Ok; // @mslint-disable-line
      declare Integer Ok; // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}

      declare Integer NotOkTooLong; // @mslint-disable-line
      declare Integer NotOkTooLong; // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}

      // @mslint-disable-next-line
      declare Integer Ok;
      // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}
      declare Integer Ok;

      // @mslint-disable-next-line
      declare Integer NotOkTooLong;
      // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}
      declare Integer NotOkTooLong;
      `

      const linter = new Linter(createConfig({ reportUnusedDisableDirective: true }))
      const lintResult = await linter.lintCode(
        code,
        {
          rules: {
            [idDenylist.meta.id]: ['error', { list: ['NotOk'] }],
            [idLength.meta.id]: ['error', { maximum: 5 }],
            [noSleep.meta.id]: ['error']
          }
        }
      )

      expect(lintResult.success).toBe(false)
      expect(lintResult.disabledRuleComments.length).toBe(8)
      expect(lintResult.messages.length).toBe(16)

      const errorMessages = lintResult.messages.filter(message => message.severity === Severity.Error)
      expect(errorMessages.length).toBe(12)
      expect(errorMessages[0].ruleId).toBe('')
      expect(errorMessages[0].message).toBe('Unused @mslint-disable-line directive (no problems were reported)')
      expect(errorMessages[0].source.loc.start.line).toBe(2)
      expect(errorMessages[1].ruleId).toBe('')
      expect(errorMessages[1].message).toBe('Unused @mslint-disable-line directive (no problems were reported from id-denylist)')
      expect(errorMessages[1].source.loc.start.line).toBe(3)
      expect(errorMessages[2].ruleId).toBe('')
      expect(errorMessages[2].message).toBe('Unused @mslint-disable-line directive (no problems were reported from id-length)')
      expect(errorMessages[2].source.loc.start.line).toBe(3)
      expect(errorMessages[3].ruleId).toBe('')
      expect(errorMessages[3].message).toBe('Unused @mslint-disable-line directive (no problems were reported from no-sleep)')
      expect(errorMessages[3].source.loc.start.line).toBe(3)
      expect(errorMessages[4].ruleId).toBe('')
      expect(errorMessages[4].message).toBe('Unused @mslint-disable-line directive (no problems were reported from id-denylist)')
      expect(errorMessages[4].source.loc.start.line).toBe(6)
      expect(errorMessages[5].ruleId).toBe('')
      expect(errorMessages[5].message).toBe('Unused @mslint-disable-line directive (no problems were reported from no-sleep)')
      expect(errorMessages[5].source.loc.start.line).toBe(6)
      expect(errorMessages[6].ruleId).toBe('')
      expect(errorMessages[6].message).toBe('Unused @mslint-disable-next-line directive (no problems were reported)')
      expect(errorMessages[6].source.loc.start.line).toBe(8)
      expect(errorMessages[7].ruleId).toBe('')
      expect(errorMessages[7].message).toBe('Unused @mslint-disable-next-line directive (no problems were reported from id-denylist)')
      expect(errorMessages[7].source.loc.start.line).toBe(10)
      expect(errorMessages[8].ruleId).toBe('')
      expect(errorMessages[8].message).toBe('Unused @mslint-disable-next-line directive (no problems were reported from id-length)')
      expect(errorMessages[8].source.loc.start.line).toBe(10)
      expect(errorMessages[9].ruleId).toBe('')
      expect(errorMessages[9].message).toBe('Unused @mslint-disable-next-line directive (no problems were reported from no-sleep)')
      expect(errorMessages[9].source.loc.start.line).toBe(10)
      expect(errorMessages[10].ruleId).toBe('')
      expect(errorMessages[10].message).toBe('Unused @mslint-disable-next-line directive (no problems were reported from id-denylist)')
      expect(errorMessages[10].source.loc.start.line).toBe(15)
      expect(errorMessages[11].ruleId).toBe('')
      expect(errorMessages[11].message).toBe('Unused @mslint-disable-next-line directive (no problems were reported from no-sleep)')
      expect(errorMessages[11].source.loc.start.line).toBe(15)
    })

    test('report disable directives without description', async () => {
      const code = `
      declare Integer Ok; // @mslint-disable-line -- Description
      declare Integer Ok; // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} -- Description

      declare Integer Ok; // @mslint-disable-line
      declare Integer Ok; // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}

      // @mslint-disable-next-line -- Description
      declare Integer Ok;
      // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id} -- Description
      declare Integer Ok;

      // @mslint-disable-next-line
      declare Integer Ok;
      // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}
      declare Integer Ok;
      `

      const linter = new Linter(createConfig({ reportDisableDirectiveWithoutDescription: true }))
      const lintResult = await linter.lintCode(
        code,
        {
          rules: {
            [idDenylist.meta.id]: ['error', { list: ['NotOk'] }],
            [idLength.meta.id]: ['error', { maximum: 5 }],
            [noSleep.meta.id]: ['error']
          }
        }
      )

      expect(lintResult.success).toBe(false)
      expect(lintResult.disabledRuleComments.length).toBe(8)
      expect(lintResult.messages.length).toBe(4)

      expect(lintResult.messages[0].ruleId).toBe('')
      expect(lintResult.messages[0].message).toBe('You must add a description to the @mslint-disable-line directives')
      expect(lintResult.messages[0].source.loc.start.line).toBe(5)
      expect(lintResult.messages[1].ruleId).toBe('')
      expect(lintResult.messages[1].message).toBe('You must add a description to the @mslint-disable-line directives')
      expect(lintResult.messages[1].source.loc.start.line).toBe(6)
      expect(lintResult.messages[2].ruleId).toBe('')
      expect(lintResult.messages[2].message).toBe('You must add a description to the @mslint-disable-next-line directives')
      expect(lintResult.messages[2].source.loc.start.line).toBe(13)
      expect(lintResult.messages[3].ruleId).toBe('')
      expect(lintResult.messages[3].message).toBe('You must add a description to the @mslint-disable-next-line directives')
      expect(lintResult.messages[3].source.loc.start.line).toBe(15)
    })

    test('compute disableRuleComments only when necessary', async () => {
      const code = `
      declare Integer Ok; // @mslint-disable-line
      declare Integer Ok; // @mslint-disable-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}

      // @mslint-disable-next-line
      declare Integer Ok;
      // @mslint-disable-next-line ${idDenylist.meta.id}, ${idLength.meta.id}, ${noSleep.meta.id}
      declare Integer Ok;
      `

      const linter = new Linter()
      const lintResult = await linter.lintCode(
        code,
        {
          rules: {
            [idDenylist.meta.id]: ['error', { list: ['NotOk'] }],
            [idLength.meta.id]: ['error', { maximum: 5 }],
            [noSleep.meta.id]: ['error']
          }
        }
      )

      expect(lintResult.success).toBe(true)
      expect(lintResult.messages.length).toBe(0)
      expect(lintResult.disabledRuleComments.length).toBe(0)
    })
  })
})
