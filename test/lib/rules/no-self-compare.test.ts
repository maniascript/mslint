import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noSelfCompare } from '../../../src/lib/rules/no-self-compare.js'

describe('lib/rules/no-self-compare.ts', () => {
  describe('Forbid comparisons where both sides are the same', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          A == B;
          if (A == B) {}
          while (A == B) {}
          A < B;
          A <= B;
          A > B;
          A >= B;
          A != B;
          1 == 2;
          A * A;
          A / A;
          A % A;
          A + A;
          A - A;
          "A"^"A";
        }
      `, noSelfCompare)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        main() {
          A == A;
          if (A == A) {}
          while (A == A) {}
          A < A;
          A <= A;
          A > A;
          A >= A;
          A != A;
          1 == 1;
          1 + 2 * F() != 1    +     2   *  F  (  )  ;
        }
      `, noSelfCompare)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(10)
      for (const message of result.messages) {
        expect(message.message).toBe('Comparing to itself is probably an error')
      }
      expect(result.messages[0].source.range.start).toBe(28)
      expect(result.messages[0].source.range.end).toBe(33)
      expect(result.messages[1].source.range.start).toBe(50)
      expect(result.messages[1].source.range.end).toBe(55)
      expect(result.messages[2].source.range.start).toBe(78)
      expect(result.messages[2].source.range.end).toBe(83)
      expect(result.messages[3].source.range.start).toBe(99)
      expect(result.messages[3].source.range.end).toBe(103)
      expect(result.messages[4].source.range.start).toBe(116)
      expect(result.messages[4].source.range.end).toBe(121)
      expect(result.messages[5].source.range.start).toBe(134)
      expect(result.messages[5].source.range.end).toBe(138)
      expect(result.messages[6].source.range.start).toBe(151)
      expect(result.messages[6].source.range.end).toBe(156)
      expect(result.messages[7].source.range.start).toBe(169)
      expect(result.messages[7].source.range.end).toBe(174)
      expect(result.messages[8].source.range.start).toBe(187)
      expect(result.messages[8].source.range.end).toBe(192)
      expect(result.messages[9].source.range.start).toBe(205)
      expect(result.messages[9].source.range.end).toBe(244)
    })
  })
})
