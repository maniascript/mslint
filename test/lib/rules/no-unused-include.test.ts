import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noUnusedInclude } from '../../../src/lib/rules/no-unused-include.js'

describe('lib/rules/no-unused-include.ts', () => {
  describe('An included library must be used', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Include "A" as A
        #Include "B" as B
        #Include "C" as C
        #Include "D" as D

        #Const C::C as C
        #Struct D::D as D

        Void D() {
          B::B();
        }

        main() {
          A::A();
        }
      `, noUnusedInclude)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
      #Include "A" as A
      #Include "C" as C
      main() {
        B::B();
        declare Integer[] Test = [
          1,
          C::C(),
          3
        ];
      }
      `, noUnusedInclude)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(1)
      expect(result.messages[0].message).toBe('Include \'A\' is never used')
      expect(result.messages[0].source.range.start).toBe(7)
      expect(result.messages[0].source.range.end).toBe(23)
    })
    test('does not check includes without alias', async () => {
      const result = await testRule(`
      #Include "A"
      main() {
        B::B();
      }
      `, noUnusedInclude)
      expect(result.success).toBe(true)
    })
    test('does not check scripts that can be extended', async () => {
      let result = await testRule(`
      #RequireContext CSmMode
      #Include "A" as A
      main() {
        B::B();
      }
      `, noUnusedInclude)
      expect(result.success).toBe(true)

      result = await testRule(`
      #Extends "A.Script.txt"
      #Include "A" as A
      main() {
        B::B();
      }
      `, noUnusedInclude)
      expect(result.success).toBe(true)
    })
  })
})
