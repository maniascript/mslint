import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { forDirection } from '../../../src/lib/rules/for-direction.js'

describe('lib/rules/for-direction.ts', () => {
  describe('Start value must be smaller than stop value', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          for (A, 1, 10) {}
          for (A, 1, 10, -2) {}
        }
      `, forDirection)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        main() {
          for (A, 10, 1) {}
          for (A, 10, 1, -2) {}
        }
      `, forDirection)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(2)
      for (const message of result.messages) {
        expect(message.message).toBe('This for loop won\'t be executed because the start value is bigger or equal to the stop value')
      }
      expect(result.messages[0].source.range.start).toBe(28)
      expect(result.messages[0].source.range.end).toBe(44)
      expect(result.messages[1].source.range.start).toBe(56)
      expect(result.messages[1].source.range.end).toBe(76)
    })
  })
})
