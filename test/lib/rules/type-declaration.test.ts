import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { typeDeclaration, Type } from '../../../src/lib/rules/type-declaration.js'

describe('lib/rules/type-declaration.ts', () => {
  describe('Check variable declaration explicite type', () => {
    test('is valid', async () => {
      const result = await testRule(`
        declare Integer G_A;

        main() {
          declare Integer B;
          declare Integer C = 0;
          declare Integer D for This;
          declare Integer E for This = 1;
          let F = 1;
        }
      `, typeDeclaration)
      expect(result.success).toBe(true)
    })
    describe('is invalid', () => {
      test('explicite type', async () => {
        const result = await testRule(`
          main() {
            declare Integer A;
            declare Integer B = 1;
            declare C = 0;
            declare Integer D for This;
            declare Integer E for This = 3;
            declare F for This = 2;
            let G = 4;
          }
        `, typeDeclaration, { type: Type.Explicite })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(2)
        expect(result.messages[0].message).toBe('Variable \'C\' must have an explicite type on declaration')
        expect(result.messages[1].message).toBe('Variable \'F\' must have an explicite type on declaration')

        expect(result.messages[0].source.range.start).toBe(98)
        expect(result.messages[0].source.range.end).toBe(111)
        expect(result.messages[1].source.range.start).toBe(209)
        expect(result.messages[1].source.range.end).toBe(231)
      })

      test('implicite type', async () => {
        const result = await testRule(`
          main() {
            declare Integer A;
            declare Integer B = 1;
            declare C = 0;
            declare Integer D for This;
            declare Integer E for This = 3;
            declare F for This = 2;
            let G = 4;
          }
        `, typeDeclaration, { type: Type.Implicite })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(4)
        expect(result.messages[0].message).toBe('Variable \'A\' should be initialized to have an implicit type on declaration')
        expect(result.messages[1].message).toBe('Variable \'B\' should have an implicit type on declaration')
        expect(result.messages[2].message).toBe('Variable \'D\' should be initialized to have an implicit type on declaration')
        expect(result.messages[3].message).toBe('Variable \'E\' should have an implicit type on declaration')

        expect(result.messages[0].source.range.start).toBe(32)
        expect(result.messages[0].source.range.end).toBe(49)
        expect(result.messages[1].source.range.start).toBe(63)
        expect(result.messages[1].source.range.end).toBe(84)
        expect(result.messages[2].source.range.start).toBe(125)
        expect(result.messages[2].source.range.end).toBe(151)
        expect(result.messages[3].source.range.start).toBe(165)
        expect(result.messages[3].source.range.end).toBe(195)
      })
    })
  })
})
