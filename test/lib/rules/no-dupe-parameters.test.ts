import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noDupeParameters } from '../../../src/lib/rules/no-dupe-parameters.js'

describe('lib/rules/no-dupe-parameters.ts', () => {
  describe('Forbid duplicate parameters in function definitions', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA(Integer _A1, Integer _A2) {}
        Void FunctionB(Integer _A1, Integer _A2) {}
      `, noDupeParameters)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        Void FunctionA(Integer _A1, Integer _A1) {}
        Void FunctionB(Integer _B1, Integer _B1, Integer _B1) {}
        Void FunctionC(Integer _C1, Text _C1) {}
        Void FunctionD(Integer _D1, Text _D2, Integer _D1, Text _D2) {}
      `, noDupeParameters)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(6)
      expect(result.messages[0].message).toBe('Duplicated parameter \'_A1\'')
      expect(result.messages[0].source.range.start).toBe(45)
      expect(result.messages[0].source.range.end).toBe(47)
      expect(result.messages[1].message).toBe('Duplicated parameter \'_B1\'')
      expect(result.messages[1].source.range.start).toBe(97)
      expect(result.messages[1].source.range.end).toBe(99)
      expect(result.messages[2].message).toBe('Duplicated parameter \'_B1\'')
      expect(result.messages[2].source.range.start).toBe(110)
      expect(result.messages[2].source.range.end).toBe(112)
      expect(result.messages[3].message).toBe('Duplicated parameter \'_C1\'')
      expect(result.messages[3].source.range.start).toBe(159)
      expect(result.messages[3].source.range.end).toBe(161)
      expect(result.messages[4].message).toBe('Duplicated parameter \'_D1\'')
      expect(result.messages[4].source.range.start).toBe(221)
      expect(result.messages[4].source.range.end).toBe(223)
      expect(result.messages[5].message).toBe('Duplicated parameter \'_D2\'')
      expect(result.messages[5].source.range.start).toBe(231)
      expect(result.messages[5].source.range.end).toBe(233)
    })
  })
})
