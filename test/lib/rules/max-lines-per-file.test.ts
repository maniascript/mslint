import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { maxLinesPerFile } from '../../../src/lib/rules/max-lines-per-file.js'

describe('lib/rules/max-lines-per-file.ts', () => {
  describe('Enforce a maximum number of lines per file', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA() {

        }
        main() {

        }
      `, maxLinesPerFile)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('file is too long', async () => {
        const result = await testRule(`
          Void FunctionA() {

          }
          main() {

          }
        `, maxLinesPerFile, { maximum: 4 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('File has too many lines (6 > 4)')

        expect(result.messages[0].source.range.start).toBe(11)
        expect(result.messages[0].source.range.end).toBe(73)
      })
    })
  })
})
