import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noNegatedCondition } from '../../../src/lib/rules/no-negated-condition.js'

describe('lib/rules/no-negated-condition.ts', () => {
  describe('Forbid negated conditions', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          if (A) {}
          if (B) {} else {}
          if (!C) {}
          if (!D) {} else if (E) {}
          if (!F) {} else if (G) {} else {}
          if (H == I) {}
          if (J == K) {} else {}
          if (L != M) {}
          if (N != O) {} else if (P) {}
          if (Q != R) {} else if (S) {} else {}
        }
      `, noNegatedCondition)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('has negated condition', async () => {
        const result = await testRule(`
          main() {
            if (!A) {} else {}
            if (B != C) {} else {}
          }
        `, noNegatedCondition)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(2)
        expect(result.messages[0].message).toBe('Forbidden negated condition')
        expect(result.messages[1].message).toBe('Forbidden negated condition')

        expect(result.messages[0].source.range.start).toBe(36)
        expect(result.messages[0].source.range.end).toBe(37)
        expect(result.messages[1].source.range.start).toBe(67)
        expect(result.messages[1].source.range.end).toBe(72)
      })
    })
  })
})
