import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { deprecatedInitializerType } from '../../../src/lib/rules/deprecated-initializer-type.js'

describe('lib/rules/trim-array-type.ts', () => {
  test('Find valid value use', async () => {
    const result = await testRule(`
      Void Function_01(Integer _Param1) {
        return 1;
      }

      main() {
        declare Integer Variable_01 = 2;
        Variable_01 = 3;
        Function_01(4);
      }
    `, deprecatedInitializerType)
    expect(result.success).toBe(true)
  })
  test('Find invalid type as value use', async () => {
    const result = await testRule(`
      Void Function_01(Integer _Param1) {
        return Integer;
      }

      main() {
        declare Integer Variable_01 = Integer;
        Variable_01 = Integer;
        Function_01(Integer);
      }
    `, deprecatedInitializerType)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(4)
    for (const message of result.messages) {
      expect(message.message).toBe('Using a type as value is deprecated')
    }

    expect(result.messages[0].source.range.start).toBe(58)
    expect(result.messages[0].source.range.end).toBe(64)
    expect(result.messages[1].source.range.start).toBe(129)
    expect(result.messages[1].source.range.end).toBe(135)
    expect(result.messages[2].source.range.start).toBe(160)
    expect(result.messages[2].source.range.end).toBe(166)
    expect(result.messages[3].source.range.start).toBe(189)
    expect(result.messages[3].source.range.end).toBe(195)
  })
})
