import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { namingConventionCommand } from '../../../src/lib/rules/naming-convention-command.js'

describe('lib/rules/naming-convention-command.ts', () => {
  describe('A command name must be prefixed with \'Command_\'', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Command Command_A (Boolean)
        #Command Command_B (Integer) as "B"
      `, namingConventionCommand)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
      #Command A (Boolean)
      #Command Com_B (Integer) as "B"
      `, namingConventionCommand)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(2)
      for (const message of result.messages) {
        expect(message.message).toBe('A command name must be prefixed with \'Command_\'')
      }
      expect(result.messages[0].source.range.start).toBe(16)
      expect(result.messages[0].source.range.end).toBe(16)
      expect(result.messages[1].source.range.start).toBe(43)
      expect(result.messages[1].source.range.end).toBe(47)
    })
  })
})
