import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { namingConventionConstant } from '../../../src/lib/rules/naming-convention-constant.js'

describe('lib/rules/naming-convention-constant.ts', () => {
  test('Find valid constant name', async () => {
    const result = await testRule(`
      #Const C_ValidConst 0
      #Const Lib_C_ValidConst 1
    `, namingConventionConstant)

    expect(result.success).toBe(true)
  })
  test('Find invalid constant name', async () => {
    const result = await testRule(`
      #Const InvalidConst 0
      #Const CC_InvalidConst 0
      #Const A::InvalidA as InvalidA
    `, namingConventionConstant)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(3)
  })
  test('Ignore special names', async () => {
    const result = await testRule(`
      #Const CompatibleMapTypes ""
      #Const Version ""
      #Const ScriptName ""
      #Const Error ""
      #Const Description ""
      #Const CustomMusicFolder ""
    `, namingConventionConstant)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(1)
  })
  test('Ignore exceptions', async () => {
    const result = await testRule(`
      #Const AAA ""
      #Const Error ""
      #Const BBB ""
    `, namingConventionConstant, { exceptions: ['AAA', 'BBB'] })

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(1)
  })
  test('Report the correct position', async () => {
    const result = await testRule(`
      #Const InvalidConst 0
      #Const A::InvalidA as InvalidA
    `, namingConventionConstant)

    expect(result.success).toBe(false)
    expect(result.messages[0].source.range.start).toBe(14)
    expect(result.messages[0].source.range.end).toBe(25)
    expect(result.messages[1].source.range.start).toBe(57)
    expect(result.messages[1].source.range.end).toBe(64)
  })
})
