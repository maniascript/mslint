import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { idDenylist } from '../../../src/lib/rules/id-denylist.js'

describe('lib/rules/id-denylist.ts', () => {
  describe('Forbid the use of the specified identifiers', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Setting S_SettingG 1
        #Command Command_H (Boolean)
        #Const NamespaceI::C_ConstantJ as C_ConstantK
        #Const C_ConstantL 2
        #Struct NamespaceM::K_StructureN as K_StructureO
        #Struct K_StructureP {
          Integer MemberQ;
        }
        Void FunctionA(Integer _ParameterR) {}
        main() {
          declare Integer[] VariableB;
          AliasC::FunctionE();
          +++LabelF+++
          foreach (KeyS => ValueT in VariableB) {}
          for (ValueU, 0, 10) {}
        }
      `, idDenylist)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('find forbidden identifier', async () => {
        const list = [
          'S_SettingG',
          'Command_H',
          'NamespaceI',
          'C_ConstantJ',
          'C_ConstantK',
          'C_ConstantL',
          'NamespaceM',
          'K_StructureN',
          'K_StructureO',
          'K_StructureP',
          'MemberQ',
          'FunctionA',
          '_ParameterR',
          'VariableB',
          'AliasC',
          'FunctionE',
          'LabelF',
          'KeyS',
          'ValueT',
          'ValueU'
        ]
        const result = await testRule(`
          #Setting S_SettingG 1
          #Command Command_H (Boolean)
          #Const NamespaceI::C_ConstantJ as C_ConstantK
          #Const C_ConstantL 2
          #Struct NamespaceM::K_StructureN as K_StructureO
          #Struct K_StructureP {
            Integer MemberQ;
          }
          Void FunctionA(Integer _ParameterR) {}
          main() {
            declare Integer[] VariableB;
            AliasC::FunctionE();
            +++LabelF+++
            foreach (KeyS => ValueT in VariableB) {}
            for (ValueU, 0, 10) {}
          }
        `, idDenylist, { list })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(14)

        expect(result.messages[0].message).toBe('Identifier name \'S_SettingG\' is forbidden')
        expect(result.messages[1].message).toBe('Identifier name \'Command_H\' is forbidden')
        expect(result.messages[2].message).toBe('Identifier name \'C_ConstantK\' is forbidden')
        expect(result.messages[3].message).toBe('Identifier name \'C_ConstantL\' is forbidden')
        expect(result.messages[4].message).toBe('Identifier name \'K_StructureO\' is forbidden')
        expect(result.messages[5].message).toBe('Identifier name \'K_StructureP\' is forbidden')
        expect(result.messages[6].message).toBe('Identifier name \'MemberQ\' is forbidden')
        expect(result.messages[7].message).toBe('Identifier name \'FunctionA\' is forbidden')
        expect(result.messages[8].message).toBe('Identifier name \'_ParameterR\' is forbidden')
        expect(result.messages[9].message).toBe('Identifier name \'VariableB\' is forbidden')
        expect(result.messages[10].message).toBe('Identifier name \'LabelF\' is forbidden')
        expect(result.messages[11].message).toBe('Identifier name \'KeyS\' is forbidden')
        expect(result.messages[12].message).toBe('Identifier name \'ValueT\' is forbidden')
        expect(result.messages[13].message).toBe('Identifier name \'ValueU\' is forbidden')

        expect(result.messages[0].source.range.start).toBe(20)
        expect(result.messages[0].source.range.end).toBe(29)
        expect(result.messages[1].source.range.start).toBe(52)
        expect(result.messages[1].source.range.end).toBe(60)
        expect(result.messages[2].source.range.start).toBe(116)
        expect(result.messages[2].source.range.end).toBe(126)
        expect(result.messages[3].source.range.start).toBe(145)
        expect(result.messages[3].source.range.end).toBe(155)
        expect(result.messages[4].source.range.start).toBe(205)
        expect(result.messages[4].source.range.end).toBe(216)
        expect(result.messages[5].source.range.start).toBe(236)
        expect(result.messages[5].source.range.end).toBe(247)
        expect(result.messages[6].source.range.start).toBe(271)
        expect(result.messages[6].source.range.end).toBe(277)
        expect(result.messages[7].source.range.start).toBe(307)
        expect(result.messages[7].source.range.end).toBe(315)
        expect(result.messages[8].source.range.start).toBe(325)
        expect(result.messages[8].source.range.end).toBe(335)
        expect(result.messages[9].source.range.start).toBe(390)
        expect(result.messages[9].source.range.end).toBe(398)
        expect(result.messages[10].source.range.start).toBe(449)
        expect(result.messages[10].source.range.end).toBe(454)
        expect(result.messages[11].source.range.start).toBe(480)
        expect(result.messages[11].source.range.end).toBe(483)
        expect(result.messages[12].source.range.start).toBe(488)
        expect(result.messages[12].source.range.end).toBe(493)
        expect(result.messages[13].source.range.start).toBe(529)
        expect(result.messages[13].source.range.end).toBe(534)
      })
    })
  })
})
