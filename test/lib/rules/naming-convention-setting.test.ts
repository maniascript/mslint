import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { namingConventionSetting } from '../../../src/lib/rules/naming-convention-setting.js'

describe('lib/rules/naming-convention-setting.ts', () => {
  describe('A setting name must be prefixed with \'S_\'', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Setting S_SettingA 0
        #Setting S_SettingB 0 as "B"
      `, namingConventionSetting)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
      #Setting SettingA 0
      #Setting SettingB 0 as "B"
      `, namingConventionSetting)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(2)
      for (const message of result.messages) {
        expect(message.message).toBe('A setting name must be prefixed with \'S_\'')
      }
      expect(result.messages[0].source.range.start).toBe(16)
      expect(result.messages[0].source.range.end).toBe(23)
      expect(result.messages[1].source.range.start).toBe(42)
      expect(result.messages[1].source.range.end).toBe(49)
    })
  })
})
