import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noUnusedVariables } from '../../../src/lib/rules/no-unused-variables.js'

describe('lib/rules/no-unused-variables.ts', () => {
  describe('Forbid unused variables', () => {
    test('is valid', async () => {
      const result = await testRule(`
        declare Integer G_A;
        declare Integer G_B;
        declare Integer G_C;

        Real FunctionA(Integer _A, Real _B) {
          return _A + _B;
        }

        Void FunctionB(Integer _E, Vec3 _F) {
          +++LabelA+++
        }

        Text FunctionC(Integer _G, Integer _H, Integer _I, Integer _J, Integer _K) {
          declare K_StructA StructA = K_StructA {
            G = _G,
            H = _H,
            I = _I,
            J = _J,
            K = _K
          };
          return StructA.tojson();
        }

        ***LabelA***
        ***
        log(G_C);
        ***

        Void FunctionD(Integer _L) {
          A = _L;
        }

        Void FunctionE() {
          log(G_B);
        }

        main() {
          declare Integer C;
          C = 10;
          declare Integer D = FunctionA(C, 10.);
          log(D);

          for (E, 0, 10) {

          }
          declare Integer[Integer] F;
          foreach (G => H in F) {
            log(G^H);
          }

          declare Integer I for This;
          I = 10;
        }
      `, noUnusedVariables)
      expect(result.success).toBe(true)
    })
    describe('is invalid', () => {
      test('function parameters must be used', async () => {
        const result = await testRule(`
          Integer FunctionA(Integer _A, Integer _B, Integer _C) {
            return _B + 10;
          }
          Void FunctionB(Integer _D) {
            _D();
            LibA::_D;
            _D = 10;
            _D += 10;
          }
        `, noUnusedVariables)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(3)
        expect(result.messages[0].message).toBe('Function parameter \'_A\' is never used')
        expect(result.messages[1].message).toBe('Function parameter \'_C\' is never used')
        expect(result.messages[2].message).toBe('Function parameter \'_D\' is never read')

        expect(result.messages[0].source.range.start).toBe(37)
        expect(result.messages[0].source.range.end).toBe(38)
        expect(result.messages[1].source.range.start).toBe(61)
        expect(result.messages[1].source.range.end).toBe(62)
        expect(result.messages[2].source.range.start).toBe(140)
        expect(result.messages[2].source.range.end).toBe(141)
      })

      test('declared variables must be used', async () => {
        const result = await testRule(`
          declare Integer G_A;
          declare Integer G_B;

          ***Label_A***
          ***
          declare Integer E;
          {
            declare Integer F;
          }
          foreach (G in H) {}
          ***

          main() {
            declare Integer C;
            declare Integer D;
            G_B += 1;
            D = 1;
          }
        `, noUnusedVariables)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(6)
        expect(result.messages[0].message).toBe('Global variable \'G_A\' is never used')
        expect(result.messages[1].message).toBe('Global variable \'G_B\' is never read')
        expect(result.messages[2].message).toBe('Variable \'F\' is never used')
        expect(result.messages[3].message).toBe('Foreach value \'G\' is never used')
        expect(result.messages[4].message).toBe('Variable \'C\' is never used')
        expect(result.messages[5].message).toBe('Variable \'D\' is never read')

        expect(result.messages[0].source.range.start).toBe(27)
        expect(result.messages[0].source.range.end).toBe(29)
        expect(result.messages[1].source.range.start).toBe(58)
        expect(result.messages[1].source.range.end).toBe(60)
        expect(result.messages[2].source.range.start).toBe(171)
        expect(result.messages[2].source.range.end).toBe(171)
        expect(result.messages[3].source.range.start).toBe(205)
        expect(result.messages[3].source.range.end).toBe(205)
        expect(result.messages[4].source.range.start).toBe(278)
        expect(result.messages[4].source.range.end).toBe(278)
        expect(result.messages[5].source.range.start).toBe(309)
        expect(result.messages[5].source.range.end).toBe(309)
      })

      test('loop variables must be used', async () => {
        const result = await testRule(`
          main() {
            foreach (A => B in C) {}
            foreach (D => E in F) {
              log(E);
            }
            foreach (G => H in I) {
              log(G);
            }
            foreach (J in K) {}
          }
        `, noUnusedVariables)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(4)
        expect(result.messages[0].message).toBe('Foreach key \'A\' is never used')
        expect(result.messages[1].message).toBe('Foreach value \'B\' is never used')
        expect(result.messages[2].message).toBe('Foreach key \'D\' is never used')
        expect(result.messages[3].message).toBe('Foreach value \'J\' is never used')

        expect(result.messages[0].source.range.start).toBe(41)
        expect(result.messages[0].source.range.end).toBe(41)
        expect(result.messages[1].source.range.start).toBe(46)
        expect(result.messages[1].source.range.end).toBe(46)
        expect(result.messages[2].source.range.start).toBe(78)
        expect(result.messages[2].source.range.end).toBe(78)
        expect(result.messages[3].source.range.start).toBe(222)
        expect(result.messages[3].source.range.end).toBe(222)
      })
    })
  })
})
