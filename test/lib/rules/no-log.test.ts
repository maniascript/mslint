import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noLog } from '../../../src/lib/rules/no-log.js'

describe('lib/rules/no-log.ts', () => {
  describe('Forbid the use of the log() function', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void Log(Integer _Value) {}
        main() {
          Log(Now);
        }
      `, noLog)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('forbidden log() function used', async () => {
        const result = await testRule(`
          main() {
            log(Now);
          }
        `, noLog)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('The use of the \'log()\' function is forbidden')

        expect(result.messages[0].source.range.start).toBe(32)
        expect(result.messages[0].source.range.end).toBe(40)
      })
    })
  })
})
