import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { trimArrayType } from '../../../src/lib/rules/trim-array-type.js'

describe('lib/rules/trim-array-type.ts', () => {
  test('Find valid array type', async () => {
    const result = await testRule('declare Integer[][Real] Test;', trimArrayType)
    expect(result.success).toBe(true)
  })
  test('Find invalid array type', async () => {
    const result = await testRule(`
      declare Integer[ ][Real] Test;
      declare Integer[] [Real] Test;
      declare Integer [][Real] Test;
      declare Integer[  ][Real] Test;
      declare Integer[]\n[Real] Test;
    `, trimArrayType)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(5)
    expect(result.messages[0].message).toBe('An array type must not contain any whitespace characters')
    expect(result.messages[1].message).toBe('An array type must not contain any whitespace characters')
    expect(result.messages[2].message).toBe('An array type must not contain any whitespace characters')
    expect(result.messages[3].message).toBe('An array type must not contain any whitespace characters')
    expect(result.messages[4].message).toBe('An array type must be on a single line')
  })
  test('Report the correct position', async () => {
    const result = await testRule('declare Integer[ ][Real] Test;', trimArrayType)

    expect(result.messages[0].source.range.start).toBe(8)
    expect(result.messages[0].source.range.end).toBe(23)
  })
})
