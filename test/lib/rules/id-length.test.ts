import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { idLength } from '../../../src/lib/rules/id-length.js'

describe('lib/rules/id-length.ts', () => {
  describe('Enforce minimum and maximum lengths for identifiers', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Setting S_SettingG 1
        #Command Command_H (Boolean)
        #Const NamespaceI::C_ConstantJ as C_ConstantK
        #Const C_ConstantL 2
        #Struct NamespaceM::K_StructureN as K_StructureO
        #Struct K_StructureP {
          Integer MemberQ;
        }
        Void FunctionA(Integer _ParameterR) {}
        main() {
          declare Integer[] VariableB;
          AliasC::FunctionE();
          +++LabelF+++
          foreach (KeyS => ValueT in VariableB) {}
          for (ValueU, 0, 10) {}
        }
      `, idLength)
      expect(result.success).toBe(true)
    })

    test('ignore short identifiers in for loop', async () => {
      const result = await testRule(`
        main() {
          for (I, 0, 10) {}
        }
      `, idLength, { minimum: 2 })

      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('too long', async () => {
        const result = await testRule(`
          #Setting S_SettingG 1
          #Command Command_H (Boolean)
          #Const NamespaceI::C_ConstantJ as C_ConstantK
          #Const C_ConstantL 2
          #Struct NamespaceM::K_StructureN as K_StructureO
          #Struct K_StructureP {
            Integer MemberQ;
          }
          Void FunctionA(Integer _ParameterR) {}
          main() {
            declare Integer[] VariableB;
            AliasC::FunctionE();
            +++LabelF+++
            foreach (KeyS => ValueT in VariableB) {}
            for (ValueU, 0, 10) {}
          }
        `, idLength, { maximum: 3 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(14)
        expect(result.messages[0].message).toBe('Identifier name \'S_SettingG\' is too long (10 > 3)')
        expect(result.messages[1].message).toBe('Identifier name \'Command_H\' is too long (9 > 3)')
        expect(result.messages[2].message).toBe('Identifier name \'C_ConstantK\' is too long (11 > 3)')
        expect(result.messages[3].message).toBe('Identifier name \'C_ConstantL\' is too long (11 > 3)')
        expect(result.messages[4].message).toBe('Identifier name \'K_StructureO\' is too long (12 > 3)')
        expect(result.messages[5].message).toBe('Identifier name \'K_StructureP\' is too long (12 > 3)')
        expect(result.messages[6].message).toBe('Identifier name \'MemberQ\' is too long (7 > 3)')
        expect(result.messages[7].message).toBe('Identifier name \'FunctionA\' is too long (9 > 3)')
        expect(result.messages[8].message).toBe('Identifier name \'_ParameterR\' is too long (11 > 3)')
        expect(result.messages[9].message).toBe('Identifier name \'VariableB\' is too long (9 > 3)')
        expect(result.messages[10].message).toBe('Identifier name \'LabelF\' is too long (6 > 3)')
        expect(result.messages[11].message).toBe('Identifier name \'KeyS\' is too long (4 > 3)')
        expect(result.messages[12].message).toBe('Identifier name \'ValueT\' is too long (6 > 3)')
        expect(result.messages[13].message).toBe('Identifier name \'ValueU\' is too long (6 > 3)')

        expect(result.messages[0].source.range.start).toBe(20)
        expect(result.messages[0].source.range.end).toBe(29)
        expect(result.messages[1].source.range.start).toBe(52)
        expect(result.messages[1].source.range.end).toBe(60)
        expect(result.messages[2].source.range.start).toBe(116)
        expect(result.messages[2].source.range.end).toBe(126)
        expect(result.messages[3].source.range.start).toBe(145)
        expect(result.messages[3].source.range.end).toBe(155)
        expect(result.messages[4].source.range.start).toBe(205)
        expect(result.messages[4].source.range.end).toBe(216)
        expect(result.messages[5].source.range.start).toBe(236)
        expect(result.messages[5].source.range.end).toBe(247)
        expect(result.messages[6].source.range.start).toBe(271)
        expect(result.messages[6].source.range.end).toBe(277)
        expect(result.messages[7].source.range.start).toBe(307)
        expect(result.messages[7].source.range.end).toBe(315)
        expect(result.messages[8].source.range.start).toBe(325)
        expect(result.messages[8].source.range.end).toBe(335)
        expect(result.messages[9].source.range.start).toBe(390)
        expect(result.messages[9].source.range.end).toBe(398)
        expect(result.messages[10].source.range.start).toBe(449)
        expect(result.messages[10].source.range.end).toBe(454)
        expect(result.messages[11].source.range.start).toBe(480)
        expect(result.messages[11].source.range.end).toBe(483)
        expect(result.messages[12].source.range.start).toBe(488)
        expect(result.messages[12].source.range.end).toBe(493)
        expect(result.messages[13].source.range.start).toBe(529)
        expect(result.messages[13].source.range.end).toBe(534)
      })

      test('too short', async () => {
        const result = await testRule(`
          #Setting S_SettingG 1
          #Command Command_H (Boolean)
          #Const NamespaceI::C_ConstantJ as C_ConstantK
          #Const C_ConstantL 2
          #Struct NamespaceM::K_StructureN as K_StructureO
          #Struct K_StructureP {
            Integer MemberQ;
          }
          Void FunctionA(Integer _ParameterR) {}
          main() {
            declare Integer[] VariableB;
            AliasC::FunctionE();
            +++LabelF+++
            foreach (KeyS => ValueT in VariableB) {}
            for (ValueU, 0, 10) {}
          }
        `, idLength, { minimum: 100 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(13)
        expect(result.messages[0].message).toBe('Identifier name \'S_SettingG\' is too short (10 < 100)')
        expect(result.messages[1].message).toBe('Identifier name \'Command_H\' is too short (9 < 100)')
        expect(result.messages[2].message).toBe('Identifier name \'C_ConstantK\' is too short (11 < 100)')
        expect(result.messages[3].message).toBe('Identifier name \'C_ConstantL\' is too short (11 < 100)')
        expect(result.messages[4].message).toBe('Identifier name \'K_StructureO\' is too short (12 < 100)')
        expect(result.messages[5].message).toBe('Identifier name \'K_StructureP\' is too short (12 < 100)')
        expect(result.messages[6].message).toBe('Identifier name \'MemberQ\' is too short (7 < 100)')
        expect(result.messages[7].message).toBe('Identifier name \'FunctionA\' is too short (9 < 100)')
        expect(result.messages[8].message).toBe('Identifier name \'_ParameterR\' is too short (11 < 100)')
        expect(result.messages[9].message).toBe('Identifier name \'VariableB\' is too short (9 < 100)')
        expect(result.messages[10].message).toBe('Identifier name \'LabelF\' is too short (6 < 100)')
        expect(result.messages[11].message).toBe('Identifier name \'KeyS\' is too short (4 < 100)')
        expect(result.messages[12].message).toBe('Identifier name \'ValueT\' is too short (6 < 100)')

        expect(result.messages[0].source.range.start).toBe(20)
        expect(result.messages[0].source.range.end).toBe(29)
        expect(result.messages[1].source.range.start).toBe(52)
        expect(result.messages[1].source.range.end).toBe(60)
        expect(result.messages[2].source.range.start).toBe(116)
        expect(result.messages[2].source.range.end).toBe(126)
        expect(result.messages[3].source.range.start).toBe(145)
        expect(result.messages[3].source.range.end).toBe(155)
        expect(result.messages[4].source.range.start).toBe(205)
        expect(result.messages[4].source.range.end).toBe(216)
        expect(result.messages[5].source.range.start).toBe(236)
        expect(result.messages[5].source.range.end).toBe(247)
        expect(result.messages[6].source.range.start).toBe(271)
        expect(result.messages[6].source.range.end).toBe(277)
        expect(result.messages[7].source.range.start).toBe(307)
        expect(result.messages[7].source.range.end).toBe(315)
        expect(result.messages[8].source.range.start).toBe(325)
        expect(result.messages[8].source.range.end).toBe(335)
        expect(result.messages[9].source.range.start).toBe(390)
        expect(result.messages[9].source.range.end).toBe(398)
        expect(result.messages[10].source.range.start).toBe(449)
        expect(result.messages[10].source.range.end).toBe(454)
        expect(result.messages[11].source.range.start).toBe(480)
        expect(result.messages[11].source.range.end).toBe(483)
        expect(result.messages[12].source.range.start).toBe(488)
        expect(result.messages[12].source.range.end).toBe(493)
      })

      test('ignore exceptions', async () => {
        const result = await testRule(`
          #Const A 1
          #Const B 2
          #Const VeryLongConstantNameC 3
          #Const VeryLongConstantNameD 4
        `, idLength, { minimum: 5, maximum: 10, exceptions: ['A', 'VeryLongConstantNameC'] })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(2)
        expect(result.messages[0].message).toBe('Identifier name \'B\' is too short (1 < 5)')
        expect(result.messages[1].message).toBe('Identifier name \'VeryLongConstantNameD\' is too long (21 > 10)')

        expect(result.messages[0].source.range.start).toBe(39)
        expect(result.messages[0].source.range.end).toBe(39)
        expect(result.messages[1].source.range.start).toBe(101)
        expect(result.messages[1].source.range.end).toBe(121)
      })
    })
  })
})
