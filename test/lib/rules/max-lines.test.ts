import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { maxLines } from '../../../src/lib/rules/max-lines.js'

describe('lib/rules/max-lines.ts', () => {
  describe('Enforce a maximum number of lines per function', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA() {

        }
        ***LabelB***
        ***
        log(Now);
        ***
        main() {

        }
      `, maxLines)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('function is too long', async () => {
        const result = await testRule(`
          Void FunctionA() {
            // This
            // function
            // is
            // too
            // long
          }
          ***LabelB***
          ***
          // This
          // label
          // is
          // far
          // too
          // long
          log(Now);
          ***
          main() {
            // This
            // main
            // is
            // too
            // long
            // for
            // real
          }
        `, maxLines, { maximum: { function: 5, label: 6, main: 7 } })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(3)
        expect(result.messages[0].message).toBe('Function \'FunctionA()\' has too many lines (7 > 5)')
        expect(result.messages[1].message).toBe('Label \'***LabelB***\' has too many lines (10 > 6)')
        expect(result.messages[2].message).toBe('Function \'main()\' has too many lines (9 > 7)')

        expect(result.messages[0].source.range.start).toBe(11)
        expect(result.messages[0].source.range.end).toBe(141)
        expect(result.messages[1].source.range.start).toBe(153)
        expect(result.messages[1].source.range.end).toBe(317)
        expect(result.messages[2].source.range.start).toBe(329)
        expect(result.messages[2].source.range.end).toBe(484)
      })
    })
  })
})
