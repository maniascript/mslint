import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { _template } from '../../../src/lib/rules/_template.js'

describe('lib/rules/_template_id.ts', () => {
  describe('_template_description', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA() {

        }
        main() {

        }
      `, _template)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('_template_test', async () => {
        const result = await testRule(`
          Void FunctionA() {

          }
          main() {

          }
        `, _template, { maximum: 15 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('_template_error')

        expect(result.messages[0].source.range.start).toBe(53)
        expect(result.messages[0].source.range.end).toBe(73)
      })
    })
  })
})
