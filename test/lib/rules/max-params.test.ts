import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { maxParams } from '../../../src/lib/rules/max-params.js'

describe('lib/rules/max-params.ts', () => {
  describe('Enforce a maximum number of parameters in function declarations', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA() {

        }
        Void FunctionB(Integer _ParamC, Integer _ParamD) {

        }
      `, maxParams)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('too many params', async () => {
        const result = await testRule(`
          Void FunctionA(Integer _ParamB, Integer _ParamC) {}
          Void FunctionD(Integer _ParamE, Integer _ParamF, Integer _ParamG, Integer _ParamH) {}
        `, maxParams, { maximum: 2 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('Function \'FunctionD()\' has too many parameters (4 > 2)')

        expect(result.messages[0].source.range.start).toBe(73)
        expect(result.messages[0].source.range.end).toBe(157)
      })
    })
  })
})
