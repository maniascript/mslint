import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { useTrackInsteadOfMap } from '../../../src/lib/rules/use-track-instead-of-map.js'

describe('lib/rules/use-track-instead-of-map.ts', () => {
  describe('Use the word track instead of map', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Const A _("This is a track and it is ok")
        #Const B _("These are tracks and it is ok")
        #Const C _("Mapping")
        #Const D _("AAmapBB")
        #Const E _("Map type")
      `, useTrackInsteadOfMap)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        #Const A _("This is a map and it is not ok")
        #Const B _("These are maps and it is not ok")
        #Const C _("Map")
        #Const D _("mAPs")
        #Const E _("map maps map")
      `, useTrackInsteadOfMap)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(5)
      for (const message of result.messages) {
        expect(message.message).toBe('Use the word track instead of map')
      }
      expect(result.messages[0].source.range.start).toBe(18)
      expect(result.messages[0].source.range.end).toBe(52)
      expect(result.messages[1].source.range.start).toBe(71)
      expect(result.messages[1].source.range.end).toBe(106)
      expect(result.messages[2].source.range.start).toBe(125)
      expect(result.messages[2].source.range.end).toBe(132)
      expect(result.messages[3].source.range.start).toBe(151)
      expect(result.messages[3].source.range.end).toBe(159)
      expect(result.messages[4].source.range.start).toBe(178)
      expect(result.messages[4].source.range.end).toBe(194)
    })
    test('check only translated text', async () => {
      const results = await Promise.all([
        testRule(`
          #Const A "This is a map and it is ok"
        `, useTrackInsteadOfMap, { onlyTranslatedText: true }),
        testRule(`
          #Const A "This is a map and it is not ok"
        `, useTrackInsteadOfMap, { onlyTranslatedText: false })
      ])
      expect(results[0].success).toBe(true)
      expect(results[1].success).toBe(false)
    })
  })
})
