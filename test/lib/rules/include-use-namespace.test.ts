import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { includeUseNamespace } from '../../../src/lib/rules/include-use-namespace.js'

describe('lib/rules/include-use-namespace.ts', () => {
  describe('You must use a namespace when including a library', () => {
    test('is valid', async () => {
      const result = await testRule('#Include "Path/To/Lib.Script.txt" as Lib', includeUseNamespace)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule('#Include "Path/To/Lib.Script.txt"', includeUseNamespace)

      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(1)
      expect(result.messages[0].message).toBe('You must use a namespace when including a library')

      expect(result.messages[0].source.range.start).toBe(0)
      expect(result.messages[0].source.range.end).toBe(32)
    })
  })
  describe('This namespace is already used by another include', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Include "Path/To/LibA.Script.txt" as LibA
        #Include "Path/To/LibB.Script.txt" as LibB
      `, includeUseNamespace)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        #Include "Path/To/LibA.Script.txt" as LibA
        #Include "Path/To/LibB.Script.txt" as LibA
      `, includeUseNamespace)

      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(1)
      expect(result.messages[0].message).toBe('This namespace is already used by another include')

      expect(result.messages[0].source.range.start).toBe(98)
      expect(result.messages[0].source.range.end).toBe(101)
    })
  })
})
