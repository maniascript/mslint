import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { maxDepth } from '../../../src/lib/rules/max-depth.js'

describe('lib/rules/max-depth.ts', () => {
  describe('Check variable declaration initialization', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA() {
          if (True) {}
        }

        ***LabelA***
        ***
        if (True) {}
        ***

        main() {
          if (True) {}
        }
      `, maxDepth)
      expect(result.success).toBe(true)
    })
    describe('is invalid', () => {
      test('max depth exceeded', async () => {
        const result = await testRule(`
          Void FunctionA() {
            if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { } } } } } } } } } }
          }

          ***LabelA***
          ***
          if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { } } } } } } } } } }
          ***

          main() {
            if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { if (True) { if (False) { } } } } } } } } } }
          }
        `, maxDepth)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(6)
        expect(result.messages[0].message).toBe('Blocks are nested too deeply. Depth 9 while maximum allowed is 8.')
        expect(result.messages[1].message).toBe('Blocks are nested too deeply. Depth 10 while maximum allowed is 8.')
        expect(result.messages[2].message).toBe('Blocks are nested too deeply. Depth 9 while maximum allowed is 8.')
        expect(result.messages[3].message).toBe('Blocks are nested too deeply. Depth 10 while maximum allowed is 8.')
        expect(result.messages[4].message).toBe('Blocks are nested too deeply. Depth 9 while maximum allowed is 8.')
        expect(result.messages[5].message).toBe('Blocks are nested too deeply. Depth 10 while maximum allowed is 8.')

        expect(result.messages[0].source.range.start).toBe(142)
        expect(result.messages[0].source.range.end).toBe(169)
        expect(result.messages[1].source.range.start).toBe(154)
        expect(result.messages[1].source.range.end).toBe(167)
        expect(result.messages[2].source.range.start).toBe(347)
        expect(result.messages[2].source.range.end).toBe(374)
        expect(result.messages[3].source.range.start).toBe(359)
        expect(result.messages[3].source.range.end).toBe(372)
        expect(result.messages[4].source.range.start).toBe(538)
        expect(result.messages[4].source.range.end).toBe(565)
        expect(result.messages[5].source.range.start).toBe(550)
        expect(result.messages[5].source.range.end).toBe(563)
      })

      test('detect all kinds of blocks', async () => {
        const result = await testRule(`
          main() {
            foreach (A in B) {
              for (I, 0, 10) {
                while (True) {
                  meanwhile (True) {
                    switch (C) {
                      case 1: {
                        switchtype (D) {
                          case CSmMode: {
                            if (True) {
                              {
                                {
                                  log("deep");
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            foreach (A in B)
              for (I, 0, 10)
                while (True)
                  meanwhile (True)
                    switch (C) {
                      case 1:
                        switchtype (D) {
                          case CSmMode:
                            if (True) { { { log("deep"); } } }
                        }
                    }

            if (True) {
              if (False) {
                if (True) {
                  if (False) {
                    if (True) {
                      if (False) {
                        if (True) {
                          if (False) {
                            if (True) {
                              log("deep");
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        `, maxDepth)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(3)
        expect(result.messages[0].message).toBe('Blocks are nested too deeply. Depth 9 while maximum allowed is 8.')
        expect(result.messages[1].message).toBe('Blocks are nested too deeply. Depth 9 while maximum allowed is 8.')
        expect(result.messages[2].message).toBe('Blocks are nested too deeply. Depth 9 while maximum allowed is 8.')

        expect(result.messages[0].source.range.start).toBe(402)
        expect(result.messages[0].source.range.end).toBe(483)
        expect(result.messages[1].source.range.start).toBe(1024)
        expect(result.messages[1].source.range.end).toBe(1039)
        expect(result.messages[2].source.range.start).toBe(1374)
        expect(result.messages[2].source.range.end).toBe(1457)
      })

      test('accept a setting', async () => {
        const result = await testRule(`
          Void FunctionA() {
            {
              {

              }
            }
          }
          main() {
            {
              {

              }
            }
          }
        `, maxDepth, { maximum: 1 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(2)
        expect(result.messages[0].message).toBe('Blocks are nested too deeply. Depth 2 while maximum allowed is 1.')
        expect(result.messages[1].message).toBe('Blocks are nested too deeply. Depth 2 while maximum allowed is 1.')

        expect(result.messages[0].source.range.start).toBe(58)
        expect(result.messages[0].source.range.end).toBe(75)
        expect(result.messages[1].source.range.start).toBe(150)
        expect(result.messages[1].source.range.end).toBe(167)
      })
    })
  })
})
