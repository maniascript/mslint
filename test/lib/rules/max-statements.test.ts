import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { maxStatements } from '../../../src/lib/rules/max-statements.js'

describe('lib/rules/max-statements.ts', () => {
  describe('Enforce a maximum number of statements in functions', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA() {

        }
        main() {

        }
      `, maxStatements)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('too many statements', async () => {
        const result = await testRule(`
          Void FunctionA() {
            declare Integer B;
            declare Integer C;
            declare Integer D;
          }
          ***LabelE***
          ***
          declare Integer F;
          declare Integer G;
          declare Integer H;
          declare Integer I;
          ***
          main() {
            declare Integer J;
            declare Integer K;
            declare Integer L;
            declare Integer M;
            declare Integer N;
          }
        `, maxStatements, { maximum: { function: 2, label: 3, main: 4 } })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(3)
        expect(result.messages[0].message).toBe('Function \'FunctionA()\' contains too many statements (3 > 2)')
        expect(result.messages[1].message).toBe('Label \'***LabelE***\' contains too many statements (4 > 3)')
        expect(result.messages[2].message).toBe('Function \'main()\' contains too many statements (5 > 4)')

        expect(result.messages[0].source.range.start).toBe(11)
        expect(result.messages[0].source.range.end).toBe(133)
        expect(result.messages[1].source.range.start).toBe(145)
        expect(result.messages[1].source.range.end).toBe(300)
        expect(result.messages[2].source.range.start).toBe(312)
        expect(result.messages[2].source.range.end).toBe(486)
      })

      test('find nested statements', async () => {
        const result = await testRule(`
          main() {
            declare Integer A;
            {
              declare Integer B;
            }
            for (I, 0, 10) {
              declare Integer C;
              foreach (J in K) {
                log(Now);
              }
            }
            while (True) {
              declare Integer D;
            }
            if (True) {
              declare Integer E;
            } else if (False) {
              declare Integer F;
            } else {
              declare Integer G;
            }
          }
        `, maxStatements, { maximum: 2 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('Function \'main()\' contains too many statements (13 > 2)')

        expect(result.messages[0].source.range.start).toBe(11)
        expect(result.messages[0].source.range.end).toBe(537)
      })
    })
  })
})
