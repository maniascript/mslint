import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noSelfAssign } from '../../../src/lib/rules/no-self-assign.js'

describe('lib/rules/no-self-assign.ts', () => {
  describe('Forbid assignments where both sides are the same', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          declare Integer A = A;
          A = B;
          A <=> B;
          A += A;
          C::D = E::D;
        }
      `, noSelfAssign)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        main() {
          A = A;
          A <=> A;
          B::C = B::C;
        }
      `, noSelfAssign)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(3)
      expect(result.messages[0].message).toBe('\'A\' is assigned to itself')
      expect(result.messages[0].source.range.start).toBe(28)
      expect(result.messages[0].source.range.end).toBe(33)
      expect(result.messages[1].message).toBe('\'A\' is assigned to itself')
      expect(result.messages[1].source.range.start).toBe(45)
      expect(result.messages[1].source.range.end).toBe(52)
      expect(result.messages[2].message).toBe('\'B::C\' is assigned to itself')
      expect(result.messages[2].source.range.start).toBe(64)
      expect(result.messages[2].source.range.end).toBe(75)
    })
  })
})
