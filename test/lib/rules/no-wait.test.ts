import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noWait } from '../../../src/lib/rules/no-wait.js'

describe('lib/rules/no-wait.ts', () => {
  describe('Forbid the use of the wait() function', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void Wait(Integer _Value) {}
        main() {
          Wait(Now);
        }
      `, noWait)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('forbidden wait() function used', async () => {
        const result = await testRule(`
          main() {
            wait(Now);
          }
        `, noWait)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('The use of the \'wait()\' function is forbidden')

        expect(result.messages[0].source.range.start).toBe(32)
        expect(result.messages[0].source.range.end).toBe(41)
      })
    })
  })
})
