import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noDupeKeys } from '../../../src/lib/rules/no-dupe-keys.js'

describe('lib/rules/no-dupe-keys.ts', () => {
  describe('Forbid duplicate keys in associative arrays', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          declare Integer[Text] ArrayA = [
            "A" => 1,
            "B" => 2,
            "C" => 3,
            D() => 4,
            D() => 5
          ];
          declare Integer[Text] ArrayABis = [
            "A" => 1,
            "B" => 2,
            "C" => 3,
            D() => 4,
            D() => 5
          ];
          declare Integer[Integer] ArrayB = [
            1 => 1,
            2 => 2,
            3 => 3,
            D() => 4,
            D() => 5
          ];
          declare Integer[Integer][Integer] ArrayC = [
            1 => [10 => 10, 11 => 11],
            2 => [20 => 20, 21 => 21],
            3 => [30 => 30, 31 => 31],
            4 => [30 => 30, 31 => 31],
            D() => [D() => D, D() => D]
          ];
          declare Integer[Integer] ArrayD = [
            <1, 1> => 1,
            <1, 1> => 2,
            <1, 2> => 3
          ];
          declare Integer[Integer] ArrayE = [
            1 + 1 => 1,
            1 + 1 => 2
          ];
        }
      `, noDupeKeys)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        main() {
          declare Integer[Text] ArrayA = [
            "A" => 1,
            "A" => 2
          ];
          declare Integer[Integer] ArrayB = [
            1 => 1,
            1 => 2
          ];
          declare Integer[Integer][Integer] ArrayC = [
            1 => [10 => 10, 10 => 11],
            1 => [10 => 20, 11 => 21]
          ];
          declare Integer[Real] ArrayE = [
            1. => 1,
            1. => 2
          ];
          declare Integer[Boolean] ArrayF = [
            True => 1,
            True => 2
          ];
          declare Integer[CSmPlayer] ArrayG = [
            Null => 1,
            Null => 2
          ];
          declare Integer[Ident] ArrayH = [
            NullId => 1,
            NullId => 2
          ];
          declare Integer[CSmMode::EMedal] ArrayI = [
            CSmMode::EMedal::Gold => 1,
            CSmMode::EMedal::Gold => 2,
            ::EMedal::Silver => 3,
            ::EMedal::Silver => 4
          ];
        }
      `, noDupeKeys)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(10)
      expect(result.messages[0].message).toBe('Duplicate key \'"A"\'')
      expect(result.messages[0].source.range.start).toBe(95)
      expect(result.messages[0].source.range.end).toBe(97)
      expect(result.messages[1].message).toBe('Duplicate key \'1\'')
      expect(result.messages[1].source.range.start).toBe(195)
      expect(result.messages[1].source.range.end).toBe(195)
      expect(result.messages[2].message).toBe('Duplicate key \'10\'')
      expect(result.messages[2].source.range.start).toBe(298)
      expect(result.messages[2].source.range.end).toBe(299)
      expect(result.messages[3].message).toBe('Duplicate key \'1\'')
      expect(result.messages[3].source.range.start).toBe(321)
      expect(result.messages[3].source.range.end).toBe(321)
      expect(result.messages[4].message).toBe('Duplicate key \'1.\'')
      expect(result.messages[4].source.range.start).toBe(436)
      expect(result.messages[4].source.range.end).toBe(437)
      expect(result.messages[5].message).toBe('Duplicate key \'True\'')
      expect(result.messages[5].source.range.start).toBe(538)
      expect(result.messages[5].source.range.end).toBe(541)
      expect(result.messages[6].message).toBe('Duplicate key \'Null\'')
      expect(result.messages[6].source.range.start).toBe(644)
      expect(result.messages[6].source.range.end).toBe(647)
      expect(result.messages[7].message).toBe('Duplicate key \'NullId\'')
      expect(result.messages[7].source.range.start).toBe(748)
      expect(result.messages[7].source.range.end).toBe(753)
      expect(result.messages[8].message).toBe('Duplicate key \'CSmMode::EMedal::Gold\'')
      expect(result.messages[8].source.range.start).toBe(879)
      expect(result.messages[8].source.range.end).toBe(899)
      expect(result.messages[9].message).toBe('Duplicate key \'::EMedal::Silver\'')
      expect(result.messages[9].source.range.start).toBe(954)
      expect(result.messages[9].source.range.end).toBe(969)
    })
  })
})
