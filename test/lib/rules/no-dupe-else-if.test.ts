// Based on the no-dupe-else-if rule from eslint
// https://github.com/eslint/eslint/blob/main/tests/lib/rules/no-dupe-else-if.js

import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noDupeElseIf } from '../../../src/lib/rules/no-dupe-else-if.js'

describe('lib/rules/no-dupe-else-if.ts', () => {
  describe('Forbid duplicate conditions in if-else-if chains', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          // different test conditions
          if (A) {} else if (B) {}
          if (A) {} else if (B) {} else if (C) {}
          if (True) {} else if (False) {} else {}
          if (1) {} else if (2) {}
          if (F) {} else if (F()) {}
          if (F(A)) {} else if (G(A)) {}
          if (F(A)) {} else if (F(B)) {}
          if (A == 1) {} else if (A == 2) {}
          if (A == 1) {} else if (B == 1) {}

          // not an if-else-if chain
          if (A) {}
          if (A) {} else {}
          if (A) if (A) {}
          if (A) { if (A) {} }
          if (A) {} else { if (A) {} }
          if (A) {} if (A) {}

          // not same conditions in the chain
          if (A) { if (B) {} } else if (B) {}
          if (A) if (B) {} else if (A) {}

          // not equal tokens
          if (A) {} else if (!!A) {}
          if (A == 1) {} else if (A == (1)) {}

          // more complex valid chains (may contain redundant subconditions, but the branch can be executed)
          if (A || B) {} else if (C || d) {}
          if (A || B) {} else if (A || C) {}
          if (A) {} else if (A || B) {}
          if (A) {} else if (B) {} else if (A || B || C) {}
          if (A && B) {} else if (A) {} else if (B) {}
          if (A && B) {} else if (B && C) {} else if (A && C) {}
          if (A && B) {} else if (B || C) {}
          if (A) {} else if (B && (A || C)) {}
          if (A) {} else if (B && (C || D && A)) {}
          if (A && B && C) {} else if (A && B && (C || D)) {}
        }
      `, noDupeElseIf)
      expect(result.success).toBe(true)
    })
    describe('is invalid', () => {
      test('basic tests', async () => {
        const result = await testRule(`
          main() {
            if (A) {} else if (A) {}
            if (A) {} else if (A) {} else {}
            if (A) {} else if (B) {} else if (A) {} else if (C) {}
            if (A) {} else if (B) {} else if (A) {}
            if (A) {} else if (B) {} else if (C) {} else if (A) {}
            if (A) {} else if (B) {} else if (B) {}
            if (A) {} else if (B) {} else if (B) {} else {}
            if (A) {} else if (B) {} else if (C) {} else if (B) {}
          }
        `, noDupeElseIf)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(8)
        for (const message of result.messages) {
          expect(message.message).toBe('This branch will never be executed because its condition is already verified in a previous branch')
        }
        expect(result.messages[0].source.range.start).toBe(51)
        expect(result.messages[0].source.range.end).toBe(51)
        expect(result.messages[1].source.range.start).toBe(88)
        expect(result.messages[1].source.range.end).toBe(88)
        expect(result.messages[2].source.range.start).toBe(148)
        expect(result.messages[2].source.range.end).toBe(148)
        expect(result.messages[3].source.range.start).toBe(215)
        expect(result.messages[3].source.range.end).toBe(215)
        expect(result.messages[4].source.range.start).toBe(282)
        expect(result.messages[4].source.range.end).toBe(282)
        expect(result.messages[5].source.range.start).toBe(334)
        expect(result.messages[5].source.range.end).toBe(334)
        expect(result.messages[6].source.range.start).toBe(386)
        expect(result.messages[6].source.range.end).toBe(386)
        expect(result.messages[7].source.range.start).toBe(461)
        expect(result.messages[7].source.range.end).toBe(461)
      })
      test('multiple duplicates', async () => {
        const result = await testRule(`
          main() {
            if (A) {} else if (A) {} else if (A) {}
            if (A) {} else if (B) {} else if (A) {} else if (B) {} else if (A) {}
          }
        `, noDupeElseIf)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(5)
        for (const message of result.messages) {
          expect(message.message).toBe('This branch will never be executed because its condition is already verified in a previous branch')
        }
        expect(result.messages[0].source.range.start).toBe(51)
        expect(result.messages[0].source.range.end).toBe(51)
        expect(result.messages[1].source.range.start).toBe(66)
        expect(result.messages[1].source.range.end).toBe(66)
        expect(result.messages[2].source.range.start).toBe(118)
        expect(result.messages[2].source.range.end).toBe(118)
        expect(result.messages[3].source.range.start).toBe(133)
        expect(result.messages[3].source.range.end).toBe(133)
        expect(result.messages[4].source.range.start).toBe(148)
        expect(result.messages[4].source.range.end).toBe(148)
      })
      test('inner if statements do not affect chain', async () => {
        const result = await testRule(`
          main() {
            if (A) { if (B) {} } else if (A) {}
          }
        `, noDupeElseIf)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        for (const message of result.messages) {
          expect(message.message).toBe('This branch will never be executed because its condition is already verified in a previous branch')
        }
        expect(result.messages[0].source.range.start).toBe(62)
        expect(result.messages[0].source.range.end).toBe(62)
      })
      test('various kinds of test conditions', async () => {
        const result = await testRule(`
          main() {
            if (A == 1) {} else if (A == 1) {}
            if (1 < A) {} else if (1 < A) {}
            if (True) {} else if (True) {}
            if (A && B) {} else if (A && B) {}
            if (A && B || C)  {} else if (A && B || C) {}
            if (Fun(A)) {} else if (Fun(A)) {}
          }
        `, noDupeElseIf)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(6)
        for (const message of result.messages) {
          expect(message.message).toBe('This branch will never be executed because its condition is already verified in a previous branch')
        }
        expect(result.messages[0].source.range.start).toBe(56)
        expect(result.messages[0].source.range.end).toBe(61)
        expect(result.messages[1].source.range.start).toBe(102)
        expect(result.messages[1].source.range.end).toBe(106)
        expect(result.messages[2].source.range.start).toBe(146)
        expect(result.messages[2].source.range.end).toBe(149)
        expect(result.messages[3].source.range.start).toBe(191)
        expect(result.messages[3].source.range.end).toBe(196)
        expect(result.messages[4].source.range.start).toBe(244)
        expect(result.messages[4].source.range.end).toBe(254)
        expect(result.messages[5].source.range.start).toBe(296)
        expect(result.messages[5].source.range.end).toBe(301)
      })
      test('spaces and comments do not affect comparison', async () => {
        const result = await testRule(`
          main() {
            if (A == 1) {} else if (A==1) {}
            if (A == 1) {} else if (A == /* comment */ 1) {}
          }
        `, noDupeElseIf)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(2)
        for (const message of result.messages) {
          expect(message.message).toBe('This branch will never be executed because its condition is already verified in a previous branch')
        }
        expect(result.messages[0].source.range.start).toBe(56)
        expect(result.messages[0].source.range.end).toBe(59)
        expect(result.messages[1].source.range.start).toBe(101)
        expect(result.messages[1].source.range.end).toBe(120)
      })
      test('extra parenthesis around the whole test condition do not affect comparison', async () => {
        const result = await testRule(`
          main() {
            if (A == 1) {} else if ((A == 1)) {}
          }
        `, noDupeElseIf)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        for (const message of result.messages) {
          expect(message.message).toBe('This branch will never be executed because its condition is already verified in a previous branch')
        }
        expect(result.messages[0].source.range.start).toBe(57)
        expect(result.messages[0].source.range.end).toBe(62)
      })
      test('more complex errors with `||` and `&&`', async () => {
        const result = await testRule(`
          main() {
            if (A || B) {} else if (A) {}
            if (A || B) {} else if (A) {} else if (B) {}
            if (A || B) {} else if (B || A) {}
            if (A) {} else if (B) {} else if (A || B) {}
            if (A || B) {} else if (C || D) {} else if (A || D) {}
            if ((A == B && Fun(C)) || D) {} else if (Fun(C) && A == B) {}
            if (A) {} else if (A && B) {}
            if (A && B) {} else if (B && A) {}
            if (A && B) {} else if (A && B && C) {}
            if (A || C) {} else if (A && B || C) {}
            if (A) {} else if (B) {} else if (C && A || B) {}
            if (A) {} else if (B) {} else if (C && (A || B)) {}
            if (A) {} else if (B && C) {} else if (D && (A || E && C && B)) {}
            if (A || B && C) {} else if (B && C && D) {}
            if (A || B) {} else if (B && C) {}
            if (A) {} else if (B) {} else if ((A || B) && C) {}
            if ((A && (B || C)) || D) {} else if ((C || B) && E && A) {}
            if (A && B || B && C) {} else if (A && B && C) {}
            if (A) {} else if (B && C) {} else if (D && (C && E && B || A)) {}
            if (A || (B && (C || D))) {} else if ((D || C) && B) {}
            if (A || B) {} else if ((B || A) && C) {}
            if (A || B) {} else if (C) {} else if (D) {} else if (B && (A || C)) {}
            if (A || B || C) {} else if (A || (B && D) || (C && E)) {}
            if (A || (B || C)) {} else if (A || (B && C)) {}
            if (A || B) {} else if (C) {} else if (D) {} else if ((A || C) && (B || D)) {}
            if (A) {} else if (B) {} else if (C && (A || D && B)) {}
            if (A) {} else if (A || A) {}
            if (A || A) {} else if (A || A) {}
            if (A || A) {} else if (A) {}
            if (A) {} else if (A && A) {}
            if (A && A) {} else if (A && A) {}
            if (A && A) {} else if (A) {}
          }
        `, noDupeElseIf)
        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(33)
        for (const message of result.messages) {
          expect(message.message).toBe('This branch will never be executed because its condition is already verified in a previous branch')
        }
        expect(result.messages[0].source.range.start).toBe(56)
        expect(result.messages[0].source.range.end).toBe(56)
        expect(result.messages[1].source.range.start).toBe(98)
        expect(result.messages[1].source.range.end).toBe(98)
        expect(result.messages[2].source.range.start).toBe(113)
        expect(result.messages[2].source.range.end).toBe(113)
        expect(result.messages[3].source.range.start).toBe(155)
        expect(result.messages[3].source.range.end).toBe(160)
        expect(result.messages[4].source.range.start).toBe(212)
        expect(result.messages[4].source.range.end).toBe(217)
        expect(result.messages[5].source.range.start).toBe(279)
        expect(result.messages[5].source.range.end).toBe(284)
        expect(result.messages[6].source.range.start).toBe(343)
        expect(result.messages[6].source.range.end).toBe(358)
        expect(result.messages[7].source.range.start).toBe(395)
        expect(result.messages[7].source.range.end).toBe(400)
        expect(result.messages[8].source.range.start).toBe(442)
        expect(result.messages[8].source.range.end).toBe(447)
        expect(result.messages[9].source.range.start).toBe(489)
        expect(result.messages[9].source.range.end).toBe(499)
        expect(result.messages[10].source.range.start).toBe(541)
        expect(result.messages[10].source.range.end).toBe(551)
        expect(result.messages[11].source.range.start).toBe(603)
        expect(result.messages[11].source.range.end).toBe(613)
        expect(result.messages[12].source.range.start).toBe(665)
        expect(result.messages[12].source.range.end).toBe(677)
        expect(result.messages[13].source.range.start).toBe(734)
        expect(result.messages[13].source.range.end).toBe(756)
        expect(result.messages[14].source.range.start).toBe(803)
        expect(result.messages[14].source.range.end).toBe(813)
        expect(result.messages[15].source.range.start).toBe(855)
        expect(result.messages[15].source.range.end).toBe(860)
        expect(result.messages[16].source.range.start).toBe(912)
        expect(result.messages[16].source.range.end).toBe(924)
        expect(result.messages[17].source.range.start).toBe(980)
        expect(result.messages[17].source.range.end).toBe(997)
        expect(result.messages[18].source.range.start).toBe(1049)
        expect(result.messages[18].source.range.end).toBe(1059)
        expect(result.messages[19].source.range.start).toBe(1116)
        expect(result.messages[19].source.range.end).toBe(1138)
        expect(result.messages[20].source.range.start).toBe(1194)
        expect(result.messages[20].source.range.end).toBe(1206)
        expect(result.messages[21].source.range.start).toBe(1248)
        expect(result.messages[21].source.range.end).toBe(1260)
        expect(result.messages[22].source.range.start).toBe(1332)
        expect(result.messages[22].source.range.end).toBe(1344)
        expect(result.messages[23].source.range.start).toBe(1391)
        expect(result.messages[23].source.range.end).toBe(1415)
        expect(result.messages[24].source.range.start).toBe(1464)
        expect(result.messages[24].source.range.end).toBe(1476)
        expect(result.messages[25].source.range.start).toBe(1548)
        expect(result.messages[25].source.range.end).toBe(1567)
        expect(result.messages[26].source.range.start).toBe(1619)
        expect(result.messages[26].source.range.end).toBe(1636)
        expect(result.messages[27].source.range.start).toBe(1673)
        expect(result.messages[27].source.range.end).toBe(1678)
        expect(result.messages[28].source.range.start).toBe(1720)
        expect(result.messages[28].source.range.end).toBe(1725)
        expect(result.messages[29].source.range.start).toBe(1767)
        expect(result.messages[29].source.range.end).toBe(1767)
        expect(result.messages[30].source.range.start).toBe(1804)
        expect(result.messages[30].source.range.end).toBe(1809)
        expect(result.messages[31].source.range.start).toBe(1851)
        expect(result.messages[31].source.range.end).toBe(1856)
        expect(result.messages[32].source.range.start).toBe(1898)
        expect(result.messages[32].source.range.end).toBe(1898)
      })
    })
  })
})
