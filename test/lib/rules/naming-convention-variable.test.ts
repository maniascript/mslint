import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { namingConventionVariable } from '../../../src/lib/rules/naming-convention-variable.js'

describe('lib/rules/naming-convention-variable.ts', () => {
  describe('A variable name must start with an uppercase letter', () => {
    test('Find valid variable name', async () => {
      const result = await testRule(`
        main() {
          declare Integer ValidNameA;
          declare Integer ValidNameB as ValidNameC;
        }
      `, namingConventionVariable)

      expect(result.success).toBe(true)
    })
    test('Find invalid variable name', async () => {
      const result = await testRule(`
        main() {
          declare Integer invalidNameA;
          declare Integer _InvalidNameB;
          declare Integer invalidNameC as invalidNameD;
        }
      `, namingConventionVariable)

      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(4)

      expect(result.messages[0].message).toBe('A variable name must start with an uppercase letter')
      expect(result.messages[1].message).toBe('A variable name must start with an uppercase letter')
      expect(result.messages[2].message).toBe('A variable name must start with an uppercase letter')
      expect(result.messages[3].message).toBe('An alias name for a variable must start with an uppercase letter')

      expect(result.messages[0].source.range.start).toBe(44)
      expect(result.messages[0].source.range.end).toBe(55)
      expect(result.messages[1].source.range.start).toBe(84)
      expect(result.messages[1].source.range.end).toBe(96)
      expect(result.messages[2].source.range.start).toBe(125)
      expect(result.messages[2].source.range.end).toBe(136)
      expect(result.messages[3].source.range.start).toBe(141)
      expect(result.messages[3].source.range.end).toBe(152)
    })
  })
  describe('A global variable name must start with \'G_\'', () => {
    test('Find valid global variable name', async () => {
      const result = await testRule(`
        declare Integer G_A;
        declare Integer G_B as G_C;
        main () {
          declare Integer D;
        }
      `, namingConventionVariable)

      expect(result.success).toBe(true)
    })
    test('Find invalid global variable name', async () => {
      const result = await testRule(`
        declare Integer InvalidNameA;
        declare Integer InvalidNameB as InvalidNameC;
        declare Integer G_;
        declare Integer g_invalidNameE;
      `, namingConventionVariable)

      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(5)

      expect(result.messages[0].message).toBe('A global variable name must be prefixed with \'G_\'')
      expect(result.messages[1].message).toBe('A global variable name must be prefixed with \'G_\'')
      expect(result.messages[2].message).toBe('An alias name for a global variable must be prefixed with \'G_\'')
      expect(result.messages[3].message).toBe('A global variable name must be prefixed with \'G_\'')

      expect(result.messages[0].source.range.start).toBe(25)
      expect(result.messages[0].source.range.end).toBe(36)
      expect(result.messages[1].source.range.start).toBe(63)
      expect(result.messages[1].source.range.end).toBe(74)
      expect(result.messages[2].source.range.start).toBe(79)
      expect(result.messages[2].source.range.end).toBe(90)
      expect(result.messages[3].source.range.start).toBe(117)
      expect(result.messages[3].source.range.end).toBe(118)
      expect(result.messages[4].source.range.start).toBe(145)
      expect(result.messages[4].source.range.end).toBe(158)
    })
  })
})
