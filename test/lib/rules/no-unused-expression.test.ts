import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noUnusedExpression } from '../../../src/lib/rules/no-unused-expression.js'

describe('lib/rules/no-unused-expression.ts', () => {
  describe('Forbid unused expression', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          FunctionA();
          VarA = 0;
          VarB.FunctionB();
          VarC.VarD.FunctionC();
          VarE.FunctionD().varF;
        }
      `, noUnusedExpression)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('find unused expression', async () => {
        const result = await testRule(`
          #Struct K_StructA {
            Integer MemberA;
          }
          main() {
            0;
            "ABC";
            [1];
            [2 => 3];
            K_StructA {};
            VarA as CMlLabel;
            VarB is CMlLabel;
            VarC.VarD;
            VarE[0];
            !True;
            -4;
            VarF + VarG;
            5 - 6 * 7 / 8 % 9;
            "DEF"^"GHI";
            VarH != VarI;
            VarJ > VarK;
            VarL && VarM;
            VarN || VarO;
            """AAA {{{VarP}}} BBB""";
            dumptype(K_StructA);
            dump(VarQ);
            cast(CMlLabel, VarR);
            VarS;
            <1, 2, 3>;
            This;
            (VarT);
            if (True) 1;
            VarU.VarV.VarW;
          }
        `, noUnusedExpression)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(28)
        for (const message of result.messages) {
          expect(message.message).toBe('Expected an assignement or function call and instead found an expression')
        }

        expect(result.messages[0].source.range.start).toBe(103)
        expect(result.messages[0].source.range.end).toBe(104)
        expect(result.messages[1].source.range.start).toBe(118)
        expect(result.messages[1].source.range.end).toBe(123)
        expect(result.messages[2].source.range.start).toBe(137)
        expect(result.messages[2].source.range.end).toBe(140)
        expect(result.messages[3].source.range.start).toBe(154)
        expect(result.messages[3].source.range.end).toBe(162)
        expect(result.messages[4].source.range.start).toBe(176)
        expect(result.messages[4].source.range.end).toBe(188)
        expect(result.messages[5].source.range.start).toBe(202)
        expect(result.messages[5].source.range.end).toBe(218)
        expect(result.messages[6].source.range.start).toBe(232)
        expect(result.messages[6].source.range.end).toBe(248)
        expect(result.messages[7].source.range.start).toBe(262)
        expect(result.messages[7].source.range.end).toBe(271)
        expect(result.messages[8].source.range.start).toBe(285)
        expect(result.messages[8].source.range.end).toBe(292)
        expect(result.messages[9].source.range.start).toBe(306)
        expect(result.messages[9].source.range.end).toBe(311)
        expect(result.messages[10].source.range.start).toBe(325)
        expect(result.messages[10].source.range.end).toBe(327)
        expect(result.messages[11].source.range.start).toBe(341)
        expect(result.messages[11].source.range.end).toBe(352)
        expect(result.messages[12].source.range.start).toBe(366)
        expect(result.messages[12].source.range.end).toBe(383)
        expect(result.messages[13].source.range.start).toBe(397)
        expect(result.messages[13].source.range.end).toBe(408)
        expect(result.messages[14].source.range.start).toBe(422)
        expect(result.messages[14].source.range.end).toBe(434)
        expect(result.messages[15].source.range.start).toBe(448)
        expect(result.messages[15].source.range.end).toBe(459)
        expect(result.messages[16].source.range.start).toBe(473)
        expect(result.messages[16].source.range.end).toBe(485)
        expect(result.messages[17].source.range.start).toBe(499)
        expect(result.messages[17].source.range.end).toBe(511)
        expect(result.messages[18].source.range.start).toBe(525)
        expect(result.messages[18].source.range.end).toBe(549)
        expect(result.messages[19].source.range.start).toBe(563)
        expect(result.messages[19].source.range.end).toBe(582)
        expect(result.messages[20].source.range.start).toBe(596)
        expect(result.messages[20].source.range.end).toBe(606)
        expect(result.messages[21].source.range.start).toBe(620)
        expect(result.messages[21].source.range.end).toBe(640)
        expect(result.messages[22].source.range.start).toBe(654)
        expect(result.messages[22].source.range.end).toBe(658)
        expect(result.messages[23].source.range.start).toBe(672)
        expect(result.messages[23].source.range.end).toBe(681)
        expect(result.messages[24].source.range.start).toBe(695)
        expect(result.messages[24].source.range.end).toBe(699)
        expect(result.messages[25].source.range.start).toBe(713)
        expect(result.messages[25].source.range.end).toBe(719)
        expect(result.messages[26].source.range.start).toBe(743)
        expect(result.messages[26].source.range.end).toBe(744)
        expect(result.messages[27].source.range.start).toBe(758)
        expect(result.messages[27].source.range.end).toBe(772)
      })
    })
  })
})
