import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noEmpty } from '../../../src/lib/rules/no-empty.js'

describe('lib/rules/no-empty.ts', () => {
  describe('Forbid empty block statement', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void FunctionA() {
          log("not empty");
        }
        main() {
          log("not empty");
        }
      `, noEmpty)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('forbid empty block', async () => {
        const result = await testRule(`
          Void FunctionA() {}
          main() {
            foreach (A in B) {}
            for (I, 0, 10) {}
            while (True) {}
            meanwhile (True) {}
            switch (C) {
              case 1: {}
            }
            switchtype (D) {
              case CSmMode: {}
            }
            if (True) {}
            else if (True) {}
            else {}
            {}
          }
        `, noEmpty)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(11)
        for (const message of result.messages) {
          expect(message.message).toBe('Empty block statement')
        }

        expect(result.messages[0].source.range.start).toBe(28)
        expect(result.messages[0].source.range.end).toBe(29)
        expect(result.messages[1].source.range.start).toBe(79)
        expect(result.messages[1].source.range.end).toBe(80)
        expect(result.messages[2].source.range.start).toBe(109)
        expect(result.messages[2].source.range.end).toBe(110)
        expect(result.messages[3].source.range.start).toBe(137)
        expect(result.messages[3].source.range.end).toBe(138)
        expect(result.messages[4].source.range.start).toBe(169)
        expect(result.messages[4].source.range.end).toBe(170)
        expect(result.messages[5].source.range.start).toBe(219)
        expect(result.messages[5].source.range.end).toBe(220)
        expect(result.messages[6].source.range.start).toBe(293)
        expect(result.messages[6].source.range.end).toBe(294)
        expect(result.messages[7].source.range.start).toBe(332)
        expect(result.messages[7].source.range.end).toBe(333)
        expect(result.messages[8].source.range.start).toBe(362)
        expect(result.messages[8].source.range.end).toBe(363)
        expect(result.messages[9].source.range.start).toBe(382)
        expect(result.messages[9].source.range.end).toBe(383)
        expect(result.messages[10].source.range.start).toBe(397)
        expect(result.messages[10].source.range.end).toBe(398)
      })
    })
  })
})
