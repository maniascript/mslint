import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { formatReal } from '../../../src/lib/rules/format-real.js'

describe('lib/rules/format-real.ts', () => {
  test('Find valid Real format', async () => {
    const result = await testRule('main() { 2.; 3.4; }', formatReal)

    expect(result.success).toBe(true)
  })
  test('Find invalid Real format', async () => {
    const result = await testRule('main() { .1; 2.; 3.4; }', formatReal)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(1)
    expect(result.messages[0].message).toBe('A Real value must have an integer part, write \'0.1\' instead of \'.1\'')
  })
  test('Accept settings', async () => {
    let result = await testRule('main() { .1; 2.; 3.4; }', formatReal, { hasIntegerPart: true, hasDecimalPart: true })

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(2)
    expect(result.messages[0].message).toBe('A Real value must have an integer part, write \'0.1\' instead of \'.1\'')
    expect(result.messages[1].message).toBe('A Real value must have a decimal part, write \'2.0\' instead of \'2.\'')

    result = await testRule('main() { .1; 2.; 3.4; }', formatReal, { hasIntegerPart: false, hasDecimalPart: true })

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(1)
    expect(result.messages[0].message).toBe('A Real value must have a decimal part, write \'2.0\' instead of \'2.\'')

    result = await testRule('main() { .1; 2.; 3.4; }', formatReal, { hasIntegerPart: true, hasDecimalPart: false })

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(1)
    expect(result.messages[0].message).toBe('A Real value must have an integer part, write \'0.1\' instead of \'.1\'')
  })
  test('Report the correct position', async () => {
    const result = await testRule('main() { .5; }', formatReal)

    expect(result.messages[0].source.range.start).toBe(9)
    expect(result.messages[0].source.range.end).toBe(10)
  })
})
