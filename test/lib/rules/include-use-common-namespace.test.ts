import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { includeUseCommonNamespace } from '../../../src/lib/rules/include-use-common-namespace.js'

describe('lib/rules/include-use-common-namespace.ts', () => {
  test('Find valid standard library namespace', async () => {
    const result = await testRule(`
      #Include "MathLib" as ML
      #Include "TextLib" as TL
      #Include "MapUnits" as MU
      #Include "AnimLib" as AL
      #Include "TimeLib" as TiL
      #Include "ColorLib" as CL
    `, includeUseCommonNamespace)
    expect(result.success).toBe(true)
  })
  test('Find invalid standard library namespace', async () => {
    const result = await testRule(`
      #Include "MathLib" as A
      #Include "TextLib" as B
      #Include "MapUnits" as C
      #Include "AnimLib" as D
      #Include "TimeLib" as E
      #Include "ColorLib" as F
    `, includeUseCommonNamespace)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(6)
    expect(result.messages[0].message).toBe('Use the common MathLib namespace. Replace \'A\' by \'ML\'.')
    expect(result.messages[1].message).toBe('Use the common TextLib namespace. Replace \'B\' by \'TL\'.')
    expect(result.messages[2].message).toBe('Use the common MapUnits namespace. Replace \'C\' by \'MU\'.')
    expect(result.messages[3].message).toBe('Use the common AnimLib namespace. Replace \'D\' by \'AL\'.')
    expect(result.messages[4].message).toBe('Use the common TimeLib namespace. Replace \'E\' by \'TiL\'.')
    expect(result.messages[5].message).toBe('Use the common ColorLib namespace. Replace \'F\' by \'CL\'.')

    expect(result.messages[0].source.range.start).toBe(29)
    expect(result.messages[0].source.range.end).toBe(29)
    expect(result.messages[1].source.range.start).toBe(59)
    expect(result.messages[1].source.range.end).toBe(59)
    expect(result.messages[2].source.range.start).toBe(90)
    expect(result.messages[2].source.range.end).toBe(90)
    expect(result.messages[3].source.range.start).toBe(120)
    expect(result.messages[3].source.range.end).toBe(120)
    expect(result.messages[4].source.range.start).toBe(150)
    expect(result.messages[4].source.range.end).toBe(150)
    expect(result.messages[5].source.range.start).toBe(181)
    expect(result.messages[5].source.range.end).toBe(181)
  })
})
