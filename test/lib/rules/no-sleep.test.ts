import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noSleep } from '../../../src/lib/rules/no-sleep.js'

describe('lib/rules/no-sleep.ts', () => {
  describe('Forbid the use of the sleep() function', () => {
    test('is valid', async () => {
      const result = await testRule(`
        Void Sleep(Integer _Value) {}
        main() {
          Sleep(Now);
        }
      `, noSleep)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('forbidden sleep() function used', async () => {
        const result = await testRule(`
          main() {
            sleep(Now);
          }
        `, noSleep)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('The use of the \'sleep()\' function is forbidden')

        expect(result.messages[0].source.range.start).toBe(32)
        expect(result.messages[0].source.range.end).toBe(42)
      })
    })
  })
})
