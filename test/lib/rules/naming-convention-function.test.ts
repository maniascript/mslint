import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { namingConventionFunction } from '../../../src/lib/rules/naming-convention-function.js'

describe('lib/rules/naming-convention-constant.ts', () => {
  test('Find valid function name', async () => {
    const result = await testRule('Void Valid() {}', namingConventionFunction)

    expect(result.success).toBe(true)
  })
  test('Find invalid function name', async () => {
    const result = await testRule(`
      Void invalidName() {}
      Void _InvalidName() {}
    `, namingConventionFunction)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(2)
  })
  test('Find valid parameter name', async () => {
    const result = await testRule(`
      Void Valid(Integer _ValidName) {}
    `, namingConventionFunction)

    expect(result.success).toBe(true)
  })
  test('Find invalid parameter name', async () => {
    const result = await testRule(`
    Void Valid(Integer InvalidName) {}
    Void Valid(Integer _invalidName) {}
    Void Valid(Integer invalidName) {}
    `, namingConventionFunction)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(3)
  })
  test('Report the correct position', async () => {
    const result = await testRule('Void invalidName(Integer invalidParameter, Integer _invalidParameter) {}', namingConventionFunction)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(3)
    expect(result.messages[0].source.range.start).toBe(5)
    expect(result.messages[0].source.range.end).toBe(15)
    expect(result.messages[1].source.range.start).toBe(25)
    expect(result.messages[1].source.range.end).toBe(40)
    expect(result.messages[2].source.range.start).toBe(51)
    expect(result.messages[2].source.range.end).toBe(67)
  })
})
