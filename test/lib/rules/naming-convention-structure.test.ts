import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { namingConventionStructure } from '../../../src/lib/rules/naming-convention-structure.js'

describe('lib/rules/naming-convention-structure.ts', () => {
  describe('A structure name must be prefixed with \'K_\'', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Struct K_A { Integer A; }
        #Struct B::B as K_B
        #Struct Lib_K_C { Integer C; }
      `, namingConventionStructure)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        #Struct A { Integer A; }
        #Struct B::B as B
      `, namingConventionStructure)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(2)
      for (const message of result.messages) {
        expect(message.message).toBe('A structure name must be prefixed with \'K_\'')
      }
      expect(result.messages[0].source.range.start).toBe(17)
      expect(result.messages[0].source.range.end).toBe(17)
      expect(result.messages[1].source.range.start).toBe(58)
      expect(result.messages[1].source.range.end).toBe(58)
    })
  })
})
