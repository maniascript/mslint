import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { includeOneTime } from '../../../src/lib/rules/include-one-time.js'

describe('lib/rules/include-one-time.ts', () => {
  describe('Start value must be smaller than stop value', () => {
    test('is valid', async () => {
      const result = await testRule(`
        #Include "Path/To/FileA.Script.txt"
        #Include "Path/To/FileB.Script.txt"
        #Include "Path/To/FileC.Script.txt" as C
      `, includeOneTime)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        #Include "Path/To/FileA.Script.txt"
        #Include "Path/To/FileA.Script.txt"
        #Include "Path/To/FileA.Script.txt" as C
      `, includeOneTime)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(2)
      for (const message of result.messages) {
        expect(message.message).toBe('This file is already included')
      }
      expect(result.messages[0].source.range.start).toBe(62)
      expect(result.messages[0].source.range.end).toBe(87)
      expect(result.messages[1].source.range.start).toBe(106)
      expect(result.messages[1].source.range.end).toBe(131)
    })
  })
})
