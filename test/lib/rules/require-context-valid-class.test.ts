import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { requireContextValidClass } from '../../../src/lib/rules/require-context-valid-class.js'

describe('lib/rules/require-context-valid-class.ts', () => {
  test('Find valid class', async () => {
    const result = await testRule('#RequireContext CSmMode', requireContextValidClass)
    expect(result.success).toBe(true)
  })
  test('Find invalid class', async () => {
    const result = await testRule('#RequireContext CMode', requireContextValidClass)

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(1)
    expect(result.messages[0].message).toBe('\'CMode\' is not a valid script execution context')

    expect(result.messages[0].source.range.start).toBe(16)
    expect(result.messages[0].source.range.end).toBe(20)
  })
  test('Find custom valid class', async () => {
    const result = await testRule('#RequireContext CPlayer', requireContextValidClass, { validClasses: ['CPlayer'] })
    expect(result.success).toBe(true)
  })
  test('Find custom invalid class', async () => {
    const result = await testRule('#RequireContext CSmMode', requireContextValidClass, { invalidClasses: ['CSmMode'] })

    expect(result.success).toBe(false)
    expect(result.messages.length).toBe(1)
    expect(result.messages[0].message).toBe('\'CSmMode\' is not a valid script execution context')

    expect(result.messages[0].source.range.start).toBe(16)
    expect(result.messages[0].source.range.end).toBe(22)
  })
})
