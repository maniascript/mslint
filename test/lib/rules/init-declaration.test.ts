import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { initDeclaration, Target } from '../../../src/lib/rules/init-declaration.js'

describe('lib/rules/init-declaration.ts', () => {
  describe('Check variable declaration initialization', () => {
    test('is valid', async () => {
      const result = await testRule(`
        declare Integer G_A;

        main() {
          declare Integer B;
          declare Integer C for This = 0;
        }
      `, initDeclaration)
      expect(result.success).toBe(true)
    })
    describe('is invalid', () => {
      test('ignore global variables', async () => {
        const result = await testRule(`
          declare Integer G_A;
          declare Integer G_B for This;
          declare Integer G_C = 0;
          declare Integer G_D for This = 1;
        `, initDeclaration)
        expect(result.success).toBe(true)
      })

      test('always initialize', async () => {
        const result = await testRule(`
          main() {
            declare Integer A;
            declare Integer B = 0;
            declare Integer C for This;
            declare Integer D for This = 1;
          }
        `, initDeclaration, { target: Target.Always })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(2)
        expect(result.messages[0].message).toBe('Variable \'A\' must be initialized on declaration')
        expect(result.messages[1].message).toBe('Variable \'C\' must be initialized on declaration')

        expect(result.messages[0].source.range.start).toBe(32)
        expect(result.messages[0].source.range.end).toBe(49)
        expect(result.messages[1].source.range.start).toBe(98)
        expect(result.messages[1].source.range.end).toBe(124)
      })

      test('never initialize', async () => {
        const result = await testRule(`
          main() {
            declare Integer A;
            declare Integer B = 0;
            declare Integer C for This;
            declare Integer D for This = 1;
          }
        `, initDeclaration, { target: Target.Never })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(2)
        expect(result.messages[0].message).toBe('Variable \'B\' should not be initialized on declaration')
        expect(result.messages[1].message).toBe('Variable \'D\' should not be initialized on declaration')

        expect(result.messages[0].source.range.start).toBe(63)
        expect(result.messages[0].source.range.end).toBe(84)
        expect(result.messages[1].source.range.start).toBe(138)
        expect(result.messages[1].source.range.end).toBe(168)
      })

      test('always initialize traits', async () => {
        const result = await testRule(`
          main() {
            declare Integer A;
            declare Integer B = 0;
            declare Integer C for This;
            declare Integer D for This = 1;
          }
        `, initDeclaration, { target: Target.AlwaysTrait })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('Variable \'C\' must be initialized on declaration')

        expect(result.messages[0].source.range.start).toBe(98)
        expect(result.messages[0].source.range.end).toBe(124)
      })
    })
  })
})
