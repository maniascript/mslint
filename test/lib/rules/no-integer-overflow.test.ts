import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noIntegerOverflow } from '../../../src/lib/rules/no-integer-overflow.js'

describe('lib/rules/no-integer-overflow.ts', () => {
  describe('Forbid integer literal overflow', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          123456;
          -123456;
          2147483647;
          -2147483648;
        }
      `, noIntegerOverflow)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        main() {
          2147483648;
          -2147483649;
        }
      `, noIntegerOverflow)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(2)
      for (const message of result.messages) {
        expect(message.message).toBe('This integer will overflow')
      }
      expect(result.messages[0].source.range.start).toBe(28)
      expect(result.messages[0].source.range.end).toBe(37)
      expect(result.messages[1].source.range.start).toBe(50)
      expect(result.messages[1].source.range.end).toBe(60)
    })
  })
})
