import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { formatIfElse } from '../../../src/lib/rules/format-if-else.js'

describe('lib/rules/format-if-else.ts', () => {
  describe('consequent not in a block must be on the same line', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          if (A) A();
          else if (B) B();
          else if (
            C
          ) C();
          else if (D) {
            D();
          }
          else E();
        }
      `, formatIfElse.meta.id)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        main() {
          if (A)
            A();
          else if (B)
            B();
          else
            C();
        }
      `,
      formatIfElse.meta.id)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(3)
      for (const message of result.messages) {
        expect(message.message).toBe('Use braces or put everything on one line')
      }
    })
  })
})
