import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noNowClassMember } from '../../../src/lib/rules/no-now-class-member.js'

describe('lib/rules/no-now-class-member.ts', () => {
  describe('_template_description', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          Now;
        }
      `, noNowClassMember)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('_template_test', async () => {
        const result = await testRule(`
          main() {
            This.Now;
          }
        `, noNowClassMember, { maximum: 15 })

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('Use the `Now` keyword instead of the `x.Now` class member')

        expect(result.messages[0].source.range.start).toBe(32)
        expect(result.messages[0].source.range.end).toBe(39)
      })
    })
  })
})
