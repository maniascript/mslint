import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noLonelyIf } from '../../../src/lib/rules/no-lonely-if.js'

describe('lib/rules/no-lonely-if.ts', () => {
  describe('Forbid `if` statement as the only statement in an `else` block', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          if (A) {} else if (B) {}
          if (A) {} else { if (B) {} log(C); }
        }
      `, noLonelyIf)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('Only `if` in `else`', async () => {
        const result = await testRule(`
          main() {
            if (A) {} else { if (B) {} }
            if (A) {} else { if (B) log(C); }
            if (A) {} else { if (B) {} else if (C) {} }
          }
        `, noLonelyIf)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(3)
        for (const message of result.messages) {
          expect(message.message).toBe('Forbidden `if` statement as the only statement in an `else` block')
        }

        expect(result.messages[0].source.range.start).toBe(42)
        expect(result.messages[0].source.range.end).toBe(59)
        expect(result.messages[1].source.range.start).toBe(83)
        expect(result.messages[1].source.range.end).toBe(105)
        expect(result.messages[2].source.range.start).toBe(129)
        expect(result.messages[2].source.range.end).toBe(161)
      })
    })
  })
})
