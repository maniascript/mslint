import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noUselessConcat } from '../../../src/lib/rules/no-useless-concat.js'

describe('lib/rules/no-useless-concat.ts', () => {
  describe('Forbid unecessary concatenation of Text literals', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          "A" ^ B;
          "C" ^ D ^ "E";
          """F""" ^ G;
          H ^ I;
        }
      `, noUselessConcat)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('find useless concatenation', async () => {
        const result = await testRule(`
          main() {
            "A" ^ "B";
            """C""" ^ """D""";
            """E""" ^ "F";
            """H {{{I}}} J""" ^ "K";
            """L {{{M}}} N""" ^ """O {{{P}}} Q""";
            "R" ^ "S" ^ "T";
            U ^ "V" ^ "W";
            (X1 ^ "X2") ^ ("X3" ^ "X4");
          }
        `, noUselessConcat)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(10)
        for (const message of result.messages) {
          expect(message.message).toBe('Forbidden concatenation of Text literals')
        }

        expect(result.messages[0].source.range.start).toBe(32)
        expect(result.messages[0].source.range.end).toBe(40)
        expect(result.messages[1].source.range.start).toBe(55)
        expect(result.messages[1].source.range.end).toBe(71)
        expect(result.messages[2].source.range.start).toBe(86)
        expect(result.messages[2].source.range.end).toBe(98)
        expect(result.messages[3].source.range.start).toBe(113)
        expect(result.messages[3].source.range.end).toBe(135)
        expect(result.messages[4].source.range.start).toBe(150)
        expect(result.messages[4].source.range.end).toBe(186)
        expect(result.messages[5].source.range.start).toBe(201)
        expect(result.messages[5].source.range.end).toBe(215)
        expect(result.messages[6].source.range.start).toBe(201)
        expect(result.messages[6].source.range.end).toBe(209)
        expect(result.messages[7].source.range.start).toBe(230)
        expect(result.messages[7].source.range.end).toBe(242)
        expect(result.messages[8].source.range.start).toBe(257)
        expect(result.messages[8].source.range.end).toBe(283)
        expect(result.messages[9].source.range.start).toBe(272)
        expect(result.messages[9].source.range.end).toBe(282)
      })
    })
  })
})
