import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noDupeCase } from '../../../src/lib/rules/no-dupe-case.js'

describe('lib/rules/no-dupe-case.ts', () => {
  describe('Forbid duplicate case in switch statements', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          switch (A) {
            case 1: {}
            case 2: {}
            case 3, 4, 5: {}
            case 1+1: {}
            case Fn(): {}
          }
        }
      `, noDupeCase)
      expect(result.success).toBe(true)
    })
    test('is invalid', async () => {
      const result = await testRule(`
        main() {
          switch (A) {
            case 1: {}
            case 1: {}
            case 2, 3: {}
            case 3: {}
            case 1, 2, 3, 4: {}
            case 4, 1, 2: {}
            case 1+1: {}
            case     1  +   1     : {}
            case Fn(): {}
            case Fn(): {}
            case 5, 6, 5, 6: {}
          }
        }
      `, noDupeCase)
      expect(result.success).toBe(false)
      expect(result.messages.length).toBe(12)
      expect(result.messages[0].message).toBe('Duplicate case \'1\'')
      expect(result.messages[0].source.range.start).toBe(81)
      expect(result.messages[0].source.range.end).toBe(81)
      expect(result.messages[1].message).toBe('Duplicate case \'3\'')
      expect(result.messages[1].source.range.start).toBe(130)
      expect(result.messages[1].source.range.end).toBe(130)
      expect(result.messages[2].message).toBe('Duplicate case \'1\'')
      expect(result.messages[2].source.range.start).toBe(153)
      expect(result.messages[2].source.range.end).toBe(153)
      expect(result.messages[3].message).toBe('Duplicate case \'2\'')
      expect(result.messages[3].source.range.start).toBe(156)
      expect(result.messages[3].source.range.end).toBe(156)
      expect(result.messages[4].message).toBe('Duplicate case \'3\'')
      expect(result.messages[4].source.range.start).toBe(159)
      expect(result.messages[4].source.range.end).toBe(159)
      expect(result.messages[5].message).toBe('Duplicate case \'4\'')
      expect(result.messages[5].source.range.start).toBe(185)
      expect(result.messages[5].source.range.end).toBe(185)
      expect(result.messages[6].message).toBe('Duplicate case \'1\'')
      expect(result.messages[6].source.range.start).toBe(188)
      expect(result.messages[6].source.range.end).toBe(188)
      expect(result.messages[7].message).toBe('Duplicate case \'2\'')
      expect(result.messages[7].source.range.start).toBe(191)
      expect(result.messages[7].source.range.end).toBe(191)
      expect(result.messages[8].message).toBe('Duplicate case \'1  +   1\'')
      expect(result.messages[8].source.range.start).toBe(243)
      expect(result.messages[8].source.range.end).toBe(250)
      expect(result.messages[9].message).toBe('Duplicate case \'Fn()\'')
      expect(result.messages[9].source.range.start).toBe(304)
      expect(result.messages[9].source.range.end).toBe(307)
      expect(result.messages[10].message).toBe('Duplicate case \'5\'')
      expect(result.messages[10].source.range.start).toBe(336)
      expect(result.messages[10].source.range.end).toBe(336)
      expect(result.messages[11].message).toBe('Duplicate case \'6\'')
      expect(result.messages[11].source.range.start).toBe(339)
      expect(result.messages[11].source.range.end).toBe(339)
    })
  })
})
