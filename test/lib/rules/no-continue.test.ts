import { describe, test, expect } from 'vitest'
import { testRule } from '../../utils.js'
import { noContinue } from '../../../src/lib/rules/no-continue.js'

describe('lib/rules/no-continue.ts', () => {
  describe('Forbid the use of the continue() function', () => {
    test('is valid', async () => {
      const result = await testRule(`
        main() {
          for (I, 0, 10) {}
        }
      `, noContinue)
      expect(result.success).toBe(true)
    })

    describe('is invalid', () => {
      test('forbidden continue statement used', async () => {
        const result = await testRule(`
          main() {
            for (I, 0, 10) {
              continue;
            }
          }
        `, noContinue)

        expect(result.success).toBe(false)
        expect(result.messages.length).toBe(1)
        expect(result.messages[0].message).toBe('The use of the \'continue\' statement is forbidden')

        expect(result.messages[0].source.range.start).toBe(63)
        expect(result.messages[0].source.range.end).toBe(71)
      })
    })
  })
})
