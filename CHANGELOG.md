Changelog
=========

Unreleased
----------

- Nothing yet ⌛

Version 4.2.0
-------------

- [#65](https://gitlab.com/maniascript/mslint/-/issues/65) Updated node dependencies

Version 4.1.0
-------------

- [#64](https://gitlab.com/maniascript/mslint/-/issues/64) Added a rule preventing the use of the `Now` class member

Version 4.0.0
-------------

- [#59](https://gitlab.com/maniascript/mslint/-/issues/59) Updated node dependencies
- [#60](https://gitlab.com/maniascript/mslint/-/issues/60) Updated `@humanwhocodes/config-array` to `@eslint/config-array`
- [#61](https://gitlab.com/maniascript/mslint/-/issues/61) Updated typescript
- [#62](https://gitlab.com/maniascript/mslint/-/issues/62) Replaced `ts-standard` by `typescript-eslint`
- [#63](https://gitlab.com/maniascript/mslint/-/issues/63) Updated `vitepress` and the mslint documetation
- [#55](https://gitlab.com/maniascript/mslint/-/issues/55) Fixed files with disabled errors listed in the CLI report

Version 3.4.0
-------------

- [#58](https://gitlab.com/maniascript/mslint/-/issues/58) Added the `CAnyEditorPlugin` class to the `require-context-valid-class` rule

Version 3.3.0
-------------

- [#56](https://gitlab.com/maniascript/mslint/-/issues/56) Added the new special constant name `CustomMusicFolder` to the `naming-convention-constant` rule
- [#57](https://gitlab.com/maniascript/mslint/-/issues/57) Added a setting in the `naming-convention-constant` rule allowing the user to define a list of constant names ignored by the rule

Version 3.2.1
-------------

- [#54](https://gitlab.com/maniascript/mslint/-/issues/54) Prevented typescript from adding a reference path to src directory in the build output

Version 3.2.0
-------------

- [#48](https://gitlab.com/maniascript/mslint/-/issues/48) Added the ability to disable rules inline with a comment
- [#51](https://gitlab.com/maniascript/mslint/-/issues/51) Added an option to report unused disable directives
- [#53](https://gitlab.com/maniascript/mslint/-/issues/53) Added an option to report disable directives without a reason
- [#52](https://gitlab.com/maniascript/mslint/-/issues/52) Added the report severity in the CLI output
- [#49](https://gitlab.com/maniascript/mslint/-/issues/49) Added maniascript code highlighting in the documentation
- [#50](https://gitlab.com/maniascript/mslint/-/issues/50) Changed the theme of the code highlighting in the documentation
- [#47](https://gitlab.com/maniascript/mslint/-/issues/47) Updated node dependencies

Version 3.1.1
-------------

- Updated the README file to link to the new documentation page

Version 3.1.0
-------------

- [#41](https://gitlab.com/maniascript/mslint/-/issues/41) Fixed wrong path used to check the `msApiPath` cache
- [#42](https://gitlab.com/maniascript/mslint/-/issues/42) Added the possibility to provide patterns to lint in the config file
- [#43](https://gitlab.com/maniascript/mslint/-/issues/43) Added support for `let` variables in `type-declaration` rule

Version 3.0.0
-------------

- [#38](https://gitlab.com/maniascript/mslint/-/issues/38) Made it easier to configure the linter
- [#39](https://gitlab.com/maniascript/mslint/-/issues/39) Updated npm packages dependencies
- [#40](https://gitlab.com/maniascript/mslint/-/issues/40) Replaced the testing framework with `vitest` instead of `jest`

Version 2.2.0
-------------

- [#37](https://gitlab.com/maniascript/mslint/-/issues/37) Updated parser to support new `switchtype()`

Version 2.1.0
-------------

- [#35](https://gitlab.com/maniascript/mslint/-/issues/35) Exported more types

Version 2.0.0
-------------

- [#34](https://gitlab.com/maniascript/mslint/-/issues/34) Provided an easier to use interface for MSLint

Version 1.0.0
-------------

- Included a lot of new rules
- Added support for config files
- Lot of things ...

Version 0.10.0
-------------

- Do not bundle the module into a single file anymore

Version 0.9.0
-------------

- [#31](https://gitlab.com/maniascript/mslint/-/issues/31) Parsed cases with commas

Version 0.8.0
-------------

- [#30](https://gitlab.com/maniascript/mslint/-/issues/30) Updated ManiaScript parser

Version 0.7.1
-------------

- [#29](https://gitlab.com/maniascript/mslint/-/issues/29) Added missing chalk dependency

Version 0.7.0
-------------

- [#28](https://gitlab.com/maniascript/mslint/-/issues/28) Displayed the files that took the longest time to lint
- [#27](https://gitlab.com/maniascript/mslint/-/issues/27) Completed rewrite of the module in TypeScript

Version 0.6.1
-------------

- [#26](https://gitlab.com/maniascript/mslint/-/issues/26) Updated the parser settings of an MSLint instance

Version 0.6.0
-------------

- [#24](https://gitlab.com/maniascript/mslint/-/issues/24) Upgraded Maniascript parser
- [#21](https://gitlab.com/maniascript/mslint/-/issues/21) Added support for Maniascript API doc file
- [#23](https://gitlab.com/maniascript/mslint/-/issues/23) Added a --verbose option
- [#25](https://gitlab.com/maniascript/mslint/-/issues/25) Allowed one letter constants

Version 0.5.1
-------------

- [#20](https://gitlab.com/maniascript/mslint/-/issues/20) Upgraded Maniascript parser

Version 0.5.0
-------------

- [#19](https://gitlab.com/maniascript/mslint/-/issues/19) Preparation for the VSCode extension

Version 0.4.0
-------------

- Rename repo from Linter to MSLint.

Version 0.3.0
-------------

- Initial release. 🎉
