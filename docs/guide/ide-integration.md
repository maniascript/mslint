VS Code Extension
=================

MSLint for VS Code is available on the [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=aessi.vscode-mslint). The extension will lint your code and display the result directly in VS Code.

![linting](/vscode-extension.png)

Configuration
-------------

If you are using a single root workspace, the extension will look for an `mslint.json` configuration file in the root of the workspace.

::: tip Configuration File
Visit the [Configuration File](/guide/configuration-file) page for more information about the MSLint configuration file.
:::

Settings
--------

The extension offer the following settings:

* `mslint.lintOn`: Specifies when to lint the file. As you type, on save or on command only.
* `mslint.configFileName`: Name of the MSLint configuration file that the extension will search in the root of the workspace.
* `mslint.configFilePath`: Path to a configuration file to use instead of the one found in the root of the workspace.
