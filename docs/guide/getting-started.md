Getting Started
===============

MSLint is a tool that analyzes your ManiaScript code to find problematic patterns. It aims to help you and your team maintain consistent coding practices and prevent errors.

Prerequisites
-------------

To use MSLint you need:
- [Node.js](https://nodejs.org/) version 18 or higher
- A terminal to access MSLint via its Command Line Interface (CLI)

Installation
------------

To install MSLint as a local dependency in your project.

```sh
npm install --save-dev @maniascript/mslint
```

Basic Usage
-----------

MSLint comes with a default configuration set. So you can start linting without any further configuration. You only need to provide a [glob pattern](https://en.wikipedia.org/wiki/Glob_(programming)) of the files you want to lint.

```sh
npx mslint **/*.Script.txt
```

This will recursively lint all script files in the current working directory.

::: info Glob Pattern
MSLint uses [fast-glob](https://github.com/mrmlnc/fast-glob#basic-syntax) for pattern matching.
:::

Rules
-----

Rules are at the heart of MSLint's linting process. Each rule checks that your code meets a specific condition and raises an error if it does not. Rules can expose options allowing you to modify their behavior.

For example, the rule [id-length](/rules/id-length) will check that your variable names have a maximum length of 60 characters. But you can change this maximum to 40 characters.

::: tip Rules
Visit the [Rules](/rules/list) page for more information on all the rules available with MSLint.
:::

Configuration File
------------------

The MSLint configuration file is where you can setup MSLint for your project. Choose which files to lint, which rules to apply and how to apply them, which maniascript API to use, ...

::: tip Configuration File
Visit the [Configuration File](/guide/configuration-file) page for more information about the MSLint configuration file.
:::
