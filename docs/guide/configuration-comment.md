Configuration Comment
=====================

Disable Rules
-------------

Rules can be disabled within a file using specially formatted comments.

To disable all rules on the current or next line:

```maniascript
main() {
  sleep(1); // @mslint-disable-line
  sleep(1); /* @mslint-disable-line */

  // @mslint-disable-next-line
  sleep(1);
  /*
  @mslint-disable-next-line
  */
  sleep(1);
}
```

To disable a specific rule on the current or next line:

```maniascript
main() {
  sleep(1); // @mslint-disable-line no-sleep
  sleep(1); /* @mslint-disable-line no-sleep */

  // @mslint-disable-next-line no-sleep
  sleep(1);
  /*
  @mslint-disable-next-line no-sleep
  */
  sleep(1);
}
```

To disable multiple rules on the current or next line:

```maniascript
main() {
  sleep(1); wait(True); log(""); // @mslint-disable-line no-sleep, no-wait, no-log
  sleep(1); wait(True); log(""); /* @mslint-disable-line no-sleep, no-wait, no-log */

  // @mslint-disable-next-line no-sleep, no-wait, no-log
  sleep(1); wait(True); log("");
  /*
  @mslint-disable-next-line no-sleep, no-wait, no-log
  */
  sleep(1); wait(True); log("");
}
```

It is recommended that you add a description to the comment explaining why the rule was disabled. The description comes after the configuration and must be separated by two or more consecutive `-` characters.

```maniascript
main() {
  sleep(1); // @mslint-disable-line no-sleep -- Describe why the rule was disabled here

  /*
  @mslint-disable-next-line no-sleep, no-wait, no-log -- Describe why the rule was disabled here
  */
  sleep(1); wait(True); log("");
}
```
