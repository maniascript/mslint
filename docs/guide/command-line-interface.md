Command Line Interface
======================

The MSLint Command Line Interface (CLI) allows you to lint your files from a terminal.

Usage
-----

To run the MSLint CLI you need Node.js installed. Then you can execute MSLint with [npx](https://docs.npmjs.com/cli/v9/commands/npx) like this:

```sh
npx mslint [options] [file|glob-pattern]+
```

For example:

```sh
# Lint two files
npx mslint File1.Script.txt Scripts/File2.Script.txt

# Lint all files in the Media/ManiaApps and Scripts folders
npx mslint Media/ManiaApps/** Scripts/**
```

Options
-------

You can type `npx mslint --help` to see all available options.

```
Usage: mslint [options] [file|glob-pattern]+

  -h, --help                  Displays this help
  -v, --version               Displays MSLint version number
  -c, --config        String  Path to an MSLint configuration file - default: ""
  -l, --verbose               Displays information about the lint as it progresses
  -s, --stats                 Displays lint statistics at the end

  --report-unused-disable-directives  Report disable directive comments that are not useful
  --report-disable-directives-without-description  Report disable directive comments without a description
```

### `-h`, `--help`

Print MSLint help, listing all CLI options. If you use this option, all other options will be ignored. The CLI will not lint.

```sh
npx mslint --help
```

### `-v`, `--version`

Print MSLint version number. If you use this option, all other options will be ignored. The CLI will not lint.

```sh
npx mslint --version
```

### `-c`, `--config`

Allow you to specify a path to an MSLint configuration file (see [Configuration File](/guide/configuration-file) to learn more about configuration files).

Options specified from the command line override those specified in the configuration file.

The files and glob patterns from the command line are merged with those specified in the configuration file.

```sh
npx mslint --config ./path/to/mslint.json
```

### `-l`, `--verbose`

Displays information about the lint as it progresses: the file being linted and how long it took.

```sh
npx mslint --verbose **/*.Script.txt
```

### `-s`, `--stats`

Displays statistics at the end of the lint process: how many files were linted, how long it took, and which files were the slowest to process.

```sh
npx mslint --stats **/*.Script.txt
```

### `--report-unused-disable-directives`

Report directive comments like `// @mslint-disable-line` if no errors would have been reported on that line anyway.

```sh
npx mslint --report-unused-disable-directives **/*.Script.txt
```

### `--report-disable-directives-without-description`

When writing a disable directive, it's possible to describe why the directive was added :

```maniascript
// @mslint-disable-line rule-a, rule-b, rule-c -- Describe why the rules were disabled here
```

It is recommended that you always add a description. To make it easy to detect when this is not the case, the `--report-disable-directives-without-description` will create a report for this.

```sh
npx mslint --report-disable-directives-without-description **/*.Script.txt
```

Exit codes
----------

After linting files, MSLint exits with one of the following codes:

* `0` - The linting is complete and there are no linting errors.
* `1` - The linting is complete and there is at least one linting error.
* `2` - The linting failed before completion due to a configuration problem or an internal error.
