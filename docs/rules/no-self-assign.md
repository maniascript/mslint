<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-self-assign')
</script>

# no-self-assign

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits assignments where both sides are exactly the same.

Example of **incorrect** code for this rule:

```maniascript
main() {
  A = A; // [!code error]
  B <=> B; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  A = C; // [!code hl]
  B <=> D; // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
