<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'naming-convention-command')
</script>

# naming-convention-command

<RuleSummary :meta="meta" />

## Rule details

This rule enforces a few naming conventions for `#Command` directives:
* All command names must be prefixed with `Command_`

Example of **incorrect** code for this rule:

```maniascript
#Command A (Boolean) // [!code error]
#Command Com_B (Integer) as "B" // [!code error]
```

Example of **correct** code for this rule:

```maniascript
#Command Command_A (Boolean) // [!code hl]
#Command Command_B (Integer) as "B" // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
