<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'type-declaration')
</script>

# type-declaration

<RuleSummary :meta="meta" />

## Rule details

This rule imposes the presence or absence of the type in the variable declaration.

### Settings

* `type` can be `Explicite` to impose the presence of the type or `Implicite` to impose the absence of the type. Default `Explicite`.

Example of **incorrect** code for this rule:

```maniascript
// { "type" = "Explicite" }
declare A = 0; // [!code error]
declare B for This = 0; // [!code error]

// { "type" = "Implicite" }
declare Integer A; // [!code error]
declare Integer B = 0; // [!code error]
declare Integer C for This; // [!code error]
declare Integer D for This = 0; // [!code error]
```

Example of **correct** code for this rule:

```maniascript
// { "type" = "Explicite" }
declare Integer A; // [!code hl]
declare Integer B = 0; // [!code hl]
declare Integer C for This; // [!code hl]
declare Integer D for This = 0; // [!code hl]
let E = 0; // [!code hl]

// { "type" = "Implicite" }
declare A = 0; // [!code hl]
declare B for This = 0; // [!code hl]
let C = 0; // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
