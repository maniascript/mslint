<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-useless-concat')
</script>

# no-useless-concat

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits unnecessary string literal concatenation.

Example of **incorrect** code for this rule:

```maniascript
main() {
  "A" ^ "B"; // [!code error]
  C ^ "D" ^ "E" ^ "F" ^ G ^ "H"; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  "AB"; // [!code hl]
  C ^ "DEF" ^ G ^ "H"; // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
