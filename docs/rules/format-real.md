<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'format-real')
</script>

# format-real

<RuleSummary :meta="meta" />

## Rule details

This rule verifies that real numbers are formatted accurately with both integer and decimal parts.

### Settings

* `hasIntegerPart` real numbers must have an integer part. Default `true`.
* `hasDecimalPart` real numbers must have a decimal part. Default `false`.

Example of **incorrect** code for this rule:

```maniascript
// { "hasIntegerPart" = true, "hasDecimalPart" = false }
main() {
  .1; // [!code error]
}

// { "hasIntegerPart" = false, "hasDecimalPart" = true }
main() {
  1.; // [!code error]
}

// { "hasIntegerPart" = true, "hasDecimalPart" = true }
main() {
  .1; // [!code error]
  1.; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
// { "hasIntegerPart" = true, "hasDecimalPart" = false }
main() {
  1.0; // [!code hl]
  1.; // [!code hl]
}

// { "hasIntegerPart" = false, "hasDecimalPart" = true }
main() {
  0.1; // [!code hl]
  .1; // [!code hl]
}

// { "hasIntegerPart" = true, "hasDecimalPart" = true }
main() {
  1.0; // [!code hl]
}

// { "hasIntegerPart" = false, "hasDecimalPart" = false }
main() {
  1.0; // [!code hl]
  1.; // [!code hl]
  0.1; // [!code hl]
  .1; // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
