<script setup lang="ts">
import { data as rules } from '../data/rules.data.mjs'

function recommendedIcon(isRecommended: boolean) {
  return isRecommended ? '☑️' : '⬛'
}
</script>

# All rules

Rules marked with ☑️ are included in the `mslint:recommended` configuration.

<template v-for="rule of rules">
  <a :class="$style.ruleButton" :href="rule.url">
    <div :class="$style.content">
      <p :class="$style.name">{{ rule.name }}</p>
      <p :class="$style.description">{{ rule.description }}</p>
    </div>
    <div :class="$style.icons">{{ recommendedIcon(rule.recommended) }}</div>
  </a>
</template>

<style module>

a.ruleButton {
  border: 1px solid var(--vp-c-divider);
  border-radius: 0.5rem;
  margin: 0.5rem 0rem;
  padding: 1rem;
  text-decoration: none;
  transition: border-color 0.25s, background-color 0.25s;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  gap: 1rem;
}

a.ruleButton:hover {
  color: var(--vp-c-brand);
  border-color: var(--vp-c-brand);
  background-color: var(--vp-c-bg-soft);
}

.ruleButton .content {
  flex: 1 1 35ch;
}

.ruleButton .name {
  font-size: 1rem;
  margin: 0;
}

.ruleButton .description {
  color: var(--vp-c-text-2);
  font-size: 0.8rem;
  margin: 0;
}

.ruleButton .icons {
  border-radius: 0.5rem;
  background: var(--vp-c-bg);
  padding: 0.25rem;
}
</style>
