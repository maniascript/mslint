<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-empty')
</script>

# no-empty

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits empty block statements.

Example of **incorrect** code for this rule:

```maniascript
Void FunctionA() {
  // [!code error]
}
main() {
  foreach (A in B) {
    // [!code error]
  }

  for (I, 0, 10) {
    // [!code error]
  }

  while (True) {
    // [!code error]
  }

  meanwhile (True) {
    // [!code error]
  }

  switch (C) {
    case 1: {
      // [!code error]
    }
  }

  switchtype (D) {
    case CSmMode: {
      // [!code error]
    }
  }

  if (True) {
    // [!code error]
  } else if (True) {
    // [!code error]
  } else {
    // [!code error]
  }

  {
    // [!code error]
  }
}
```

Example of **correct** code for this rule:

```maniascript
Void FunctionA() {
  DoSomehting(); // [!code hl]
}
main() {
  foreach (A in B) {
    DoSomehting(); // [!code hl]
  }

  for (I, 0, 10) {
    DoSomehting(); // [!code hl]
  }

  while (True) {
    DoSomehting(); // [!code hl]
  }

  meanwhile (True) {
    DoSomehting(); // [!code hl]
  }

  switch (C) {
    case 1: {
      DoSomehting(); // [!code hl]
    }
  }

  switchtype (D) {
    case CSmMode: {
      DoSomehting(); // [!code hl]
    }
  }

  if (True) {
    DoSomehting(); // [!code hl]
  } else if (True) {
    DoSomehting(); // [!code hl]
  } else {
    DoSomehting(); // [!code hl]
  }

  {
    DoSomehting(); // [!code hl]
  }
}
```

## Resources

<RuleResources :meta="meta" />
