<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-lonely-if')
</script>

# no-lonely-if

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits using `if` statements as the sole statement within `else` blocks.

Example of **incorrect** code for this rule:

```maniascript
main() {
  if (A) {

  } else {
    if (B) { // [!code error]
      // [!code error]
    } // [!code error]
  }

  if (A) {

  } else {
    if (B) { // [!code error]
      // [!code error]
    } else if (C) { // [!code error]
      // [!code error]
    } // [!code error]
  }
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  if (A) {

  } else if (B) { // [!code hl]
    // [!code hl]
  } // [!code hl]

  if (A) {

  } else if (B) { // [!code hl]
    // [!code hl]
  } else if (C) { // [!code hl]
    // [!code hl]
  } // [!code hl]

  if (A) {

  } else {
    if (B) { // [!code hl]
      // [!code hl]
    } // [!code hl]
    DoSomehting(); // [!code hl]
  }
}
```

## Resources

<RuleResources :meta="meta" />
