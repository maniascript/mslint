<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'use-track-instead-of-map')
</script>

# use-track-instead-of-map

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits the utilization of the term "map" and recommends employing the term "track" instead in strings.

### Settings

* `onlyTranslatedText` search only in translated texts (`_("Text")`). Default `true`.

Example of **incorrect** code for this rule:

```maniascript
// { "onlyTranslatedText" = true }
#Const A _("This is a map") // [!code error]
#Const B _("These are maps") // [!code error]
#Const C "This is a map"
#Const D "These are maps"

// { "onlyTranslatedText" = false }
#Const A _("This is a map") // [!code error]
#Const B _("These are maps") // [!code error]
#Const C "This is a map" // [!code error]
#Const D "These are maps" // [!code error]
```

Example of **correct** code for this rule:

```maniascript
// { "onlyTranslatedText" = true }
#Const A _("This is a track") // [!code hl]
#Const B _("These are tracks") // [!code hl]

// { "onlyTranslatedText" = false }
#Const A _("This is a track") // [!code hl]
#Const B _("These are tracks") // [!code hl]
#Const C "This is a track" // [!code hl]
#Const D "These are tracks" // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
