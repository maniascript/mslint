<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-negated-condition')
</script>

# no-negated-condition

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits negated conditions in `if` statements that have an `else` clause.

Example of **incorrect** code for this rule:

```maniascript
main() {
  if (!A) { // [!code error]
    DoSomething();
  } else {
    DoAnotherThing();
  }

  if (B != C) { // [!code error]
    DoSomething();
  } else {
    DoAnotherThing();
  }
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  if (!A) {
    DoSomething();
  }

  if (B != C) {
    DoSomething();
  }

  if (!D) {
    DoSomething();
  } else if (E) {
    DoAnotherThing();
  }
}
```

## Resources

<RuleResources :meta="meta" />
