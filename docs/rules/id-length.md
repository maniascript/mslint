<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'id-length')
</script>

# id-length

<RuleSummary :meta="meta" />

## Rule details

This rule checks that the identifiers meet a minimum and/or maximum length. It will catch:

* setting declarations
* command declarations
* constant declarations
* structure declarations
* structure member declarations
* function declarations
* function arguments declarations
* variables declarations
* labels declarations

Only declarations are checked. This prevents errors from being raised by code outside your control, such as using a constant from a library.

### Settings

* `minimum` the minimum length of identifiers. Default `2`.
* `maximum` the maximum length of identifiers. Default `60`.
* `exceptions` a list of identifiers that are not checked by the rule. Default `[]`.

Example of **incorrect** code for this rule:

```maniascript
/*
{
  "minimum" = 2,
  "maximum" = 10,
  "exceptions" = ["I", "TooLongButIgnored"]
}
*/
#Setting TooLongSetting 1 // [!code error]
#Command TooLongCommand (Boolean) // [!code error]
#Const TooLongConstant 2 // [!code error]
#Struct TooLongStruct { // [!code error]
  Integer TooLongStructMember; // [!code error]
}

Void TooLongFunction() {} // [!code error]
Void Function(Integer TooLongFunctionArg) {} // [!code error]

***TooLongLabel*** // [!code hl]
***
TooLongVariable = 1; // [!code hl]
***

main() {
  declare Integer TooLongButIgnored;
  declare Integer[] TooLongVariable; // [!code error]
  +++TooLongLabel+++ // [!code error]
  for (I, 0, 10) {}
  for (J, 0, 10) {} // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
/*
{
  "minimum" = 2,
  "maximum" = 10,
  "exceptions" = ["I", "TooLongButIgnored"]
}
*/
#Setting Setting 1 // [!code hl]
#Command Command (Boolean) // [!code hl]
#Const Constant 2 // [!code hl]
#Struct Struct { // [!code hl]
  Integer Member; // [!code hl]
}

Void Function() {} // [!code hl]
Void Function(Integer Arg) {} // [!code hl]

main() {
  declare Integer[] Variable; // [!code hl]
  +++Label+++ // [!code hl]
  for (I, 0, 10) {}
  for (Count, 0, 10) {} // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
