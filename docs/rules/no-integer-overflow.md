<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-integer-overflow')
</script>

# no-integer-overflow

<RuleSummary :meta="meta" />

## Rule details

ManiaScript only partially supports 64-bit integers. This rule prohibits literal integers above the 32-bit limit.

Example of **incorrect** code for this rule:

```maniascript
main() {
  2147483648; // [!code error]
  -2147483649; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  2147483647; // [!code hl]
  -2147483648; // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
