<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'init-declaration')
</script>

# init-declaration

<RuleSummary :meta="meta" />

## Rule details

This rule enforces how variables must be initialized when they are declared.

### Settings

* `target` select how variables must be initialized when they are declared. Default `AlwaysTrait`. Can be :
  * `Always` - all variables must be initialized when they are declared.
  * `AlwaysTrait` - only traits (`declare for`) must be initialized when they are declared.
  * `Never` - no variables must be initialized when they are declared.

Example of **incorrect** code for this rule:

```maniascript
// { "target" = "Always" }
declare Integer A; // [!code error]
declare Integer B = 1;
declare Integer C for This; // [!code error]
declare Integer D for This = 1;

// { "target" = "AlwaysTrait" }
declare Integer A;
declare Integer B = 1;
declare Integer C for This; // [!code error]
declare Integer D for This = 1;

//{ "target" = "Never" }
declare Integer A;
declare Integer B = 1; // [!code error]
declare Integer C for This;
declare Integer D for This = 1; // [!code error]
```

Example of **correct** code for this rule:

```maniascript
// { "target" = "Always" }
declare Integer A = 1; // [!code hl]
declare Integer B = 1;
declare Integer C for This = 1; // [!code hl]
declare Integer D for This = 1;

// { "target" = "AlwaysTrait" }
declare Integer A;
declare Integer B = 1;
declare Integer C for This = 1; // [!code hl]
declare Integer D for This = 1;

// { "target" = "Never" }
declare Integer A;
declare Integer B; // [!code hl]
declare Integer C for This;
declare Integer D for This; // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
