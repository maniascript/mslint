<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-unused-include')
</script>

# no-unused-include

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits adding a library to a script if it will not be utilized later.

Example of **incorrect** code for this rule:

```maniascript
#Include "A" as A // [!code error]
#Include "B" as B

main() {
  B::B();
}
```

Example of **correct** code for this rule:

```maniascript
#Include "A" as A // [!code hl]
#Include "B" as B

main() {
  A::A(); // [!code hl]
  B::B();
}
```

## Resources

<RuleResources :meta="meta" />
