<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-continue')
</script>

# no-continue

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits using the `continue` statement.

Example of **incorrect** code for this rule:

```maniascript
main() {
  for (A, 1, 10) {
    if (A % 2 == 0) continue; // [!code error]
    log(A);
  }
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  for (A, 1, 10) {
    if (A % 2 != 0) { // [!code hl]
      log(A); // [!code hl]
    } // [!code hl]
  }
}
```

## Resources

<RuleResources :meta="meta" />
