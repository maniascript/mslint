<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-dupe-case')
</script>

# no-dupe-case

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits duplicate `case` conditions in `switch` statements.

Example of **incorrect** code for this rule:

```maniascript
main() {
  switch (A) {
    case 1: {}
    case 1: {} // 1 already catched above // [!code error]
    case 2, 3: {}
    case 3: {} // 3 already catched above // [!code error]
    case 1, 2, 3, 4: {} // 1, 2 and 3 already catched above // [!code error]
    case 4, 1, 2: {} // 4, 1 and 2 already catched above // [!code error]
    case 1+1: {}
    case     1  +   1     : {} // 1 + 1 already catched above // [!code error]
    case 5, 6, 5, 6: {} // 5 and 6 already catched in the same case // [!code error]
  }
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  switch (A) {
    case 1: {}
    case 2, 3: {}
    case 4: {} // [!code hl]
    case 1+1: {}
    case 5, 6: {} // [!code hl]
  }
}
```

## Resources

<RuleResources :meta="meta" />
