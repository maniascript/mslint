<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'naming-convention-function')
</script>

# naming-convention-function

<RuleSummary :meta="meta" />

## Rule details

This rule enforces a few naming conventions for function and parameter names:
* All function names must start with an uppercase letter
* All function parameter names must start with an underscore and be followed by uppercase letter

Example of **incorrect** code for this rule:

```maniascript
Void functionA() {} // [!code error]
Void _FunctionB() {} // [!code error]
Void FunctionC(Integer ParamC) {} // [!code error]
Void FunctionD(Integer _paramD) {} // [!code error]
Void FunctionE(Integer paramE) {} // [!code error]
```

Example of **correct** code for this rule:

```maniascript
Void FunctionA() {} // [!code hl]
Void FunctionB() {} // [!code hl]
Void FunctionC(Integer _ParamC) {} // [!code hl]
Void FunctionD(Integer _ParamD) {} // [!code hl]
Void FunctionE(Integer _ParamE) {} // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
