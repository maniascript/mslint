<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'naming-convention-structure')
</script>

# naming-convention-structure

<RuleSummary :meta="meta" />

## Rule details

This rule enforces a few naming conventions for `#Struct` directives:
* All structure names must be prefixed with `K_`
* Libraries can add another prefix before the `K_` prefix, for example: `MyLibPrefix_K_`

Example of **incorrect** code for this rule:

```maniascript
#Struct A { // [!code error]
  Integer A;
}
#Struct B::B as B // [!code error]
#Struct Lib_C { // [!code error]
  Integer C;
}
```

Example of **correct** code for this rule:

```maniascript
#Struct K_A { // [!code hl]
  Integer A;
}
#Struct B::B as K_B // [!code hl]
#Struct Lib_K_C { // [!code hl]
  Integer C;
}
```

## Resources

<RuleResources :meta="meta" />
