<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-self-compare')
</script>

# no-self-compare

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits comparisons where both sides are exactly the same.

Example of **incorrect** code for this rule:

```maniascript
main() {
  if (A == A) {} // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  if (A == B) {} // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
