<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'max-params')
</script>

# max-params

<RuleSummary :meta="meta" />

## Rule details

This rule restricts the number of parameters allowed in function declarations.

### Settings

* `maximum` the maximum number of parameters allowed in function declarations. Default `10`.

Example of **incorrect** code for this rule:

```maniascript
// { "maximum" = 3 }
Void Function(Integer _Param1, Integer _Param2, Integer _Param3, Integer _Param4) { // [!code error]
  DoThings();
}
```

Example of **correct** code for this rule:

```maniascript
// { "maximum" = 3 }
Void Function(Integer _Param1, Integer _Param2, Integer _Param3) { // [!code hl]
  DoThings();
}
```

## Resources

<RuleResources :meta="meta" />
