<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-log')
</script>

# no-log

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits log statements.

Example of **incorrect** code for this rule:

```maniascript
main() {
  log("This"); // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  NotLogging("This"); // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
