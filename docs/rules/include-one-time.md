<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'include-one-time')
</script>

# include-one-time

<RuleSummary :meta="meta" />

## Rule details

This rule ensures that the same file is not included multiple times by an `#Include`.

Example of **incorrect** code for this rule:

```maniascript
#Include "Path/To/FileA.Script.txt"
#Include "Path/To/FileA.Script.txt" // [!code error]
#Include "Path/To/FileA.Script.txt" as C // [!code error]
```

Example of **correct** code for this rule:

```maniascript
#Include "Path/To/FileA.Script.txt"
#Include "Path/To/FileB.Script.txt" // [!code hl]
#Include "Path/To/FileC.Script.txt" as C // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
