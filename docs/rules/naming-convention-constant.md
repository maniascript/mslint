<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'naming-convention-constant')
</script>

# naming-convention-constant

<RuleSummary :meta="meta" />

## Rule details

This rule enforces a few naming conventions for `#Const` directives:
* All constant names must be prefixed with `C_`
* Libraries can add another prefix before the `C_` prefix, for example: `MyLibPrefix_C_`
* One letter constant names are allowed. eg: `A`, `B`, `P`, `W`, ...

The following special constants are exempt from these restrictions:
* `CompatibleMapTypes`
* `Version`
* `ScriptName`
* `Description`
* `CustomMusicFolder`

### Settings

* `exceptions` a list of constant names that are not checked by the rule. Default `[]`.

Example of **incorrect** code for this rule:

```maniascript
/*
{
  "exceptions" = ["ConstIgnored"]
}
*/
#Const ConstA 1 // [!code error]
#Const CC_ConstB 2 // [!code error]
#Const C::ConstC as ConstC // [!code error]
#Const Lib_ConstD 4 // [!code error]
#Const ConstNotIgnored 5 // [!code error]
```

Example of **correct** code for this rule:

```maniascript
/*
{
  "exceptions" = ["ConstIgnored"]
}
*/
#Const C_ConstA 1 // [!code hl]
#Const C_ConstB 2 // [!code hl]
#Const C::ConstC as C_ConstC // [!code hl]
#Const Lib_C_ConstD 4 // [!code hl]
#Const ConstIgnored 5 // [!code hl]
#Const E 6
#Const CompatibleMapTypes "Race"
#Const Version "1.0.0"
#Const ScriptName "Path/To/Script/Name.Script.txt"
#Const Description "Description of the game mode"
```

## Resources

<RuleResources :meta="meta" />
