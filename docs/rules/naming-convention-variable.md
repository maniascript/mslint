<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'naming-convention-variable')
</script>

# naming-convention-variable

<RuleSummary :meta="meta" />

## Rule details

This rule enforces a few naming conventions for variable declarations:
* Globale variables must be prefixed with `G_`
* All variable names must start with an uppercase letter

Example of **incorrect** code for this rule:

```maniascript
declare Integer VariableA; // [!code error]
declare Integer G_; // [!code error]
declare Integer g_variableC; // [!code error]

main() {
  declare Integer variableD; // [!code error]
  declare Integer _VariableE; // [!code error]
  declare Integer variableF as variableG; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
declare Integer G_VariableA; // [!code hl]
declare Integer G_VariableB; // [!code hl]
declare Integer G_variableC; // [!code hl]

main() {
  declare Integer VariableD; // [!code hl]
  declare Integer VariableE; // [!code hl]
  declare Integer VariableF as VariableG; // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
