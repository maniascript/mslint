<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-dupe-parameters')
</script>

# no-dupe-parameters

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits duplicate parameter names in function declarations.

Example of **incorrect** code for this rule:

```maniascript
Void FunctionA(
  Integer _A1,
  Integer _A1 // [!code error]
) {}
Void FunctionB(
  Integer _B1,
  Text _B1 // [!code error]
) {}
Void FunctionC(
  Integer _C1,
  Text _C2,
  Integer _C1, // [!code error]
  Text _C2 // [!code error]
) {}
```

Example of **correct** code for this rule:

```maniascript
Void FunctionA(
  Integer _A1,
  Integer _A2 // [!code hl]
) {}
Void FunctionB(
  Integer _B1,
  Text _B2 // [!code hl]
) {}
Void FunctionC(
  Integer _C1,
  Text _C2,
  Integer _C3, // [!code hl]
  Text _C4 // [!code hl]
) {}
```

## Resources

<RuleResources :meta="meta" />
