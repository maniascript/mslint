<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'max-statements')
</script>

# max-statements

<RuleSummary :meta="meta" />

## Rule details

This rule limits the number of statements in three contexts: function, label and main.

### Settings

* `maximum`
  * `function` the maximum number of statements in functions. Default `100`.
  * `label` the maximum number of statements in labels. Default `100`.
  * `main` the maximum number of statements in main. Default `100`.

Example of **incorrect** code for this rule:

```maniascript
/*
{
  "maximum" = {
    "function" = 2,
    "label" = 2,
    "main" = 2
  }
}
*/
Void Function() {
  declare Integer A = 1;

  declare Integer B = 2;

  // White spaces or comment are not statements
  declare Integer C = 3; // [!code error]
}

***Label***
***
declare Integer A = 1;

declare Integer B = 2;

declare Integer C = 3; // [!code error]
***

main() {
  declare Integer A = 1;

  declare Integer B = 2;

  declare Integer C = 3; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
/*
{
  "maximum" = {
    "function" = 2,
    "label" = 2,
    "main" = 2
  }
}
*/
Void Function() {
  declare Integer A = 1;

  declare Integer B = 2;
}

***Label***
***
declare Integer A = 1;

declare Integer B = 2;
***

main() {
  declare Integer A = 1;

  declare Integer B = 2;
}
```

## Resources

<RuleResources :meta="meta" />
