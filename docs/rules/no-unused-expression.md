<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-unused-expression')
</script>

# no-unused-expression

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits expressions that do not affect on the program's state.

Example of **incorrect** code for this rule:

```maniascript
main() {
  "ABC"; // [!code error]
  A + B; // [!code error]
  A == B; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  DoSomething(); // [!code hl]
  A = 0; // [!code hl]
  A += B; // [!code hl]
  if (A == B) {} // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
