<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'format-if-else')
</script>

# format-if-else

<RuleSummary :meta="meta" />

## Rule details

This rule checks that the consequents that are not enclosed in braces are placed on the same line as their condition.

Example of **incorrect** code for this rule:

```maniascript
main() {
  if (A)
    A(); // [!code error]
  else if (B)
    B(); // [!code error]
  else
    C(); // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  if (A) A(); // [!code hl]
  else if (B) B(); // [!code hl]
  else if (
    C
  ) C(); // [!code hl]
  else if (D) {
    D(); // [!code hl]
  }
  else E(); // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
