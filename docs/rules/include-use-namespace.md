<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'include-use-namespace')
</script>

# include-use-namespace

<RuleSummary :meta="meta" />

## Rule details

This rule checks that all included files have a namespace and that the namespace is unique.

Example of **incorrect** code for this rule:

```maniascript
#Include "Path/To/LibA.Script.txt" //< Missing namespace // [!code error]
#Include "Path/To/LibB.Script.txt" as LibB
#Include "Path/To/LibC.Script.txt" as LibB //< Duplicated namespace // [!code error]
```

Example of **correct** code for this rule:

```maniascript
#Include "Path/To/LibA.Script.txt" as LibA // [!code hl]
#Include "Path/To/LibB.Script.txt" as LibB
#Include "Path/To/LibC.Script.txt" as LibC // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
