<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'deprecated-initializer-type')
</script>

# deprecated-initializer-type

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits the use of a type name to initialize a value. You must use a literal instead.

Example of **incorrect** code for this rule:

```maniascript
Void Function_01(Integer _Param1) {
  return Integer; // [!code error]
}

main() {
  declare Integer Variable_01 = Integer; // [!code error]
  Variable_01 = Integer; // [!code error]
  Function_01(Integer); // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
Void Function_01(Integer _Param1) {
  return 1; // [!code hl]
}

main() {
  declare Integer Variable_01 = 2; // [!code hl]
  Variable_01 = 3; // [!code hl]
  Function_01(4); // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
