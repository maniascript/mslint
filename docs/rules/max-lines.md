<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'max-lines')
</script>

# max-lines

<RuleSummary :meta="meta" />

## Rule details

This rule limits the number of lines of code in three contexts: function, label and main.

### Settings

* `maximum`
  * `function` the maximum number of lines of code in functions. Default `1000`.
  * `label` the maximum number of lines of code in labels. Default `1000`.
  * `main` the maximum number of lines of code in main. Default `1000`.

Example of **incorrect** code for this rule:

```maniascript
/*
{
  "maximum" = {
    "function" = 2,
    "label" = 2,
    "main" = 2
  }
}
*/
Void Function() {
  declare Integer A = 1;
  declare Integer B = 2;
  declare Integer C = 3; // [!code error]
}

***Label***
***
declare Integer A = 1;
declare Integer B = 2;
declare Integer C = 3; // [!code error]
***

main() {
  declare Integer A = 1;
  declare Integer B = 2;
  declare Integer C = 3; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
/*
{
  "maximum" = {
    "function" = 2,
    "label" = 2,
    "main" = 2
  }
}
*/
Void Function() {
  declare Integer A = 1;
  declare Integer B = 2;
}

***Label***
***
declare Integer A = 1;
declare Integer B = 2;
***

main() {
  declare Integer A = 1;
  declare Integer B = 2;
}
```

## Resources

<RuleResources :meta="meta" />
