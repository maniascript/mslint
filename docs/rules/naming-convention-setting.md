<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'naming-convention-setting')
</script>

# naming-convention-setting

<RuleSummary :meta="meta" />

## Rule details

This rule enforces a few naming conventions for `#Settings` directives:
* All setting names must be prefixed with `S_`

Example of **incorrect** code for this rule:

```maniascript
#Setting SettingA 1 // [!code error]
#Setting SettingB 2 as "B" // [!code error]
```

Example of **correct** code for this rule:

```maniascript
#Setting S_SettingA 1 // [!code hl]
#Setting S_SettingB 2 as "B" // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
