<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-sleep')
</script>

# no-sleep

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits sleep statements.

Example of **incorrect** code for this rule:

```maniascript
main() {
  sleep(1000); // [!code error]
}
```

## Resources

<RuleResources :meta="meta" />
