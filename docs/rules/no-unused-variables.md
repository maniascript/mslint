<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-unused-variables')
</script>

# no-unused-variables

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits unused variables and function parameters.

Example of **incorrect** code for this rule:

```maniascript
Integer FunctionA(
  Integer _A, // [!code error]
  Integer _B
) {
  return _B + 10;
}

main() {
  declare Integer C; // [!code error]
  declare Integer D; // [!code error]
  D = 1; //< Assigning a value is not enough, the variable must be read to be considered used
}
```

Example of **correct** code for this rule:

```maniascript
Integer FunctionA(
  Integer _A, // [!code hl]
  Integer _B
) {
  return _B + _A; // [!code hl]
}

main() {
  declare Integer C; // [!code hl]
  declare Integer D; // [!code hl]
  D = 1;
  Function(C); // [!code hl]
  if (D > 0) { // [!code hl]

  }
}
```

## Resources

<RuleResources :meta="meta" />
