<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'include-use-common-namespace')
</script>

# include-use-common-namespace

<RuleSummary :meta="meta" />

## Rule details

This rule enforces the use of particular namespaces when including common libraries.

* `MathLib` must use `ML`
* `TextLib` must use `TL`
* `MapUnits` must use `MU`
* `AnimLib` must use `AL`
* `TimeLib` must use `TiL`
* `ColorLib` must use `CL`

Example of **incorrect** code for this rule:

```maniascript
#Include "MathLib" as A // [!code error]
#Include "TextLib" as B // [!code error]
#Include "MapUnits" as C // [!code error]
#Include "AnimLib" as D // [!code error]
#Include "TimeLib" as E // [!code error]
#Include "ColorLib" as F // [!code error]
```

Example of **correct** code for this rule:

```maniascript
#Include "MathLib" as ML // [!code hl]
#Include "TextLib" as TL // [!code hl]
#Include "MapUnits" as MU // [!code hl]
#Include "AnimLib" as AL // [!code hl]
#Include "TimeLib" as TiL // [!code hl]
#Include "ColorLib" as CL // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
