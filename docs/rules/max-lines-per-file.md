<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'max-lines-per-file')
</script>

# max-lines-per-file

<RuleSummary :meta="meta" />

## Rule details

This rule limits the number of lines of code in a file.

### Settings

* `maximum` the maximum number of lines of code in a file. Default `5000`.

Example of **incorrect** code for this rule:

```maniascript
// { "maximum" = 4 }
Void FunctionA() {

}
main() {
  FunctionA(); // [!code error]
} // [!code error]
```

Example of **correct** code for this rule:

```maniascript
// { "maximum" = 4 }
main() {
  declare Integer A = 1;
}
```

## Resources

<RuleResources :meta="meta" />
