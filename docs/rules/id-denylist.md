<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'id-denylist')
</script>

# id-denylist

<RuleSummary :meta="meta" />

## Rule details

This rule ensures that identifiers are not included in a given list. It will catch:

* setting declarations
* command declarations
* constant declarations
* structure declarations
* structure member declarations
* function declarations
* function arguments declarations
* variables declarations
* labels declarations

Only declarations are checked. This prevents errors from being raised by code outside your control, such as using a constant from a library.

### Settings

* `list` a list of values that identifiers cannot use. Default `[]`.

Example of **incorrect** code for this rule:

```maniascript
/*
{
  "list" = [
    "ForbiddenSetting",
    "ForbiddenCommand",
    "ForbiddenConstant",
    "ForbiddenStruct",
    "ForbiddenStructMember",
    "ForbiddenFunction",
    "ForbiddenFunctionArg",
    "ForbiddenVariable",
    "ForbiddenLabel"
  ]
}
*/
#Setting ForbiddenSetting 1 // [!code error]
#Command ForbiddenCommand (Boolean) // [!code error]
#Const ForbiddenConstant 2 // [!code error]
#Struct ForbiddenStruct { // [!code error]
  Integer ForbiddenStructMember; // [!code error]
}
Void ForbiddenFunction() {} // [!code error]
Void ValidFunction(Integer ForbiddenFunctionArg) {} // [!code error]
main() {
  declare Integer[] ForbiddenVariable; // [!code error]
  +++ForbiddenLabel+++ // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
/*
{
  "list" = [
    "ForbiddenSetting",
    "ForbiddenCommand",
    "ForbiddenConstant",
    "ForbiddenStruct",
    "ForbiddenStructMember",
    "ForbiddenFunction",
    "ForbiddenFunctionArg",
    "ForbiddenVariable",
    "ForbiddenLabel"
  ]
}
*/
***ForbiddenLabel*** // [!code hl]
***
ForbiddenVariable = 1; // [!code hl]
***

main() {
  declare Integer A = ForbiddenSetting; // [!code hl]
  declare Integer B = ForbiddenConstant; // [!code hl]
  declare ForbiddenStruct C = ForbiddenStruct { // [!code hl]
    ForbiddenStructMember = 1 // [!code hl]
  };
  ForbiddenFunction(); // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
