<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-now-class-member')
</script>

# no-now-class-member

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits the use of the `Now` class member in favor of the `Now` keyword.

Example of **incorrect** code for this rule:

```maniascript
main() {
  This.Now; // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  Now; // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
