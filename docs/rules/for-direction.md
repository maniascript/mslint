<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'for-direction')
</script>

# for-direction

<RuleSummary :meta="meta" />

## Rule details

This rule checks that the starting value of the for loop is less than the ending value.

Example of **incorrect** code for this rule:

```maniascript
main() {
  for (A, 10, 1) {} // [!code error]
  for (A, 10, 1, -2) {} // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  for (A, 1, 10) {} // [!code hl]
  for (A, 1, 10, -2) {} // [!code hl]
}
```

## Resources

<RuleResources :meta="meta" />
