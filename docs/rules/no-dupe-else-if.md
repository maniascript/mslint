<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-dupe-else-if')
</script>

# no-dupe-else-if

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits duplicate conditions in the same `if-else-if` chain.

Example of **incorrect** code for this rule:

```maniascript
main() {
  if (A) {}
  else if (A) {} // [!code error]

  if (A) {}
  else if (B) {}
  else if (A) {} // [!code error]
  else if (C) {}

  if (A) {}
  else if (B) {}
  else if (A) {} // [!code error]
  else if (B) {} // [!code error]
  else if (A) {} // [!code error]

  if (A || B) {}
  else if (A) {} // [!code error]
  else if (B) {} // [!code error]

  if (A || B) {}
  else if (B || A) {} // [!code error]

  if (A) {}
  else if (B) {}
  else if (A || B) {} // [!code error]

  if (A || B) {}
  else if (C || D) {}
  else if (A || D) {} // [!code error]

  if (A) {}
  else if (A && B) {} // [!code error]
}
```

Example of **correct** code for this rule:

```maniascript
main() {
  if (A) {}
  else if (B) {}

  if (A || B) {}
  else if (C) {}
  else if (D) {}

  if (A || B) {}
  else if (C || D) {}

  if (A && B) {}
  else if (A) {}
}
```

## Resources

<RuleResources :meta="meta" />
