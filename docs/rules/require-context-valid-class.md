<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'require-context-valid-class')
</script>

# require-context-valid-class

<RuleSummary :meta="meta" />

## Rule details

This rule requires a valid script execution context to be used in the `#RequireContext` directive.

Valid contexts are :

* `CManiaAppTitle`
* `CManiaAppStation`
* `CManiaAppPlayground`
* `CSmMode`
* `CTmMode`
* `CSmMapType`
* `CTmMapType`
* `CMapEditorPlugin`
* `CServerPlugin`
* `CSmAction`
* `CManiaplanetPlugin`
* `CAnyEditorPlugin`

### Settings

* `validClasses` a list of class names that are considered valid by the rule. Default `[]`.
* `invalidClasses` a list of class names that are considered invalid by the rule. Default `[]`.

Example of **incorrect** code for this rule:

```maniascript
/*
{
  "validClasses" = ["CPlayer"],
  "invalidClasses" = ["CMode"]
}
*/
#RequireContext CSmPlayer // [!code error]
#RequireContext CUser // [!code error]
#RequireContext CMode // [!code error]
```

Example of **correct** code for this rule:

```maniascript
/*
{
  "validClasses" = ["CPlayer"],
  "invalidClasses" = ["CMode"]
}
*/
#RequireContext CPlayer // [!code hl]
#RequireContext CManiaAppTitle // [!code hl]
#RequireContext CManiaAppStation // [!code hl]
#RequireContext CManiaAppPlayground // [!code hl]
#RequireContext CSmMode // [!code hl]
#RequireContext CTmMode // [!code hl]
#RequireContext CSmMapType // [!code hl]
#RequireContext CTmMapType // [!code hl]
#RequireContext CMapEditorPlugin // [!code hl]
#RequireContext CServerPlugin // [!code hl]
#RequireContext CSmAction // [!code hl]
#RequireContext CManiaplanetPlugin // [!code hl]
#RequireContext CAnyEditorPlugin // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
