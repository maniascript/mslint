<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'trim-array-type')
</script>

# trim-array-type

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits the use of white spaces and new lines in array types.

Example of **incorrect** code for this rule:

```maniascript
declare Integer[ ][Real] A; // [!code error]
declare Integer[] [Real] B; // [!code error]
declare Integer [][Real] C; // [!code error]
declare Integer[] // [!code error]
[Real] D; // [!code error]
```

Example of **correct** code for this rule:

```maniascript
declare Integer[][Real] A; // [!code hl]
```

## Resources

<RuleResources :meta="meta" />
