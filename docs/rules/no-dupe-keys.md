<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'no-dupe-keys')
</script>

# no-dupe-keys

<RuleSummary :meta="meta" />

## Rule details

This rule prohibits duplicate keys in the same associative array.

Example of **incorrect** code for this rule:

```maniascript
declare Integer[Text] ArrayA = [
  "A" => 1,
  "A" => 2 // [!code error]
];
declare Integer[Integer] ArrayB = [
  1 => 1,
  1 => 2 // [!code error]
];
declare Integer[Integer][Integer] ArrayC = [
  1 => [
    10 => 10,
    10 => 11 // [!code error]
  ],
  1 => [ // [!code error]
    10 => 20,
    11 => 21
  ]
];
```

Example of **correct** code for this rule:

```maniascript
declare Integer[Text] ArrayA = [
  "A" => 1,
  "B" => 2 // [!code hl]
];
declare Integer[Integer] ArrayB = [
  1 => 1,
  2 => 2 // [!code hl]
];
declare Integer[Integer][Integer] ArrayC = [
  1 => [
    10 => 10,
    11 => 11 // [!code hl]
  ],
  2 => [ // [!code hl]
    10 => 20,
    11 => 21
  ]
];
```

## Resources

<RuleResources :meta="meta" />
