<script setup lang="ts">
import RuleSummary from '../components/RuleSummary.vue'
import RuleResources from '../components/RuleResources.vue'
import { data as rules } from '../data/rules.data.mjs'

const meta = rules.find(rule => rule.name === 'max-depth')
</script>

# max-depth

<RuleSummary :meta="meta" />

## Rule details

This rule limits the depth of nested blocks to reduce code complexity.

### Settings

* `maximum` the maximum depth of nested blocks. Default `8`.

Example of **incorrect** code for this rule:

```maniascript
// { "maximum" = 4 }
Void Function () {
  if (True) {
    // depth 1
    if (True) {
      // depth 2
      if (True) {
        // depth 3
        if (True) {
          // depth 4
          if (True) { // [!code error]
            // depth 5 // [!code error]
            if (True) { // [!code error]
              // depth 6 // [!code error]
            } // [!code error]
          } // [!code error]
        }
      }
    }
  }
}
```

Example of **correct** code for this rule:

```maniascript
// { "maximum" = 4 }
Void Function () {
  if (True) {
    // depth 1
    if (True) {
      // depth 2
      if (True) {
        // depth 3
        if (True) {
          // depth 4
          declare Integer A = 1; // [!code hl]
        }
      }
    }
  }
}
```

## Resources

<RuleResources :meta="meta" />
