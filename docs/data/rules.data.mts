import { defineLoader } from 'vitepress'
import { rules } from '../../src/index.js'

export interface RulePage {
  url: string
  name: string
  description: string
  recommended?: boolean
}

export default defineLoader({
  load () {
    const data: RulePage[] = []
    for (const [ruleId, rule] of rules.allRules) {
      if (ruleId !== '_template_id') {
        data.push({
          url: `/rules/${ruleId}`,
          name: ruleId,
          description: rule.meta.description,
          recommended: rule.meta.recommended
        })
      }
    }
    return data
  }
})
