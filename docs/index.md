---
# https://vitepress.dev/reference/default-theme-home-page
layout: home
titleTemplate: "Linting for ManiaScript"

hero:
  name: MSLint
  text: Linting for ManiaScript
  tagline: MSLint analyzes your ManiaScript code to find common problems and help you fix them.
  image:
    src: /logo.png
    alt: MSLint logo
  actions:
    - theme: brand
      text: Get Started
      link: /guide/getting-started
    - theme: alt
      text: Rules
      link: /rules/list

features:
  - title: Fully configurable
    details: Choose which rules to apply and how to apply them
    link: /guide/configuration-file
    icon:
      src: /fully-configurable.svg
      alt: VS Code Extension
  - title: Command Line Interface
    details: Easy to integrate into your CI pipelines for automatic code validation
    link: /guide/command-line-interface
    icon:
      src: /continuous-integration.png
      alt: Continuous Integration
  - title: VS Code Extension
    details: Also available as a VS Code extension for on-the-fly code validation
    link: /guide/ide-integration#vs-code
    icon:
      src: /vscode-extension.svg
      alt: VS Code Extension
---

