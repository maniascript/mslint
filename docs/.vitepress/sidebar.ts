import type { RulePage } from '../data/rules.data.mjs'
import rulesData from '../data/rules.data.mjs'

interface NavItem {
  text: string
  link: string
}

function getRulesSidebar() {
  const items: NavItem[] = []
  const rules: RulePage[] = rulesData.load([])
  for (const rule of rules) {
    items.push({
      text: rule.name,
      link: rule.url
    })
  }
  return [
    {
      text: 'Rules',
      items: [
        {
          text: 'Index',
          link: '/rules/list'
        },
        {
          items
        }
      ]
    }
  ]
}

export {
  getRulesSidebar
}
