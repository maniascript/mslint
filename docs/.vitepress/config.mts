import { defineConfig } from 'vitepress'
import { getRulesSidebar } from './sidebar'
import maniascriptLang from './maniascript.json' assert { type: 'json' }

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "MSLint",
  description: "Linting for ManiaScript",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: '/logo.png',
    nav: [
      { text: 'Guide', link: '/guide/getting-started', activeMatch: '/guide/' },
      { text: 'Rules', link: '/rules/list', activeMatch: '/rules/' },
      { text: 'Changelog', link: 'https://gitlab.com/maniascript/mslint/-/blob/main/CHANGELOG.md' }
    ],
    sidebar: {
      '/guide/': [
        {
          text: 'Guide',
          items: [
            { text: 'Getting Started', link: '/guide/getting-started' },
            { text: 'Configuration File', link: '/guide/configuration-file' },
            { text: 'Configuration Comment', link: '/guide/configuration-comment' },
            { text: 'Command Line Interface', link: '/guide/command-line-interface' },
            { text: 'VS Code Extension', link: '/guide/ide-integration' }
          ]
        }
      ],
      '/rules/': getRulesSidebar()
    },
    outline: [2, 3],
    socialLinks: [
      {
        icon: {
          svg : '<svg xmlns="http://www.w3.org/2000/svg" width="193" height="186" viewBox="153 157 193 186"><g transform="matrix(1 0 0 1 250 250)"><path transform=" translate(-190, -190)" d="M 282.83 170.73 L 282.56 170.04 L 256.42 101.82 C 255.88816237369588 100.48292267912402 254.9464564516972 99.34867465411446 253.73000000000002 98.58 C 251.23681453513302 97.03230184743333 248.0429227664042 97.2039735300025 245.73000000000002 99.01 C 244.61332337075052 99.91705108587878 243.80324215417158 101.1461398282744 243.41000000000003 102.53 L 225.76000000000002 156.53 L 154.29 156.53 L 136.64 102.53 C 136.25720823168356 101.13860148833241 135.44537495700223 99.90335516952847 134.32 99 C 132.00707723359582 97.1939735300025 128.813185464867 97.02230184743333 126.32 98.57 C 125.10627359198936 99.34177843185367 124.16539055705245 100.47503532151745 123.63 101.80999999999999 L 97.44 170 L 97.17999999999999 170.69 C 89.44950991924833 190.888540765201 96.01474693012453 213.76492562918577 113.28 226.79 L 113.37 226.85999999999999 L 113.61 227.02999999999997 L 153.43 256.84999999999997 L 173.13 271.76 L 185.13 280.82 C 188.01470179553982 283.01023402368014 192.00529820446016 283.01023402368014 194.89 280.82 L 206.89 271.76 L 226.58999999999997 256.84999999999997 L 266.65 226.84999999999997 L 266.75 226.76999999999995 C 283.97487080079367 213.74375478112856 290.5274827738871 190.90741305400462 282.83 170.72999999999996 Z" /></g></svg>'
        },
        link: 'https://gitlab.com/maniascript/mslint',
        ariaLabel: 'MSLint git repository on GitLab'
      }
    ],
    footer: {
      message: 'Released under the <a href="https://opensource.org/license/lgpl-3-0/" target="_blank" rel="noreferrer">LGPL 3.0 License</a>.'
    },
    search: {
      provider: 'local'
    },
  },
  cleanUrls: true,
  markdown: {
    languages: [
      maniascriptLang as any
    ],
    theme: {
      light: 'slack-ochin',
      dark: 'material-theme-palenight'
    }
  }
})
