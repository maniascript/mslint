#!/usr/bin/env node

import process from 'node:process'
import { format } from 'util'
import { execute } from '../lib/linter/cli.js'
import * as output from '../lib/linter/output.js'
import { MSLintError, MSLintConfigValidationError } from '../lib/linter/error.js'

function onFatalError (error: unknown): void {
  if (error instanceof MSLintError || error instanceof MSLintConfigValidationError) {
    // Error from MSLint
    output.error(`\n${output.formatTextColorRed(error.message)}\n`)
  } else {
    let message: string

    if (typeof error !== 'object' || error === null) {
      // If a third party modules throw some exotic error
      message = String(error)
    } else if ('stack' in error && typeof error.stack === 'string') {
      // Return the stacktrace if it's an error object
      message = error.stack
    } else {
      // Otherwise dump the whole error object
      message = format('%o', error)
    }

    output.error(`\nSomething went wrong. :(\n\n${output.formatTextColorRed(message)}\n`)
  }

  process.exitCode = 2
}

async function main (): Promise<void> {
  process.exitCode = await execute(process.argv.slice(2), process.cwd())
}

main().catch((error: unknown) => { onFatalError(error) })
