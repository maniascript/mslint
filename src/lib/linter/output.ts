function formatNanoseconds (time: bigint): string {
  const seconds = time / 1000000000n
  let remain = time % 1000000000n
  const milliseconds = remain / 1000000n
  remain = remain % 1000000n
  return `${seconds.toString()}s${milliseconds.toString().padStart(3, '0')}ms${remain.toString().padStart(6, '0')}ns`
}

function formatTextUnderline (message: string): string {
  return `\u001B[4m${message}\u001B[24m`
}

function formatTextDim (message: string): string {
  return `\u001B[2m${message}\u001B[22m`
}

function formatTextColorRed (message: string): string {
  return `\u001B[31m${message}\u001B[39m`
}

function formatTextColorOrange (message: string): string {
  return `\u001B[33m${message}\u001B[39m`
}

function info (text: string): void {
  console.log(text)
}

function error (text: string): void {
  console.error(text)
}

export {
  formatNanoseconds,
  formatTextUnderline,
  formatTextDim,
  formatTextColorRed,
  formatTextColorOrange,
  info,
  error
}
