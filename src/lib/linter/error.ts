class MSLintError extends Error {}

class MSLintConfigValidationError extends MSLintError {
  constructor (messages: string[]) {
    super(`Invalid options:\n- ${messages.join('\n- ')}`)
    Error.captureStackTrace(this, MSLintConfigValidationError)
  }
}

export {
  MSLintError,
  MSLintConfigValidationError
}
