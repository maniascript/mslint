import { Node, Scope } from '@maniascript/parser'
import { CommonTokenStream } from 'antlr4ng'

type RuleReporter = (node: Node, message: string) => void

type RuleSettings = Record<string, unknown>

interface RuleContext {
  id: string
  settings: RuleSettings
  tokens: CommonTokenStream
  report: RuleReporter
  getScope: (node?: Node) => Scope | null
}

type RuleActions = Record<string, (node: Node) => void>

interface Rule {
  meta: {
    id: string
    description: string
    recommended?: boolean
    settings?: RuleSettings
  }
  create: (context: RuleContext) => RuleActions
}

enum Severity {
  Off,
  Warn,
  Error
}

const RULE_SEVERITIES = new Map<string | number, Severity>([
  ['off', Severity.Off],
  ['warn', Severity.Warn],
  ['error', Severity.Error],
  [0, Severity.Off],
  [1, Severity.Warn],
  [2, Severity.Error]
])

function getSeverity (source: unknown): Severity | undefined {
  if (typeof source === 'string') {
    return RULE_SEVERITIES.get(source.toLowerCase())
  } else if (typeof source === 'number') {
    return RULE_SEVERITIES.get(source)
  } else {
    return undefined
  }
}

function getSettings (source: unknown): RuleSettings {
  if (typeof source === 'object' && source !== null) {
    const settings: RuleSettings = {}
    for (const property in source) {
      settings[property] = source[property as keyof typeof source]
    }
    return settings
  } else {
    return {}
  }
}

export {
  type Rule,
  type RuleContext,
  type RuleSettings,
  Severity,
  getSeverity,
  getSettings
}
