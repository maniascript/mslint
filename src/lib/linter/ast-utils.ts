import {
  Node,
  TextLiteral,
  IntegerLiteral,
  RealLiteral,
  BooleanLiteral,
  ManiaScriptLexer,
  isLiteral,
  Kind,
  EnumLiteral,
  UnaryExpression
} from '@maniascript/parser'
import { CommonTokenStream, Token } from 'antlr4ng'

function onDefaultChannel (token: Token): boolean {
  return token.channel === ManiaScriptLexer.DEFAULT_TOKEN_CHANNEL
}

function sameTokens (left: Node, right: Node, tokens: CommonTokenStream): boolean {
  const leftTokens = tokens.getTokens(left.source.token.start, left.source.token.end).filter(onDefaultChannel)
  const rightTokens = tokens.getTokens(right.source.token.start, right.source.token.end).filter(onDefaultChannel)

  if (leftTokens.length !== rightTokens.length) {
    return false
  }

  let i = 0
  for (const leftToken of leftTokens) {
    if (
      leftToken.type !== rightTokens[i].type ||
      leftToken.text !== rightTokens[i].text
    ) {
      return false
    }
    i++
  }

  return true
}

function equalEnums (left: EnumLiteral, right: EnumLiteral): boolean {
  return (
    left.class?.name === right.class?.name &&
    left.name.name === right.name.name &&
    left.value.name === right.value.name
  )
}

function equalLiterals (left: Node, right: Node): boolean {
  if (left.kind !== right.kind) return false

  if (left instanceof UnaryExpression && right instanceof UnaryExpression) {
    if (left.operator === right.operator) {
      return equalLiterals(left.argument, right.argument)
    } else {
      return false
    }
  }

  if (!isLiteral(left) || !isLiteral(right)) return false

  switch (left.kind) {
    case Kind.TextLiteral: return ((left as TextLiteral).value === (right as TextLiteral).value)
    case Kind.IntegerLiteral: return ((left as IntegerLiteral).value === (right as IntegerLiteral).value)
    case Kind.RealLiteral: return ((left as RealLiteral).value === (right as RealLiteral).value)
    case Kind.BooleanLiteral: return ((left as BooleanLiteral).value === (right as BooleanLiteral).value)
    case Kind.NullLiteral: return true
    case Kind.NullIdLiteral: return true
    case Kind.EnumLiteral: return equalEnums(left as EnumLiteral, right as EnumLiteral)
  }

  return false
}

function getNodeText (node: Node, tokens: CommonTokenStream): string {
  return tokens.getTextFromRange(tokens.get(node.source.token.start), tokens.get(node.source.token.end))
}

export {
  sameTokens,
  equalEnums,
  equalLiterals,
  getNodeText
}
