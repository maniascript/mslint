import { type Rule } from '../linter/rule.js'
import { type Expression, ExpressionStatement, Kind, DotAccessExpression } from '@maniascript/parser'

export const noUnusedExpression: Rule = {
  meta: {
    id: 'no-unused-expression',
    description: 'Forbid unused expression',
    recommended: true
  },
  create (context) {
    function isForbidden (expression: Expression): boolean {
      if (expression instanceof DotAccessExpression) {
        return (
          expression.functionCall === undefined &&
          isForbidden(expression.object)
        )
      } else {
        return expression.kind !== Kind.FunctionCallExpression
      }
    }

    return {
      'ExpressionStatement:enter': (node) => {
        if (node instanceof ExpressionStatement && isForbidden(node.expression)) {
          context.report(node, 'Expected an assignement or function call and instead found an expression')
        }
      }
    }
  }
}
