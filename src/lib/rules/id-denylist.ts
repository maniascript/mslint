import { type Rule } from '../linter/rule.js'
import {
  Identifier,
  VariableDeclaration,
  FunctionDeclaration,
  FunctionParameterDeclaration,
  LabelStatement,
  SettingDirective,
  CommandDirective,
  StructAliasing,
  StructDeclaration,
  StructMemberDeclaration,
  ConstAliasing,
  ConstDeclaration,
  ForeachStatement,
  ForStatement
} from '@maniascript/parser'

const DEFAULT_LIST: string[] = []

export const idDenylist: Rule = {
  meta: {
    id: 'id-denylist',
    description: 'Forbid the use of the specified identifiers',
    recommended: false,
    settings: {
      list: DEFAULT_LIST
    }
  },
  create (context) {
    let list = new Set<string>()
    if (Array.isArray(context.settings['list'])) {
      for (const word of context.settings['list']) {
        if (typeof word === 'string') {
          list.add(word)
        }
      }
    } else {
      list = new Set(DEFAULT_LIST)
    }

    function checkId (identifier: Identifier): void {
      if (list.has(identifier.name)) {
        context.report(identifier, `Identifier name '${identifier.name}' is forbidden`)
      }
    }

    return {
      'VariableDeclaration:exit': (node) => {
        if (node instanceof VariableDeclaration) {
          checkId(node.name)
          if (node.alias !== undefined) {
            checkId(node.alias)
          }
        }
      },
      'FunctionDeclaration:exit': (node) => {
        if (node instanceof FunctionDeclaration) {
          checkId(node.name)
        }
      },
      'FunctionParameterDeclaration:exit': (node) => {
        if (node instanceof FunctionParameterDeclaration) {
          checkId(node.name)
        }
      },
      'LabelStatement:exit': (node) => {
        if (node instanceof LabelStatement && node.name instanceof Identifier) {
          checkId(node.name)
        }
      },
      'SettingDirective:exit': (node) => {
        if (node instanceof SettingDirective) {
          checkId(node.name)
        }
      },
      'CommandDirective:exit': (node) => {
        if (node instanceof CommandDirective) {
          checkId(node.name)
        }
      },
      'StructAliasing:exit': (node) => {
        if (node instanceof StructAliasing) {
          checkId(node.alias)
        }
      },
      'StructDeclaration:exit': (node) => {
        if (node instanceof StructDeclaration) {
          checkId(node.name)
        }
      },
      'StructMemberDeclaration:exit': (node) => {
        if (node instanceof StructMemberDeclaration) {
          checkId(node.name)
        }
      },
      'ConstAliasing:exit': (node) => {
        if (node instanceof ConstAliasing) {
          checkId(node.alias)
        }
      },
      'ConstDeclaration:exit': (node) => {
        if (node instanceof ConstDeclaration) {
          checkId(node.name)
        }
      },
      'ForeachStatement:exit': (node) => {
        if (node instanceof ForeachStatement) {
          if (node.key !== undefined) {
            checkId(node.key)
          }
          checkId(node.value)
        }
      },
      'ForStatement:exit': (node) => {
        if (node instanceof ForStatement) {
          checkId(node.value)
        }
      }
    }
  }
}
