// Based on https://github.com/eslint/eslint/blob/main/lib/rules/no-useless-concat.js

import { type Rule } from '../linter/rule.js'
import { BinaryExpression, BinaryOperator, TemplateTextLiteral, TextLiteral, type Expression } from '@maniascript/parser'

export const noUselessConcat: Rule = {
  meta: {
    id: 'no-useless-concat',
    description: 'Forbid unnecessary concatenation of Text literals',
    recommended: true
  },
  create (context) {
    function isConcatenation (node: Expression): node is BinaryExpression {
      return (
        node instanceof BinaryExpression &&
        node.operator === BinaryOperator['^']
      )
    }

    function getLeft (node: BinaryExpression): Expression {
      let left = node.left

      while (isConcatenation(left)) {
        left = left.right
      }

      return left
    }

    function getRight (node: BinaryExpression): Expression {
      let right = node.right

      while (isConcatenation(right)) {
        right = right.left
      }

      return right
    }

    function isStringLiteral (node: Expression): boolean {
      return (
        node instanceof TextLiteral ||
        node instanceof TemplateTextLiteral
      )
    }

    return {
      'BinaryExpression:enter': (node) => {
        if (isConcatenation(node)) {
          const left = getLeft(node)
          const right = getRight(node)

          if (
            isStringLiteral(left) &&
            isStringLiteral(right)
          ) {
            context.report(node, 'Forbidden concatenation of Text literals')
          }
        }
      }
    }
  }
}
