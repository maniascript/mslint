import { type Rule } from '../linter/rule.js'
import { ConstDeclaration, ConstAliasing } from '@maniascript/parser'

const DEFAULT_EXCEPTIONS: string[] = []

const specialNames = [
  'CompatibleMapTypes',
  'Version',
  'ScriptName',
  'Description',
  'CustomMusicFolder'
]

function isValidName (name: string): boolean {
  if (name.length === 1) return true
  if (name.search(/^(.+_)?C_.+/) !== -1) return true
  if (specialNames.includes(name)) return true
  return false
}

export const namingConventionConstant: Rule = {
  meta: {
    id: 'naming-convention-constant',
    description: 'Enforce naming convention on constants',
    recommended: true,
    settings: {
      exceptions: DEFAULT_EXCEPTIONS
    }
  },
  create (context) {
    const exceptions = Array.isArray(context.settings['exceptions']) ? context.settings['exceptions'] : DEFAULT_EXCEPTIONS

    return {
      'ConstDeclaration:exit': (node) => {
        if (node instanceof ConstDeclaration) {
          if (!exceptions.includes(node.name.name) && !isValidName(node.name.name)) {
            context.report(node.name, 'A constant name must be prefixed with \'C_\'')
          }
        }
      },
      'ConstAliasing:exit': (node) => {
        if (node instanceof ConstAliasing) {
          if (!exceptions.includes(node.name.name) && !isValidName(node.alias.name)) {
            context.report(node.alias, 'A constant name must be prefixed with \'C_\'')
          }
        }
      }
    }
  }
}
