import { type Rule } from '../linter/rule.js'
import { FunctionDeclaration } from '@maniascript/parser'

export const noDupeParameters: Rule = {
  meta: {
    id: 'no-dupe-parameters',
    description: 'Forbid duplicate parameters in function definitions',
    recommended: true
  },
  create (context) {
    return {
      'FunctionDeclaration:exit': (node) => {
        if (node instanceof FunctionDeclaration && node.parameters.length > 0) {
          const parametersName = new Set<string>()
          for (const parameter of node.parameters) {
            if (parametersName.has(parameter.name.name)) {
              context.report(parameter.name, `Duplicated parameter '${parameter.name.name}'`)
            } else {
              parametersName.add(parameter.name.name)
            }
          }
        }
      }
    }
  }
}
