import { type Rule } from '../linter/rule.js'
import { FunctionDeclaration } from '@maniascript/parser'

const DEFAULT_MAXIMUM = 10

export const maxParams: Rule = {
  meta: {
    id: 'max-params',
    description: 'Enforce a maximum number of parameters in function declarations',
    recommended: true,
    settings: {
      maximum: DEFAULT_MAXIMUM
    }
  },
  create (context) {
    const maximumParamsNb = typeof context.settings['maximum'] === 'number' ? context.settings['maximum'] : DEFAULT_MAXIMUM

    return {
      'FunctionDeclaration:exit': (node) => {
        if (node instanceof FunctionDeclaration && node.parameters.length > maximumParamsNb) {
          context.report(node, `Function '${node.name.name}()' has too many parameters (${node.parameters.length.toString()} > ${maximumParamsNb.toString()})`)
        }
      }
    }
  }
}
