import { type Rule } from '../linter/rule.js'
import { ArrayType } from '@maniascript/parser'

export const trimArrayType: Rule = {
  meta: {
    id: 'trim-array-type',
    description: 'Array type must be on a single line without whitespaces',
    recommended: true
  },
  create (context) {
    return {
      'ArrayType:enter': (node) => {
        if (node instanceof ArrayType) {
          if (node.source.loc.start.line !== node.source.loc.end.line) {
            context.report(node, 'An array type must be on a single line')
          } else {
            let length = node.value.source.range.end - node.value.source.range.start + 1
            for (const key of node.keys) {
              length += 2
              if (key !== 'EmptyKey') length += key.source.range.end - key.source.range.start + 1
            }
            if (length !== node.source.range.end - node.source.range.start + 1) {
              context.report(node, 'An array type must not contain any whitespace characters')
            }
          }
        }
      }
    }
  }
}
