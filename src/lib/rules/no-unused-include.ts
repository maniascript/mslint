import { type Rule } from '../linter/rule.js'
import { Identifier, IncludeDirective } from '@maniascript/parser'

export const noUnusedInclude: Rule = {
  meta: {
    id: 'no-unused-include',
    description: 'An included library must be used in the file',
    recommended: true
  },
  create (context) {
    const unusedIncludes = new Map<string, IncludeDirective>()
    let isEnabled = true
    return {
      'RequireContextDirective:enter': () => {
        isEnabled = false
      },
      'ExtendsDirective:enter': () => {
        isEnabled = false
      },
      'IncludeDirective:exit': (node) => {
        if (isEnabled && node instanceof IncludeDirective && node.alias !== undefined) {
          unusedIncludes.set(node.alias.name, node)
        }
      },
      'Identifier:enter': (node) => {
        if (isEnabled && node instanceof Identifier && node.namespace !== undefined) {
          unusedIncludes.delete(node.namespace)
        }
      },
      'Program:exit': () => {
        if (isEnabled) {
          for (const unusedInclude of unusedIncludes) {
            if (unusedInclude[1].alias !== undefined) {
              context.report(unusedInclude[1], `Include '${unusedInclude[1].alias.name}' is never used`)
            } else {
              context.report(unusedInclude[1], `Include '${unusedInclude[1].path.value}' is never used`)
            }
          }
        }
      }
    }
  }
}
