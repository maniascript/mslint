import { type Rule } from '../linter/rule.js'

export const noLog: Rule = {
  meta: {
    id: 'no-log',
    description: 'Forbid the use of the log() function',
    recommended: false
  },
  create (context) {
    return {
      'LogStatement:exit': (node) => {
        context.report(node, 'The use of the \'log()\' function is forbidden')
      }
    }
  }
}
