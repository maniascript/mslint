import { type Rule } from '../linter/rule.js'
import { StructDeclaration, StructAliasing } from '@maniascript/parser'

function isValidName (name: string): boolean {
  return name.search(/^(.+_)?K_.+/) !== -1
}

export const namingConventionStructure: Rule = {
  meta: {
    id: 'naming-convention-structure',
    description: 'Enforce naming convention on structures',
    recommended: true
  },
  create (context) {
    return {
      'StructDeclaration:exit': (node) => {
        if (node instanceof StructDeclaration) {
          if (!isValidName(node.name.name)) {
            context.report(node.name, 'A structure name must be prefixed with \'K_\'')
          }
        }
      },
      'StructAliasing:exit': (node) => {
        if (node instanceof StructAliasing) {
          if (!isValidName(node.alias.name)) {
            context.report(node.alias, 'A structure name must be prefixed with \'K_\'')
          }
        }
      }
    }
  }
}
