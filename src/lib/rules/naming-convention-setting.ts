import { type Rule } from '../linter/rule.js'
import { SettingDirective } from '@maniascript/parser'

function isValidName (name: string): boolean {
  return name.search(/^S_.+/) !== -1
}

export const namingConventionSetting: Rule = {
  meta: {
    id: 'naming-convention-setting',
    description: 'Enforce naming convention on settings',
    recommended: true
  },
  create (context) {
    return {
      'SettingDirective:exit': (node) => {
        if (node instanceof SettingDirective) {
          if (!isValidName(node.name.name)) {
            context.report(node.name, 'A setting name must be prefixed with \'S_\'')
          }
        }
      }
    }
  }
}
