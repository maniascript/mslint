import { type Rule } from '../linter/rule.js'
import { CommandDirective } from '@maniascript/parser'

function isValidName (name: string): boolean {
  return name.search(/^Command_.+/) !== -1
}

export const namingConventionCommand: Rule = {
  meta: {
    id: 'naming-convention-command',
    description: 'Enforce naming convention on commands',
    recommended: true
  },
  create (context) {
    return {
      'CommandDirective:exit': (node) => {
        if (node instanceof CommandDirective) {
          if (!isValidName(node.name.name)) {
            context.report(node.name, 'A command name must be prefixed with \'Command_\'')
          }
        }
      }
    }
  }
}
