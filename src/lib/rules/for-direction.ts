import { type Rule } from '../linter/rule.js'
import { ForStatement, IntegerLiteral } from '@maniascript/parser'

export const forDirection: Rule = {
  meta: {
    id: 'for-direction',
    description: 'Check that for loop start value is smaller than stop value',
    recommended: true
  },
  create (context) {
    return {
      'ForStatement:exit': (node) => {
        if (node instanceof ForStatement) {
          if (node.loopStart instanceof IntegerLiteral && node.loopStop instanceof IntegerLiteral) {
            if (node.loopStart.value >= node.loopStop.value) {
              context.report(node, 'This for loop won\'t be executed because the start value is bigger or equal to the stop value')
            }
          }
        }
      }
    }
  }
}
