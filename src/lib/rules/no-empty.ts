import { type Rule } from '../linter/rule.js'
import { BlockStatement } from '@maniascript/parser'

export const noEmpty: Rule = {
  meta: {
    id: 'no-empty',
    description: 'Forbid empty block statement',
    recommended: true
  },
  create (context) {
    return {
      'BlockStatement:exit': (node) => {
        if (node instanceof BlockStatement && node.body.length === 0) {
          context.report(node, 'Empty block statement')
        }
      }
    }
  }
}
