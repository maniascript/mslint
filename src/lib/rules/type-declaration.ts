import { type Rule } from '../linter/rule.js'
import { VariableDeclaration } from '@maniascript/parser'

export enum Type {
  Explicite = 'Explicite',
  Implicite = 'Implicite'
}

export const typeDeclaration: Rule = {
  meta: {
    id: 'type-declaration',
    description: 'Require or forbid type in variable declarations',
    recommended: true,
    settings: {
      type: 'Explicite'
    }
  },
  create (context) {
    const target = context.settings['type'] !== undefined ? context.settings['type'] as Type : Type.Explicite

    return {
      'VariableDeclaration:exit': (node) => {
        if (node instanceof VariableDeclaration && !node.isLet) {
          if (target === Type.Explicite) {
            if (node.type === undefined) {
              context.report(node, `Variable '${node.name.name}' must have an explicite type on declaration`)
            }
          } else {
            if (node.type !== undefined) {
              if (
                node.initializerExpression === undefined &&
                node.initializerType === undefined
              ) {
                context.report(node, `Variable '${node.name.name}' should be initialized to have an implicit type on declaration`)
              } else {
                context.report(node, `Variable '${node.name.name}' should have an implicit type on declaration`)
              }
            }
          }
        }
      }
    }
  }
}
