import { type Rule } from '../linter/rule.js'
import { ArrayExpression, type Expression } from '@maniascript/parser'
import { equalLiterals, getNodeText } from '../linter/ast-utils.js'

function keyAlreadyExists (key: Expression, previousKeys: Set<Expression>): boolean {
  for (const previousKey of previousKeys) {
    if (equalLiterals(key, previousKey)) {
      return true
    }
  }
  return false
}

export const noDupeKeys: Rule = {
  meta: {
    id: 'no-dupe-keys',
    description: 'Forbid duplicate keys in associative arrays',
    recommended: true
  },
  create (context) {
    return {
      'ArrayExpression:exit': (node) => {
        if (node instanceof ArrayExpression && node.isAssociative && node.values.length > 1) {
          const keys = new Set<Expression>()
          for (const { key } of node.values) {
            if (key !== undefined) {
              if (keyAlreadyExists(key, keys)) {
                context.report(key, `Duplicate key '${getNodeText(key, context.tokens)}'`)
              } else {
                keys.add(key)
              }
            }
          }
        }
      }
    }
  }
}
