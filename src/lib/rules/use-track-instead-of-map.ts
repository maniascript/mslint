import { type Rule } from '../linter/rule.js'
import { TextLiteral, TemplateTextLiteral } from '@maniascript/parser'

function containsMapWord (text: string): boolean {
  // find 'map' but ignore 'map type'
  return text.search(/\bmaps?(?!\s?type\b)\b/i) !== -1
}

export const useTrackInsteadOfMap: Rule = {
  meta: {
    id: 'use-track-instead-of-map',
    description: 'Use the word track instead of map',
    recommended: false,
    settings: {
      onlyTranslatedText: true
    }
  },
  create (context) {
    const onlyTranslatedText: boolean = typeof context.settings['onlyTranslatedText'] === 'boolean' ? context.settings['onlyTranslatedText'] : true

    return {
      'TextLiteral:exit': (node) => {
        if (node instanceof TextLiteral && (node.isTranslated || !onlyTranslatedText) && containsMapWord(node.value)) {
          context.report(node, 'Use the word track instead of map')
        }
      },
      'TemplateTextLiteral:exit': (node) => {
        if (node instanceof TemplateTextLiteral && !onlyTranslatedText) {
          for (const text of node.texts) {
            if (containsMapWord(text.value)) {
              context.report(node, 'Use the word track instead of map')
            }
          }
        }
      }
    }
  }
}
