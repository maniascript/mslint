import { type Rule } from '../linter/rule.js'
import { BlockStatement, Kind, Node } from '@maniascript/parser'

const DEFAULT_MAXIMUM = 8

export const maxDepth: Rule = {
  meta: {
    id: 'max-depth',
    description: 'Prevent nested code blocks to exceed a maximum depth',
    recommended: true,
    settings: {
      maximum: DEFAULT_MAXIMUM
    }
  },
  create (context) {
    const maximum = typeof context.settings['maximum'] === 'number' ? context.settings['maximum'] : DEFAULT_MAXIMUM
    const stack: number[] = []

    function canPushBlock (blockStatement: BlockStatement): boolean {
      return (
        blockStatement.parent !== undefined && (
          blockStatement.parent.kind !== Kind.Main &&
          blockStatement.parent.kind !== Kind.FunctionDeclaration &&
          blockStatement.parent.kind !== Kind.ForeachStatement &&
          blockStatement.parent.kind !== Kind.ForStatement &&
          blockStatement.parent.kind !== Kind.WhileStatement &&
          blockStatement.parent.kind !== Kind.MeanwhileStatement &&
          blockStatement.parent.kind !== Kind.SwitchCase &&
          blockStatement.parent.kind !== Kind.SwitchtypeCase &&
          blockStatement.parent.kind !== Kind.ConditionalBranch
        )
      )
    }

    function enterFunction (): void {
      stack.push(0)
    }

    function exitFunction (): void {
      stack.pop()
    }

    function pushBlock (node: Node): void {
      const depth = ++stack[stack.length - 1]
      if (depth > maximum) {
        context.report(node, `Blocks are nested too deeply. Depth ${depth.toString()} while maximum allowed is ${maximum.toString()}.`)
      }
    }

    function popBlock (): void {
      stack[stack.length - 1] -= 1
    }

    return {
      'Main:enter': () => {
        enterFunction()
      },
      'Main:exit': () => {
        exitFunction()
      },
      'FunctionDeclaration:enter': () => {
        enterFunction()
      },
      'FunctionDeclaration:exit': () => {
        exitFunction()
      },
      'LabelDeclaration:enter': () => {
        enterFunction()
      },
      'LabelDeclaration:exit': () => {
        exitFunction()
      },
      'ForeachStatement:enter': (node) => {
        pushBlock(node)
      },
      'ForeachStatement:exit': () => {
        popBlock()
      },
      'ForStatement:enter': (node) => {
        pushBlock(node)
      },
      'ForStatement:exit': () => {
        popBlock()
      },
      'WhileStatement:enter': (node) => {
        pushBlock(node)
      },
      'WhileStatement:exit': () => {
        popBlock()
      },
      'MeanwhileStatement:enter': (node) => {
        pushBlock(node)
      },
      'MeanwhileStatement:exit': () => {
        popBlock()
      },
      'SwitchStatement:enter': (node) => {
        pushBlock(node)
      },
      'SwitchStatement:exit': () => {
        popBlock()
      },
      'SwitchtypeStatement:enter': (node) => {
        pushBlock(node)
      },
      'SwitchtypeStatement:exit': () => {
        popBlock()
      },
      'ConditionalStatement:enter': (node) => {
        pushBlock(node)
      },
      'ConditionalStatement:exit': () => {
        popBlock()
      },
      'BlockStatement:enter': (node) => {
        if (node instanceof BlockStatement && canPushBlock(node)) {
          pushBlock(node)
        }
      },
      'BlockStatement:exit': (node) => {
        if (node instanceof BlockStatement && canPushBlock(node)) {
          popBlock()
        }
      }
    }
  }
}
