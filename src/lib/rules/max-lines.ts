import { type Rule } from '../linter/rule.js'
import { FunctionDeclaration, LabelDeclaration, Main } from '@maniascript/parser'

const DEFAULT_MAXIMUM = 1000

export const maxLines: Rule = {
  meta: {
    id: 'max-lines',
    description: 'Enforce a maximum number of lines in three contexts: function, label and main',
    recommended: true,
    settings: {
      maximum: {
        function: DEFAULT_MAXIMUM,
        label: DEFAULT_MAXIMUM,
        main: DEFAULT_MAXIMUM
      }
    }
  },
  create (context) {
    let functionMaximumLinesNb = DEFAULT_MAXIMUM
    let labelMaximumLinesNb = DEFAULT_MAXIMUM
    let mainMaximumLinesNb = DEFAULT_MAXIMUM

    if (typeof context.settings['maximum'] === 'number') {
      functionMaximumLinesNb = context.settings['maximum']
      labelMaximumLinesNb = context.settings['maximum']
      mainMaximumLinesNb = context.settings['maximum']
    } else if (typeof context.settings['maximum'] === 'object' && context.settings['maximum'] !== null) {
      const maximum = context.settings['maximum']
      if ('function' in maximum && typeof maximum.function === 'number') functionMaximumLinesNb = maximum.function
      if ('label' in maximum && typeof maximum.label === 'number') labelMaximumLinesNb = maximum.label
      if ('main' in maximum && typeof maximum.main === 'number') mainMaximumLinesNb = maximum.main
    }

    function reportTooManyLines (node: FunctionDeclaration | LabelDeclaration | Main, maximumLinesNb: number): void {
      const linesNb = node.source.loc.end.line - node.source.loc.start.line + 1
      if (linesNb > maximumLinesNb) {
        if (node instanceof FunctionDeclaration) {
          context.report(node, `Function '${node.name.name}()' has too many lines (${linesNb.toString()} > ${maximumLinesNb.toString()})`)
        } else if (node instanceof LabelDeclaration) {
          context.report(node, `Label '***${node.name.name}***' has too many lines (${linesNb.toString()} > ${maximumLinesNb.toString()})`)
        } else if (node instanceof Main) {
          context.report(node, `Function 'main()' has too many lines (${linesNb.toString()} > ${maximumLinesNb.toString()})`)
        }
      }
    }

    return {
      'FunctionDeclaration:exit': (node) => {
        if (node instanceof FunctionDeclaration) {
          reportTooManyLines(node, functionMaximumLinesNb)
        }
      },
      'LabelDeclaration:exit': (node) => {
        if (node instanceof LabelDeclaration) {
          reportTooManyLines(node, labelMaximumLinesNb)
        }
      },
      'Main:exit': (node) => {
        if (node instanceof Main) {
          reportTooManyLines(node, mainMaximumLinesNb)
        }
      }
    }
  }
}
