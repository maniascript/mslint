import { type Rule } from '../linter/rule.js'
import { DotAccessExpression } from '@maniascript/parser'

export const noNowClassMember : Rule = {
  meta: {
    id: 'no-now-class-member',
    description: 'Now class member are deprecated in favor of the Now language keyword',
    recommended: true
  },
  create (context) {
    return {
      'DotAccessExpression:enter': (node) => {
        if (node instanceof DotAccessExpression && node.member !== undefined && node.member.name === 'Now') {
          context.report(node, 'Use the `Now` keyword instead of the `x.Now` class member')
        }
      }
    }
  }
}
