import { type Rule } from '../linter/rule.js'
import {
  VariableDeclaration,
  AssignmentStatement,
  ReturnStatement,
  FunctionCallExpression,
  isInitializerType
} from '@maniascript/parser'

export const deprecatedInitializerType: Rule = {
  meta: {
    id: 'deprecated-initializer-type',
    description: 'Using a type as a value is deprecated',
    recommended: true
  },
  create (context) {
    return {
      'VariableDeclaration:enter': (node) => {
        if (node instanceof VariableDeclaration) {
          if (node.initializerType !== undefined) {
            context.report(node.initializerType, 'Using a type as value is deprecated')
          }
        }
      },
      'AssignmentStatement:enter': (node) => {
        if (node instanceof AssignmentStatement) {
          if (node.isInitializedByType) {
            context.report(node.right, 'Using a type as value is deprecated')
          }
        }
      },
      'ReturnStatement:enter': (node) => {
        if (node instanceof ReturnStatement) {
          if (node.argument !== undefined && isInitializerType(node.argument)) {
            context.report(node.argument, 'Using a type as value is deprecated')
          }
        }
      },
      'FunctionCallExpression:enter': (node) => {
        if (node instanceof FunctionCallExpression) {
          for (const argument of node.arguments) {
            if (isInitializerType(argument)) {
              context.report(argument, 'Using a type as value is deprecated')
            }
          }
        }
      }
    }
  }
}
