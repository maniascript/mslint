import { type Rule } from '../linter/rule.js'
import { Main } from '@maniascript/parser'

const DEFAULT_MAXIMUM = Infinity

export const _template: Rule = {
  meta: {
    id: '_template_id',
    description: '_template_description',
    recommended: true,
    settings: {
      maximum: DEFAULT_MAXIMUM
    }
  },
  create (context) {
    const maximum = typeof context.settings['maximum'] === 'number' ? context.settings['maximum'] : DEFAULT_MAXIMUM

    return {
      'Main:enter': (node) => {
        if (node instanceof Main && maximum === 15) {
          context.report(node, '_template_error')
        }
      }
    }
  }
}
