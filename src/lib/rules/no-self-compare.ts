import { type Rule } from '../linter/rule.js'
import { BinaryExpression, BinaryOperator } from '@maniascript/parser'
import { sameTokens } from '../linter/ast-utils.js'

export const noSelfCompare: Rule = {
  meta: {
    id: 'no-self-compare',
    description: 'Forbid comparisons where both sides are the same',
    recommended: true
  },
  create (context) {
    return {
      'BinaryExpression:enter': (node) => {
        if (
          node instanceof BinaryExpression && (
            node.operator === BinaryOperator['!='] ||
            node.operator === BinaryOperator['<'] ||
            node.operator === BinaryOperator['<='] ||
            node.operator === BinaryOperator['=='] ||
            node.operator === BinaryOperator['>'] ||
            node.operator === BinaryOperator['>=']
          ) &&
          sameTokens(node.left, node.right, context.tokens)
        ) {
          context.report(node, 'Comparing to itself is probably an error')
        }
      }
    }
  }
}
