import { type Rule } from '../linter/rule.js'
import { AssignmentOperator, AssignmentStatement, Identifier } from '@maniascript/parser'
import { getNodeText } from '../linter/ast-utils.js'

export const noSelfAssign: Rule = {
  meta: {
    id: 'no-self-assign',
    description: 'Forbid assignments where both sides are the same',
    recommended: true
  },
  create (context) {
    return {
      'AssignmentStatement:enter': (node) => {
        if (
          node instanceof AssignmentStatement && (
            node.operator === AssignmentOperator['='] ||
            node.operator === AssignmentOperator['<=>']
          ) &&
          node.left instanceof Identifier &&
          node.right instanceof Identifier &&
          node.left.namespace === node.right.namespace &&
          node.left.name === node.right.name
        ) {
          context.report(node, `'${getNodeText(node.left, context.tokens)}' is assigned to itself`)
        }
      }
    }
  }
}
