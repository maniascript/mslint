import { type Rule } from '../linter/rule.js'
import { RealLiteral } from '@maniascript/parser'

export const formatReal: Rule = {
  meta: {
    id: 'format-real',
    description: 'Enforce consistent format for Real values',
    recommended: true,
    settings: {
      hasIntegerPart: true,
      hasDecimalPart: false
    }
  },
  create (context) {
    const hasIntegerPart = typeof context.settings['hasIntegerPart'] === 'boolean' ? context.settings['hasIntegerPart'] : true
    const hasDecimalPart = typeof context.settings['hasDecimalPart'] === 'boolean' ? context.settings['hasDecimalPart'] : false

    return {
      'RealLiteral:enter': (node) => {
        if (node instanceof RealLiteral) {
          if (hasIntegerPart && node.raw.startsWith('.')) {
            context.report(node, `A Real value must have an integer part, write '0${node.raw}' instead of '${node.raw}'`)
          }
          if (hasDecimalPart && node.raw.endsWith('.')) {
            context.report(node, `A Real value must have a decimal part, write '${node.raw}0' instead of '${node.raw}'`)
          }
        }
      }
    }
  }
}
