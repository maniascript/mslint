import { type Rule } from '../linter/rule.js'
import { IncludeDirective } from '@maniascript/parser'

export const includeOneTime: Rule = {
  meta: {
    id: 'include-one-time',
    description: 'Check that a file is not included several times',
    recommended: true
  },
  create (context) {
    const alreadyIncluded = new Set<string>()
    return {
      'IncludeDirective:exit': (node) => {
        if (node instanceof IncludeDirective) {
          if (alreadyIncluded.has(node.path.value)) {
            context.report(node.path, 'This file is already included')
          } else {
            alreadyIncluded.add(node.path.value)
          }
        }
      }
    }
  }
}
