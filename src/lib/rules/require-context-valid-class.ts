import { type Rule } from '../linter/rule.js'
import { RequireContextDirective } from '@maniascript/parser'

const DEFAULT_VALID_CLASSES: string[] = []
const DEFAULT_INVALID_CLASSES: string[] = []

const validClasses = new Set([
  'CManiaAppTitle',
  'CManiaAppStation',
  'CManiaAppPlayground',
  'CSmMode',
  'CTmMode',
  'CSmMapType',
  'CTmMapType',
  'CMapEditorPlugin',
  'CServerPlugin',
  'CSmAction',
  'CManiaplanetPlugin',
  'CAnyEditorPlugin'
])

export const requireContextValidClass: Rule = {
  meta: {
    id: 'require-context-valid-class',
    description: 'The class used in the #RequireContext directive must be a valid script execution context',
    recommended: true,
    settings: {
      validClasses: DEFAULT_VALID_CLASSES,
      invalidClasses: DEFAULT_INVALID_CLASSES
    }
  },
  create (context) {
    const customValidClasses = Array.isArray(context.settings['validClasses']) ? context.settings['validClasses'] : DEFAULT_VALID_CLASSES
    const customInvalidClasses = Array.isArray(context.settings['invalidClasses']) ? context.settings['invalidClasses'] : DEFAULT_INVALID_CLASSES

    for (const customValidClass of customValidClasses) {
      if (typeof customValidClass === 'string') validClasses.add(customValidClass)
    }
    for (const customInvalidClass of customInvalidClasses) {
      if (typeof customInvalidClass === 'string') validClasses.delete(customInvalidClass)
    }

    return {
      'RequireContextDirective:enter': (node) => {
        if (node instanceof RequireContextDirective) {
          if (!validClasses.has(node.class.name)) {
            context.report(node.class, `'${node.class.name}' is not a valid script execution context`)
          }
        }
      }
    }
  }
}
