import { type Rule } from '../linter/rule.js'
import { Program } from '@maniascript/parser'

const DEFAULT_MAXIMUM = 5000

export const maxLinesPerFile: Rule = {
  meta: {
    id: 'max-lines-per-file',
    description: 'Enforce a maximum number of lines per file',
    recommended: true,
    settings: {
      maximum: DEFAULT_MAXIMUM
    }
  },
  create (context) {
    const maximumLinesNb = typeof context.settings['maximum'] === 'number' ? context.settings['maximum'] : DEFAULT_MAXIMUM

    return {
      'Program:exit': (node) => {
        const linesNb = node.source.loc.end.line - node.source.loc.start.line + 1
        if (node instanceof Program && linesNb > maximumLinesNb) {
          context.report(node, `File has too many lines (${linesNb.toString()} > ${maximumLinesNb.toString()})`)
        }
      }
    }
  }
}
