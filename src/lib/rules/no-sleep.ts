import { type Rule } from '../linter/rule.js'

export const noSleep: Rule = {
  meta: {
    id: 'no-sleep',
    description: 'Forbid the use of the sleep() function',
    recommended: false
  },
  create (context) {
    return {
      'SleepStatement:exit': (node) => {
        context.report(node, 'The use of the \'sleep()\' function is forbidden')
      }
    }
  }
}
