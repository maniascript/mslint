import { type Rule } from '../linter/rule.js'
import { _template } from './_template.js'
import { deprecatedInitializerType } from './deprecated-initializer-type.js'
import { forDirection } from './for-direction.js'
import { formatIfElse } from './format-if-else.js'
import { formatReal } from './format-real.js'
import { idDenylist } from './id-denylist.js'
import { idLength } from './id-length.js'
import { includeOneTime } from './include-one-time.js'
import { includeUseCommonNamespace } from './include-use-common-namespace.js'
import { includeUseNamespace } from './include-use-namespace.js'
import { initDeclaration } from './init-declaration.js'
import { maxDepth } from './max-depth.js'
import { maxLines } from './max-lines.js'
import { maxLinesPerFile } from './max-lines-per-file.js'
import { maxParams } from './max-params.js'
import { maxStatements } from './max-statements.js'
import { namingConventionCommand } from './naming-convention-command.js'
import { namingConventionConstant } from './naming-convention-constant.js'
import { namingConventionFunction } from './naming-convention-function.js'
import { namingConventionVariable } from './naming-convention-variable.js'
import { namingConventionSetting } from './naming-convention-setting.js'
import { namingConventionStructure } from './naming-convention-structure.js'
import { noContinue } from './no-continue.js'
import { noDupeCase } from './no-dupe-case.js'
import { noDupeElseIf } from './no-dupe-else-if.js'
import { noDupeKeys } from './no-dupe-keys.js'
import { noDupeParameters } from './no-dupe-parameters.js'
import { noEmpty } from './no-empty.js'
import { noIntegerOverflow } from './no-integer-overflow.js'
import { noLog } from './no-log.js'
import { noLonelyIf } from './no-lonely-if.js'
import { noNegatedCondition } from './no-negated-condition.js'
import { noNowClassMember } from './no-now-class-member.js'
import { noSleep } from './no-sleep.js'
import { noUnusedExpression } from './no-unused-expression.js'
import { noUselessConcat } from './no-useless-concat.js'
import { noWait } from './no-wait.js'
import { noSelfAssign } from './no-self-assign.js'
import { noSelfCompare } from './no-self-compare.js'
import { noUnusedVariables } from './no-unused-variables.js'
import { noUnusedInclude } from './no-unused-include.js'
import { requireContextValidClass } from './require-context-valid-class.js'
import { trimArrayType } from './trim-array-type.js'
import { typeDeclaration } from './type-declaration.js'
import { useTrackInsteadOfMap } from './use-track-instead-of-map.js'

const allRules = new Map<string, Rule>([
  [_template.meta.id, _template],
  [deprecatedInitializerType.meta.id, deprecatedInitializerType],
  [forDirection.meta.id, forDirection],
  [formatIfElse.meta.id, formatIfElse],
  [formatReal.meta.id, formatReal],
  [idDenylist.meta.id, idDenylist],
  [idLength.meta.id, idLength],
  [includeOneTime.meta.id, includeOneTime],
  [includeUseCommonNamespace.meta.id, includeUseCommonNamespace],
  [includeUseNamespace.meta.id, includeUseNamespace],
  [initDeclaration.meta.id, initDeclaration],
  [maxDepth.meta.id, maxDepth],
  [maxLines.meta.id, maxLines],
  [maxLinesPerFile.meta.id, maxLinesPerFile],
  [maxParams.meta.id, maxParams],
  [maxStatements.meta.id, maxStatements],
  [namingConventionCommand.meta.id, namingConventionCommand],
  [namingConventionConstant.meta.id, namingConventionConstant],
  [namingConventionFunction.meta.id, namingConventionFunction],
  [namingConventionSetting.meta.id, namingConventionSetting],
  [namingConventionStructure.meta.id, namingConventionStructure],
  [namingConventionVariable.meta.id, namingConventionVariable],
  [noContinue.meta.id, noContinue],
  [noDupeCase.meta.id, noDupeCase],
  [noDupeElseIf.meta.id, noDupeElseIf],
  [noDupeKeys.meta.id, noDupeKeys],
  [noDupeParameters.meta.id, noDupeParameters],
  [noEmpty.meta.id, noEmpty],
  [noIntegerOverflow.meta.id, noIntegerOverflow],
  [noLog.meta.id, noLog],
  [noLonelyIf.meta.id, noLonelyIf],
  [noNowClassMember.meta.id, noNowClassMember],
  [noNegatedCondition.meta.id, noNegatedCondition],
  [noSelfAssign.meta.id, noSelfAssign],
  [noSelfCompare.meta.id, noSelfCompare],
  [noSleep.meta.id, noSleep],
  [noUnusedExpression.meta.id, noUnusedExpression],
  [noUnusedInclude.meta.id, noUnusedInclude],
  [noUnusedVariables.meta.id, noUnusedVariables],
  [noUselessConcat.meta.id, noUselessConcat],
  [noWait.meta.id, noWait],
  [requireContextValidClass.meta.id, requireContextValidClass],
  [trimArrayType.meta.id, trimArrayType],
  [typeDeclaration.meta.id, typeDeclaration],
  [useTrackInsteadOfMap.meta.id, useTrackInsteadOfMap]
])

export {
  allRules,
  deprecatedInitializerType,
  forDirection,
  formatIfElse,
  formatReal,
  idDenylist,
  idLength,
  includeOneTime,
  includeUseCommonNamespace,
  includeUseNamespace,
  initDeclaration,
  maxDepth,
  maxLines,
  maxLinesPerFile,
  maxParams,
  maxStatements,
  namingConventionCommand,
  namingConventionConstant,
  namingConventionFunction,
  namingConventionVariable,
  namingConventionSetting,
  namingConventionStructure,
  noContinue,
  noDupeCase,
  noDupeElseIf,
  noDupeKeys,
  noDupeParameters,
  noEmpty,
  noIntegerOverflow,
  noLog,
  noLonelyIf,
  noNegatedCondition,
  noNowClassMember,
  noSleep,
  noUnusedExpression,
  noUselessConcat,
  noWait,
  noSelfAssign,
  noSelfCompare,
  noUnusedVariables,
  noUnusedInclude,
  requireContextValidClass,
  trimArrayType,
  typeDeclaration,
  useTrackInsteadOfMap
}
