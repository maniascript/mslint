import { type Rule } from '../linter/rule.js'
import { FunctionDeclaration } from '@maniascript/parser'

function isValidFunctionName (name: string): boolean {
  return /[A-Z]/.test(name.charAt(0)) // Start with an uppercase letter
}

function isValidParameterName (name: string): boolean {
  return name.startsWith('_') && /[A-Z]/.test(name.charAt(1))
}

export const namingConventionFunction: Rule = {
  meta: {
    id: 'naming-convention-function',
    description: 'Enforce naming convention on functions',
    recommended: true
  },
  create (context) {
    return {
      'FunctionDeclaration:enter': (node) => {
        if (node instanceof FunctionDeclaration) {
          if (!isValidFunctionName(node.name.name)) {
            context.report(node.name, 'A function name must start with an uppercase letter')
          }
          for (const parameter of node.parameters) {
            if (!isValidParameterName(parameter.name.name)) {
              context.report(parameter.name, 'A parameter name must start with an underscore')
            }
          }
        }
      }
    }
  }
}
