import { type Rule } from '../linter/rule.js'
import { VariableDeclaration } from '@maniascript/parser'

export enum Target {
  Always = 'Always',
  AlwaysTrait = 'AlwaysTrait',
  Never = 'Never'
}

export const initDeclaration: Rule = {
  meta: {
    id: 'init-declaration',
    description: 'Require or forbid initialization in variable declarations',
    recommended: true,
    settings: {
      target: 'AlwaysTrait'
    }
  },
  create (context) {
    const target = context.settings['target'] !== undefined ? context.settings['target'] as Target : Target.AlwaysTrait

    return {
      'VariableDeclaration:exit': (node) => {
        if (node instanceof VariableDeclaration && !node.isGlobal) {
          if (target === Target.Always) {
            if (node.initializerExpression === undefined) {
              context.report(node, `Variable '${node.name.name}' must be initialized on declaration`)
            }
          } else if (target === Target.AlwaysTrait) {
            if (node.forTarget !== undefined && node.initializerExpression === undefined) {
              context.report(node, `Variable '${node.name.name}' must be initialized on declaration`)
            }
          } else {
            if (node.initializerExpression !== undefined) {
              context.report(node, `Variable '${node.name.name}' should not be initialized on declaration`)
            }
          }
        }
      }
    }
  }
}
