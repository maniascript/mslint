import { type Rule } from '../linter/rule.js'
import { BlockStatement, ConditionalStatement, ManiaScriptLexer } from '@maniascript/parser'

export const formatIfElse: Rule = {
  meta: {
    id: 'format-if-else',
    description: 'Check that conditional statements are properly formated',
    recommended: true
  },
  create (context) {
    return {
      'ConditionalStatement:enter': (node) => {
        if (node instanceof ConditionalStatement) {
          for (const branch of node.branches) {
            if (!(branch.consequent instanceof BlockStatement)) {
              if (branch.test === undefined) {
                if (branch.consequent.source.loc.start.line !== branch.source.loc.start.line) {
                  context.report(branch, 'Use braces or put everything on one line')
                }
              } else {
                let closingParenthesisReached = false
                for (
                  let tokenIndex = branch.test.source.token.end + 1;
                  tokenIndex < branch.consequent.source.token.start;
                  tokenIndex++
                ) {
                  if (
                    !closingParenthesisReached &&
                    context.tokens.get(tokenIndex).type === ManiaScriptLexer.OPERATOR_CLOSE_PAREN
                  ) {
                    closingParenthesisReached = true
                  } else if (
                    closingParenthesisReached &&
                    context.tokens.get(tokenIndex).type === ManiaScriptLexer.LINE_TERMINATOR
                  ) {
                    context.report(branch, 'Use braces or put everything on one line')
                    break
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
