import { type Rule } from '../linter/rule.js'
import { IncludeDirective } from '@maniascript/parser'

const commonNamespaces = new Map([
  ['MathLib', 'ML'],
  ['TextLib', 'TL'],
  ['MapUnits', 'MU'],
  ['AnimLib', 'AL'],
  ['TimeLib', 'TiL'],
  ['ColorLib', 'CL']
])

export const includeUseCommonNamespace: Rule = {
  meta: {
    id: 'include-use-common-namespace',
    description: 'Always use the same common namespaces for the standard libraries',
    recommended: true
  },
  create (context) {
    return {
      'IncludeDirective:enter': (node) => {
        if (node instanceof IncludeDirective) {
          if (
            node.alias !== undefined &&
            commonNamespaces.has(node.path.value) &&
            node.alias.name !== commonNamespaces.get(node.path.value)
          ) {
            context.report(node.alias, `Use the common ${node.path.value} namespace. Replace '${node.alias.name}' by '${commonNamespaces.get(node.path.value) ?? ''}'.`)
          }
        }
      }
    }
  }
}
