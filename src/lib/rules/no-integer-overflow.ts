import { type Rule } from '../linter/rule.js'
import { IntegerLiteral, UnaryExpression, UnaryOperator } from '@maniascript/parser'

export const noIntegerOverflow: Rule = {
  meta: {
    id: 'no-integer-overflow',
    description: 'Forbid integer literal overflow',
    recommended: true
  },
  create (context) {
    let isNegative = false
    let unaryNode: UnaryExpression | null = null

    return {
      'UnaryExpression:enter': (node) => {
        if (
          node instanceof UnaryExpression &&
          node.operator === UnaryOperator['-'] &&
          node.argument instanceof IntegerLiteral
        ) {
          isNegative = true
          unaryNode = node
        }
      },
      'UnaryExpression:exit': () => {
        isNegative = false
        unaryNode = null
      },
      'IntegerLiteral:exit': (node) => {
        if (node instanceof IntegerLiteral) {
          if (isNegative && node.value > 2147483648) {
            if (unaryNode != null) {
              context.report(unaryNode, 'This integer will overflow')
            } else {
              context.report(node, 'This integer will overflow')
            }
          } else if (!isNegative && node.value > 2147483647) {
            context.report(node, 'This integer will overflow')
          }
        }
      }
    }
  }
}
