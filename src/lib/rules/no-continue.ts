import { type Rule } from '../linter/rule.js'

export const noContinue: Rule = {
  meta: {
    id: 'no-continue',
    description: 'Forbid the use of the continue statement',
    recommended: false
  },
  create (context) {
    return {
      'ContinueStatement:exit': (node) => {
        context.report(node, 'The use of the \'continue\' statement is forbidden')
      }
    }
  }
}
