import { type Rule } from '../linter/rule.js'
import { IncludeDirective } from '@maniascript/parser'

export const includeUseNamespace: Rule = {
  meta: {
    id: 'include-use-namespace',
    description: 'Always use a unique namespace when including a library',
    recommended: true
  },
  create (context) {
    const usedNamespaces = new Set<string>()
    return {
      'IncludeDirective:enter': (node) => {
        if (node instanceof IncludeDirective) {
          if (node.alias === undefined) {
            context.report(node, 'You must use a namespace when including a library')
          } else if (usedNamespaces.has(node.alias.name)) {
            context.report(node.alias, 'This namespace is already used by another include')
          } else {
            usedNamespaces.add(node.alias.name)
          }
        }
      }
    }
  }
}
