import { type Rule } from '../linter/rule.js'
import { BlockStatement, ConditionalStatement } from '@maniascript/parser'

export const noLonelyIf: Rule = {
  meta: {
    id: 'no-lonely-if',
    description: 'Forbid `if` statement as the only statement in an `else` block',
    recommended: true
  },
  create (context) {
    return {
      'ConditionalStatement:exit': (node) => {
        if (node instanceof ConditionalStatement) {
          const lastBranch = node.branches[node.branches.length - 1]
          if (
            lastBranch.test === undefined &&
            lastBranch.consequent instanceof BlockStatement &&
            lastBranch.consequent.body.length === 1 &&
            lastBranch.consequent.body[0] instanceof ConditionalStatement
          ) {
            context.report(lastBranch, 'Forbidden `if` statement as the only statement in an `else` block')
          }
        }
      }
    }
  }
}
