import { type Rule } from '../linter/rule.js'
import { SwitchStatement, type Expression } from '@maniascript/parser'
import { getNodeText, sameTokens } from '../linter/ast-utils.js'

export const noDupeCase: Rule = {
  meta: {
    id: 'no-dupe-case',
    description: 'Forbid duplicate case in switch statements',
    recommended: true
  },
  create (context) {
    return {
      'SwitchStatement:exit': (node) => {
        if (node instanceof SwitchStatement) {
          const previousTests: Expression[] = []
          for (const switchCase of node.cases) {
            for (const test of switchCase.tests) {
              if (previousTests.some(previousTest => sameTokens(test, previousTest, context.tokens))) {
                context.report(test, `Duplicate case '${getNodeText(test, context.tokens)}'`)
              } else {
                previousTests.push(test)
              }
            }
          }
        }
      }
    }
  }
}
