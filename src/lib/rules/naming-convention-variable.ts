import { type Rule } from '../linter/rule.js'
import { VariableDeclaration } from '@maniascript/parser'

function isValidName (name: string, isGlobal: boolean): boolean {
  return (
    (!isGlobal || name.search(/^.+_.+/) !== -1) &&
    /[A-Z]/.test(name.charAt(0)) // Start with an uppercase letter
  )
}

export const namingConventionVariable: Rule = {
  meta: {
    id: 'naming-convention-variable',
    description: 'Enforce naming convention on variables',
    recommended: true
  },
  create (context) {
    return {
      'VariableDeclaration:enter': (node) => {
        if (node instanceof VariableDeclaration) {
          if (!isValidName(node.name.name, node.isGlobal)) {
            if (node.isGlobal) {
              context.report(node.name, 'A global variable name must be prefixed with \'G_\'')
            } else {
              context.report(node.name, 'A variable name must start with an uppercase letter')
            }
          }
          if (node.alias !== undefined && !isValidName(node.alias.name, node.isGlobal)) {
            if (node.isGlobal) {
              context.report(node.alias, 'An alias name for a global variable must be prefixed with \'G_\'')
            } else {
              context.report(node.alias, 'An alias name for a variable must start with an uppercase letter')
            }
          }
        }
      }
    }
  }
}
