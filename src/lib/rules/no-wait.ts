import { type Rule } from '../linter/rule.js'

export const noWait: Rule = {
  meta: {
    id: 'no-wait',
    description: 'Forbid the use of the wait() function',
    recommended: false
  },
  create (context) {
    return {
      'WaitStatement:exit': (node) => {
        context.report(node, 'The use of the \'wait()\' function is forbidden')
      }
    }
  }
}
