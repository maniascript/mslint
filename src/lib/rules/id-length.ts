import { type Rule } from '../linter/rule.js'
import {
  Identifier,
  VariableDeclaration,
  FunctionDeclaration,
  FunctionParameterDeclaration,
  LabelStatement,
  SettingDirective,
  CommandDirective,
  StructAliasing,
  StructDeclaration,
  StructMemberDeclaration,
  ConstAliasing,
  ConstDeclaration,
  ForeachStatement,
  ForStatement
} from '@maniascript/parser'

const DEFAULT_MINIMUM = 2
const DEFAULT_MAXIMUM = 60
const DEFAULT_EXCEPTIONS: string[] = []

export const idLength: Rule = {
  meta: {
    id: 'id-length',
    description: 'Enforce minimum and maximum lengths for identifiers',
    recommended: true,
    settings: {
      minimum: DEFAULT_MINIMUM,
      maximum: DEFAULT_MAXIMUM,
      exceptions: DEFAULT_EXCEPTIONS
    }
  },
  create (context) {
    const minimum = typeof context.settings['minimum'] === 'number' ? context.settings['minimum'] : DEFAULT_MINIMUM
    const maximum = typeof context.settings['maximum'] === 'number' ? context.settings['maximum'] : DEFAULT_MAXIMUM
    const exceptions = Array.isArray(context.settings['exceptions']) ? context.settings['exceptions'] : DEFAULT_EXCEPTIONS

    function checkIdLength (id: Identifier, ignoreTooShort?: boolean): void {
      if (id.name.length < minimum) {
        if (!exceptions.includes(id.name) && (ignoreTooShort === undefined || !ignoreTooShort)) {
          context.report(id, `Identifier name '${id.name}' is too short (${id.name.length.toString()} < ${minimum.toString()})`)
        }
      } else if (id.name.length > maximum) {
        if (!exceptions.includes(id.name)) {
          context.report(id, `Identifier name '${id.name}' is too long (${id.name.length.toString()} > ${maximum.toString()})`)
        }
      }
    }

    return {
      'VariableDeclaration:exit': (node) => {
        if (node instanceof VariableDeclaration) {
          checkIdLength(node.name)
          if (node.alias !== undefined) {
            checkIdLength(node.alias)
          }
        }
      },
      'FunctionDeclaration:exit': (node) => {
        if (node instanceof FunctionDeclaration) {
          checkIdLength(node.name)
        }
      },
      'FunctionParameterDeclaration:exit': (node) => {
        if (node instanceof FunctionParameterDeclaration) {
          checkIdLength(node.name)
        }
      },
      'LabelStatement:exit': (node) => {
        if (node instanceof LabelStatement && node.name instanceof Identifier) {
          checkIdLength(node.name)
        }
      },
      'SettingDirective:exit': (node) => {
        if (node instanceof SettingDirective) {
          checkIdLength(node.name)
        }
      },
      'CommandDirective:exit': (node) => {
        if (node instanceof CommandDirective) {
          checkIdLength(node.name)
        }
      },
      'StructAliasing:exit': (node) => {
        if (node instanceof StructAliasing) {
          checkIdLength(node.alias)
        }
      },
      'StructDeclaration:exit': (node) => {
        if (node instanceof StructDeclaration) {
          checkIdLength(node.name)
        }
      },
      'StructMemberDeclaration:exit': (node) => {
        if (node instanceof StructMemberDeclaration) {
          checkIdLength(node.name)
        }
      },
      'ConstAliasing:exit': (node) => {
        if (node instanceof ConstAliasing) {
          checkIdLength(node.alias)
        }
      },
      'ConstDeclaration:exit': (node) => {
        if (node instanceof ConstDeclaration) {
          checkIdLength(node.name)
        }
      },
      'ForeachStatement:exit': (node) => {
        if (node instanceof ForeachStatement) {
          if (node.key !== undefined) {
            checkIdLength(node.key)
          }
          checkIdLength(node.value)
        }
      },
      'ForStatement:exit': (node) => {
        if (node instanceof ForStatement) {
          checkIdLength(node.value, true)
        }
      }
    }
  }
}
