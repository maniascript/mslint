import { Severity } from '../lib/linter/rule.js'
import { allRules } from '../lib/rules/index.js'

const rules: Record<string, Severity> = {}
for (const [ruleId, rule] of allRules) {
  if (rule.meta.recommended === true) {
    rules[ruleId] = Severity.Error
  }
}
export const recommended = Object.freeze({
  rules
})
