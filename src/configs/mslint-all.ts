import { Severity } from '../lib/linter/rule.js'
import { allRules } from '../lib/rules/index.js'

const rules: Record<string, Severity> = {}
for (const [ruleId] of allRules) {
  rules[ruleId] = Severity.Error
}

export const all = Object.freeze({
  rules
})
