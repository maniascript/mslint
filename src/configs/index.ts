import { all } from './mslint-all.js'
import { recommended } from './mslint-recommended.js'

enum ConfigName {
  All = 'mslint:all',
  Recommended = 'mslint:recommended'
}

const configs = {
  all,
  recommended
}

export {
  ConfigName,
  configs
}
