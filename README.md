MSLint
======

MSLint is a tool that analyzes your ManiaScript code to find problematic patterns. It aims to help you and your team maintain consistent coding practices and prevent errors.

Documentation
-------------

The complete documentation is available on the [MSLint website](https://mslint.aessi.dev).

Prerequisites
-------------

To use MSLint you need:
- [Node.js](https://nodejs.org/) version 18 or higher
- A terminal to access MSLint via its Command Line Interface (CLI)

Installation
------------

To install MSLint as a local dependency in your project.

```sh
npm install --save-dev @maniascript/mslint
```

Basic Usage
-----------

MSLint comes with a default configuration set. So you can start linting without any further configuration. You only need to provide a [glob pattern](https://github.com/mrmlnc/fast-glob#pattern-syntax) of the files you want to lint.

```sh
npx mslint **/*.Script.txt
```

This will recursively lint all script files in the current working directory.

Rules
-----

Rules are at the heart of MSLint's linting process. Each rule checks that your code meets a specific condition and raises an error if it does not. Visit the [Rules](https://mslint.aessi.dev/rules/list) page for more information on all the rules available with MSLint.

Configuration File
------------------

The MSLint configuration file is where you can setup MSLint for your project. Choose which files to lint, which rules to apply and how to apply them, which maniascript API to use, ... Visit the [Configuration File](https://mslint.aessi.dev/guide/configuration-file) page for more information about the MSLint configuration file.
